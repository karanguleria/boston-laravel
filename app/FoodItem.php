<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FoodItem;
class FoodItem extends Model
{
    static function getMenuItems($menuId,$count)
   {
       if($count){
    return FoodItem::where('food_menu_id',$menuId)->take($count)->get();
       }else{
        return FoodItem::where('food_menu_id',$menuId)->get();
    }
   }
}
