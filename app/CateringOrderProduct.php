<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringOrderProduct extends Model
{
    protected $table = 'catering_order_product';
    protected $fillable = ['order_id', 'product_id', 'quantity', 'variant','price'];
}
