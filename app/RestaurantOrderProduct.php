<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantOrderProduct extends Model
{
    
    protected $table = 'restaurant_order_product';
    protected $fillable = ['order_id', 'product_id', 'quantity', 'variant','price'];
}
