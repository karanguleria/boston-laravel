<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Press;
use App\Accolade;
use App\RestaurantMenu;
use App\CateringMenu;
use App\HomepageSlider;
use App\RetailPartner;
use App\PromotionalBanner;
use App\RetailerPartnerSignup;
use Illuminate\Support\Facades\Mail;
use App\Product;
use App\Subscriber;
use App\Contact;
// use App\Http\Requests\subscribeRequest;

class LandingPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Getting 5 Restaurent Menu
        $restaurantMenus = RestaurantMenu::take(5)->get();
        // Getting all the slider Items
        $sliderItems = HomepageSlider::orderBy('id')->get();
        // Getting 3 promotional banners Items
        $promotionalBanners = PromotionalBanner::take(3)->get();
        return view('landing-page')->with(
            [
                "restaurantMenus"=>$restaurantMenus,
                "sliderItems"=>$sliderItems,
                'promotionalBanners'=>$promotionalBanners,
            ]
        );
    }
/**
 * Display listing of menu at manu page
 */
    public function menuIndex()
    {
        // Getting all the Restaurent Menu
        $restaurantMenus = RestaurantMenu::all();
         // Getting all the Catering Menu
         $cateringMenus = CateringMenu::all();
        return view('pages.menu')->with(
            [
                "restaurantMenus"=>$restaurantMenus,
                "cateringMenus"=>$cateringMenus
            ]
        );
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function retailIndex()
    {
        return view('pages.retailPartnerSignup');
    } 
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function retailStore(Request $request)
   {
       $retailPartner = new RetailerPartnerSignup(); 
       $retailPartner->firstname = request('firstname');
       $retailPartner->lastname = request('lastname'); 
       $retailPartner->email = request('email'); 
       $retailPartner->phone = request('phone'); 
       $retailPartner->description = request('description'); 
       $retailPartner->save();
       
       try{
        $dataArray = array(

            'firstname' => request('firstname'),
            'lastname' => request('lastname'),
            'email' => request('email'),
            'phone' => request('phone'),
            'description' => request('description')
    
        );
        Mail::to(env('ADMIN_EMAIL','swap.guleria@gmail.com'))->send(new \App\Mail\RetailerPartnerSignup($dataArray));

        return redirect()->back()->with('success_message', 'Form Submitted Successfully');

    } catch(\Exception $e){
        dd($e);
        return redirect()->back()->withError('Error in submitting the form');
    }
   }
   /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function subscribe(Request $request)
    {
        $subscriber = new Subscriber(); 
        $subscriber->first_name = request('first_name');
        $subscriber->last_name = request('last_name'); 
        $subscriber->email = request('email'); 
        $subscriber->save();
        return redirect()->back();
        // try{
        //  $dataArray = array(
 
        //      'first_name' => request('first_name'),
        //      'last_name' => request('last_name'),
        //      'email' => request('email')
        //  );
        //  Mail::to(env('ADMIN_EMAIL','swap.guleria@gmail.com'))->send(new \App\Mail\RetailerPartnerSignup($dataArray));
 
    //      return redirect()->back()->with('success_message', 'Form Submitted Successfully');
 
    //  } catch(\Exception $e){
    //      dd($e);
    //      return redirect()->back()->withError('Error in submitting the form');
    //  }
    }
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function contactShow()
    {
        return view('pages.contactUs');
    }
    public function contactStore(Request $request)
    {
        // dd($request);
        $contact = new Contact(); 
        $contact->first_name = request('first_name');
        $contact->last_name = request('last_name'); 
        $contact->email = request('email'); 
        $contact->phone = request('phone'); 
        $contact->subject = request('subject'); 
        if(request('subject') == 'catering'){
        $contact->date_of_event = request('date_of_event'); 
        $contact->corporate_catering = request('corporate_catering'); 
        $contact->social_event_catering = request('social_event_catering'); 
        $contact->concession_catering = request('concession_catering'); 
        $contact->wedding = request('wedding'); 
        $contact->golf_tournament = request('golf_tournament'); 
        $contact->food_truck = request('food_truck'); 
        }
        $contact->message = request('message'); 
        $saved = $contact->save();
        // return redirect()->back();
        // try{
        //  $dataArray = array(
 
        //      'first_name' => request('first_name'),
        //      'last_name' => request('last_name'),
        //      'email' => request('email')
        //  );
        //  Mail::to(env('ADMIN_EMAIL','swap.guleria@gmail.com'))->send(new \App\Mail\RetailerPartnerSignup($dataArray));
 if(!$saved){
         return back()->withError('Error in submitting the form');
 }else{
    return redirect()->route('thank-you');
 }
    //  } catch(\Exception $e){
    //      dd($e);
    //      return redirect()->back()->withError('Error in submitting the form');
    //  }
    }

}
