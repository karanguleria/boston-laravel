<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductVariant;
use App\Category;
use TCG\Voyager\Voyager;
use App\Like;
use Illuminate\Support\Facades\Auth;
use App\Rating;
use App\Review;
use App\RestaurantItem;
use App\CateringItem;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('store.show');
    } 
     /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
   public function filter()
   {
      $pagination = 9;
      $productCount = request('productCount') * $pagination; 
      $category = request('category');
      $sort = request('sort');
      $pricemin = request('pricemin');
      $pricemax = request('pricemax');
      $categories = Category::all();
      if($category){
       $products = Product::with('categories')->whereHas('categories', function ($query) use ($category) {
           $query->where('slug', $category);
       });
       $categoryName = optional($categories->where('slug', $category)->first())->name;
       }else{
           $products = Product::where('featured', false);
           $categoryName = 'Featured';
      }
    //   if ($sort == 'low_high') {
    //    $products = $products->where('price', '<=', $pricemax )->where('price', '>=', $pricemin )->orderBy('price')->paginate($productCount);
    //        } elseif ($sort == 'high_low') {
    //            $products = $products->where('price', '<=', $pricemax )->where('price', '>=', $pricemin )->orderBy('price', 'desc')->paginate($productCount);
    //         //    $products = $products->where('price', '<=', $pricemax )->orderBy('price', 'desc')->paginate($productCount);
    //        } else {
               $products = $products->where('price', '<=', $pricemax )->where('price', '>=', $pricemin )->paginate($productCount);
        //    }
           if(Auth::user()){
               $loginuser = true;
           }else{
            $loginuser = false;
           }
          $products = formitizeProducts($products);
           return response()->json([
            'status'   => 'ok',      
            'categoryName'=>$categoryName,
            'category'=>$category, 
            'pricemin'=>$pricemin,
            'pricemax'=>$pricemax,
            'productCount'=>$productCount,
            'products' => $products,
            'sort'=>$sort,
            'loginuser'=>$loginuser
   ]);
   }

    public function show($id)
    {
        $paginate = 4;
        $product = Product::where('slug', $id)->firstOrFail();

        $productVarients = ProductVariant::where('product_id',$product->id)->get();
        
        //Getting Related Products
        $category = $product->categories[0]->slug;

        $relatedProducts = Product::with('categories')->whereHas('categories', function ($query) use ($category) {
            $query->where('slug', $category);
        })->where('slug','!=', $id);
        $relatedProducts = $relatedProducts->paginate($paginate);

        return view('store.product')->with([
            'product' => $product,
            'productVarients' => $productVarients,
            'relatedProducts' => $relatedProducts
        ]);
    }
    
    /**
     *   
     * @param  Product  $product_id
     * @return \Illuminate\Http\Response
     */

    public function product()
    {
        $product_id = request('product_id');
        $productVarients = ProductVariant::where('product_id',$product_id)->get();
        return response()->json([
            'productVarients' => $productVarients,
        ]);
    }

    /**
     *   
     * @return \Illuminate\Http\Response
     */

    public function varientRemove()
    {
        $varients_id = request('varients_id');
        $product_id = request('product_id');
        foreach($varients_id as $varient_id ){
          ProductVariant::find($varient_id)->delete();
        }
        $productVarients = ProductVariant::where('product_id',$product_id)->get();
        return response()->json([
            'productVarients' => $productVarients,
        ]);
    }  
    
    /**
    *   
    * @return \Illuminate\Http\Response
    */

   public function varientAdd()
   {
       $ProductVariant = new ProductVariant;
       $ProductVariant->name =request('name');
       $ProductVariant->price =request('price');
       $ProductVariant->product_id = request('product_id');
       $ProductVariant->save();

       $productVarients = ProductVariant::where('product_id',request('product_id'))->get();

       return response()->json([
           'productVarients' => $productVarients,
       ]);
   }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Product  $product_id
     * @return \Illuminate\Http\Response
     */
    public function wishlist(REQUEST $request)
    {
        $wishlist = Like::where('type',2)->where('parent_id',request('product_id'))->where('user_id',Auth::user()->id)->count();
        if($wishlist){
            Like::where('type',2)->where('parent_id',request('product_id'))->where('user_id',Auth::user()->id)->delete();
            // return redirect()->back()->with('success_message',"Successfull Removed from wishlist");
            return response()->json([
                'success'   => 'unliked',
               ]);
        }else{
        $wishlist = new Like();

        $wishlist->parent_id = request('product_id');
        $wishlist->type = 2;
        $wishlist->user_id = Auth::user()->id;

        $wishlist->save();
        return response()->json([
            'success'   => 'liked',
           ]);
        }
      

       }
       /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showajax()
    {
        $id = request('product_id');
        $paginate = 4;
        $product = Product::where('id', $id)->firstOrFail();
        {
            $varientLast ='';
            $varient_html ='';
            $productVarients = ProductVariant::where('product_id',$product->id)->get();
            $pice_html ="";
            if(@$productVarients){
                foreach($productVarients as $productVarient){
                    $lastVarientId = $productVarient->id ;
                    $lastVarientPrice = $productVarient->price ;
                }
            }
            $price =@($lastVarientPrice) ? presentPrice($lastVarientPrice) : presentPrice($product->price);
            if((@$product->display_price) && (!@$lastVarientPrice)){
                $pice_html = '<h2 class="product-price"><span style="text-decoration: line-through;padding-right: 20px">'.presentPrice($product->display_price).'</span>'.$price.'</h2>';
            }else{
                $pice_html = '<h2 class="product-price">'.$price.'</h2>';
            }
            if(@$lastVarientPrice){
                $varient_html = '<h4>Size</h4>';
                  foreach($productVarients as $productVarient){
                    $class =($lastVarientId == $productVarient->id) ? ' selected' : '' ;
                    $varient_html .= '<button class="varients'.$class .'" data-price="'.$productVarient->price .'" data-id="'. $productVarient->id .'">'. $productVarient->name .'</button>';
                     $varientLast = $productVarient->id ;
                    }
            }
            $product['pice_html']=$pice_html;
            $product['varient_html']=$varient_html;
            
            $product['varientLast']=$varientLast;
            $ratingAverage = Rating::where('product_id',$product->id)->where('status','Approved')->avg('rating');
            $ratingAverage = round($ratingAverage,2);
            $rating = round($ratingAverage);
            $rating_html ='<div class="starRev">
                <ul>';
                if(@$rating){
                    for($i=1; $i<$rating; $i++){
                        $rating_html .='<li><i title='.$ratingAverage.' class="fas fa-star"></i></li>';
                    }
                    
                    if($rating > $ratingAverage ){
                        $rating_html .='<li><i title='.$ratingAverage.' class="fas fa-star-half"></i></li>';
                    }else{
                    $rating_html .='<li><i title='.$ratingAverage.' class="fas fa-star"></i></li>';
                    }
                   }
                else{
                    $rating_html .='<li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li>';
                }
                $rating_html .='</ul></div>';
        $product['review'] = Review::where('product_id',$product->id)->where('status','Approved')->count(); 
        $product['rating_html'] = $rating_html;
        $product['name'] = strtoupper($product->name);
        // $product['price'] = presentPrice($product->price);
        $product['image'] = $product->image && file_exists('storage/'.$product->image) ? 'storage/'.$product->image : asset('images/not-found.jpg');
        $product['addtoCart'] = route('cart.store', $product) ;
        $product['tlink'] = "https://twitter.com/share?url=".route('shop.show',$product->slug)."&via=TWITTER_HANDLE&text=".$product->name;
        $product['flink'] = "https://www.facebook.com/sharer/sharer.php?u=".route('shop.show',$product->slug);
        $product['glink'] = "https://plus.google.com/share?url=".route('shop.show',$product->slug);
        }
        
        
        //Getting Related Products
        $category = $product->categories[0]->slug;


        return response()->json([
            'product' => $product,
            'productVarients' => $productVarients,
        ]);
    }
     /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showajaxCatering()
    {
        $id = request('product_id');
        $product_type = request('product_type');
        if($product_type=="restaurant"){
            $product = RestaurantItem::where('id', $id)->firstOrFail();
            $product['addtoCart'] = route('cart.restaurant-store', $product) ;
        }else{
            $product = CateringItem::where('id', $id)->firstOrFail();
            $product['addtoCart'] = route('cart.catering-store', $product) ;
        }
        $images_html = '';
        if($product->images){
             foreach (json_decode($product->images, true) as $image){ 
                 $images_html .='<img src="storage/'.$image.'" alt="'.$product->name .'">';
                  }
                }
        $product['name'] = strtoupper($product->name);
        $product['image'] = $images_html;
        $product['tlink'] = "https://twitter.com/share?url=".route('shop.show',$product->slug)."&via=TWITTER_HANDLE&text=".$product->name;
        $product['flink'] = "https://www.facebook.com/sharer/sharer.php?u=".route('shop.show',$product->slug);
        $product['glink'] = "https://plus.google.com/share?url=".route('shop.show',$product->slug);

        return response()->json([
            'product' => $product,
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Product  $product_id
     * @return \Illuminate\Http\Response
     */
    public function rating(REQUEST $request)
    {
        $rating = Rating::where('product_id',request('product_id'))->where('user_id',Auth::user()->id)->count();
        if($rating){
            $rating = Rating::where('product_id',request('product_id'))->where('user_id',Auth::user()->id)->first();
            $rating = Rating::find($rating->id);
            $rating->rating = request('rating');
            $rating->save();
            // Rating::where('parent_id',request('product_id'))->where('user_id',Auth::user()->id)->delete();
            // return redirect()->back()->with('success_message',"Successfull Removed from wishlist");
            return response()->json([
                'success'   => 'rated',
               ]);
        }else{
        $rating = new Rating();

        $rating->product_id = request('product_id');
        $rating->rating = request('rating');
        $rating->user_id = Auth::user()->id;

        $rating->save();
        
        return response()->json([
            'success'   => 'rated',
           ]);
        }
      

       }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Product  $product_id
     * @return \Illuminate\Http\Response
     */
    public function reviewStore($product_id)
    {
        $review = Review::where('product_id',$product_id)->where('user_id',Auth::user()->id)->count();
        if($review){
            $review = Review::where('product_id',$product_id)->where('user_id',Auth::user()->id)->first();
            $review = Review::find($review->id);
            $review->description = request('comment');
            $review->save();
            return redirect()->back()->with('success_message',"Review Successfull Updated");
        }else{
            $review = new Review();
            $review->product_id = $product_id;
            $review->description = request('comment');
            $review->user_id = Auth::user()->id;
            $review->save();  
            return redirect()->back()->with('success_message',"Review Successfull added");
        }
       }

}
