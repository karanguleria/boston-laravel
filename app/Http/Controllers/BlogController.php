<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TCG\Voyager\Models\Post;
use App\Comment;
use Illuminate\Support\Facades\Auth;
use App\Like;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::where('type', 'blog')->orderBy('id','DESC')->paginate(6);
        return view('blog.index', compact('posts'));
    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function videos()
    {
        $videos = Post::where('type', 'video')->orderBy('id','DESC')->get();
        return view('blog.videos', compact('videos'));
    }
    /**
     * Display the specified video.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function video($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        $related_posts = Post::take(3)->get();
        $previous = Post::where('type', 'video')->where('id', '<', $post->id)->orderBy('id', 'Desc')->first();
        $next = Post::where('type', 'video')->where('id', '>', $post->id)->orderBy('id')->first();

        // return view('blog.show')->with(compact('post', 'related_posts'));
        return view('blog.showvideo')->with(compact('post','related_posts', 'previous', 'next'));

    }
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function photos()
    {
        $photos = Post::where('type', 'photo')->orderBy('id','DESC')->get();
        return view('blog.photos', compact('photos'));
    }
     /**
     * Display the specified video.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function photo($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        $related_posts = Post::take(3)->get();
        $previous = Post::where('type', 'photo')->where('id', '<', $post->id)->orderBy('id', 'Desc')->first();
        $next = Post::where('type', 'photo')->where('id', '>', $post->id)->orderBy('id')->first();

        // return view('blog.show')->with(compact('post', 'related_posts'));
        return view('blog.showphoto')->with(compact('post','related_posts', 'previous', 'next'));

    }
    /**
     * Display a listing as gallery at m.
     *
     * @return \Illuminate\Http\Response
     */
    public function gallery()
    {
        $video = Post::where('type', 'video')->orderBy('id','DESC')->first();
        $video_next = Post::where('type', 'video')->where('id', '<', $video->id)->orderBy('id','desc')->first();
        $photo = Post::where('type', 'photo')->orderBy('id','DESC')->first();
        $photo_next = Post::where('type', 'photo')->where('id', '<', $photo->id)->orderBy('id','desc')->first();
        $blog = Post::where('type', 'blog')->orderBy('id','DESC')->first();
        $blog_next = Post::where('type', 'blog')->where('id', '<', $blog->id)->orderBy('id','desc')->first();
        return view('blog.gallery', compact('video','video_next','photo','photo_next','blog','blog_next'));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  Post  $post_id
     * @return \Illuminate\Http\Response
     */
    public function commentStore($post_id)
    {
        $comment = new Comment;

        $comment->post_id = $post_id;
        $comment->comment = request('comment');
        $comment->user_id = Auth::user()->id;

        $comment->save();
     
       return redirect()->back()->with('success_message',"commnet Successfull added");

       }
    /**
     * Store a newly created resource in storage.
     *
     * @param  Post  $post_id
     * @return \Illuminate\Http\Response
     */
    public function likeStore($post_id)
    {
        $likes = Like::where('type',1)->where('parent_id',$post_id)->where('user_id',Auth::user()->id)->count();
        if($likes){
            Like::where('type',1)->where('parent_id',$post_id)->where('user_id',Auth::user()->id)->delete();
            return redirect()->back()->with('success_message',"commnet Successfull added");
        }else{
        $like = new Like();

        $like->parent_id = $post_id;
        $like->type = 1;
        $like->user_id = Auth::user()->id;

        $like->save();
        return redirect()->back()->with('success_message',"commnet Successfull added");
        }
      

       }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->firstOrFail();
        $related_posts = Post::take(3)->get();
        // $previous = Post::where('id', '<', $post->id)->orderBy('id', 'Desc')->first();
        // $next = Post::where('id', '>', $post->id)->orderBy('id')->first();

        return view('blog.show')->with(compact('post', 'related_posts'));
        // return view('blog.show')->with(compact('post', 'previous', 'next'));

    }

}
