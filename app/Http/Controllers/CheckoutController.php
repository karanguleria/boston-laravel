<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CheckoutRequest;
use Gloudemans\Shoppingcart\Facades\Cart;
use Srmklive\PayPal\Services\ExpressCheckout;
use App\Order;
use App\OrderProduct;
use Illuminate\Support\Facades\Mail;
use App\Mail\OrderPlaced;
use App\RestaurantOrder;
use App\RestaurantOrderProduct;
use App\CateringOrderProduct;
use App\CateringOrder;

class CheckoutController extends Controller
{

    protected $provider;
    
    public function __construct() {
        $this->provider = new ExpressCheckout();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        if (Cart::instance('default')->count() == 0) {
            return redirect()->route('shop.index');
        }
        return view('checkout')->with([
            'discount' => getNumbers()->get('discount'),
            'newSubtotal' => getNumbers()->get('newSubtotal'),
            'newTax' => getNumbers()->get('newTax'),
            'newTotal' => getNumbers()->get('newTotal'),
        ]);

    }

    public function paypalCheckout(CheckoutRequest $request) {
        // get new invoice id
        $order_id = Order::count() + 1;
            
        // Get the cart data
        $cart = $this->getCart($order_id);
        // create new invoice
           $order = Order::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'billing_email' => $request->email,
            'billing_name' => $request->name,
            'billing_address' => $request->address,
            'billing_city' => $request->city,
            'billing_province' => $request->province,
            'billing_country' => $request->province,
            'billing_postalcode' => $request->postalcode,
            'billing_phone' => $request->phone,
            'billing_name_on_card' => $request->name_on_card,
            'billing_discount' => getNumbers()->get('discount'),
            'billing_discount_code' => getNumbers()->get('code'),
            'billing_subtotal' => getNumbers()->get('newSubtotal'),
            'billing_tax' => getNumbers()->get('newTax'),
            'billing_total' => getNumbers()->get('newTotal'),
            'error' => "",
        ]);

        // Insert into order_product table
        foreach (Cart::instance('default')->content() as $item) {
            OrderProduct::create([
                'order_id' => $order_id,
                'product_id' => $item->model->id,
                'quantity' => $item->qty,
                'variant' => $item->options->variant,
                'price' => $item->price,
                ]);
        }
        // send a request to paypal 
        // paypal should respond with an array of data
        // the array should contain a link to paypal's payment system
        $response = $this->provider->setExpressCheckout($cart);
        // if there is no link redirect back with error message
        if (!$response['paypal_link']) {
            print_r($cart);
            echo "die at paypal_link is absent";
            dd($response);
          return redirect('/')->with(['code' => 'danger', 'message' => 'Something went wrong with PayPal']);
          // For the actual error message dump out $response and see what's in there
        }
      
        // redirect to paypal
        // after payment is done paypal
        // will redirect us back to $this->expressCheckoutSuccess
        return redirect($response['paypal_link']);
      }
      
      
      
    private function getCart($order_id)
    {
        return [
            // if payment is not recurring cart can have many items
            // with name, price and quantity
            'items' =>  [
                [
                'name' => 'Boston Burger',
                'price' => round(getNumbers()->get('newTotal'),2),
                'qty' => 1,
                ]
            ],

            // return url is the url where PayPal returns after user confirmed the payment
            'return_url' => url('/paypal-checkout-success'),
            // every invoice id must be unique, else you'll get an error from paypal
            'invoice_id' => config('paypal.invoice_prefix') . '_9991' . $order_id,
            'invoice_description' => "Order #" . $order_id . " Invoice",
            'cancel_url' => url('/paypal-checkout-cancel'),
            // total is calculated by multiplying price with quantity of all cart items and then adding them up
            // in this case total is 20 because Product 1 costs 10 (price 10 * quantity 1) and Product 2 costs 10 (price 5 * quantity 2)
            'total' => round(getNumbers()->get('newTotal'),2)
        ];
    }
    public function paypalCheckoutSuccess(Request $request) {


        $token = $request->get('token');

        $PayerID = $request->get('PayerID');

        // initaly we paypal redirects us back with a token
        // but doesn't provice us any additional data
        // so we use getExpressCheckoutDetails($token)
        // to get the payment details
        $response = $this->provider->getExpressCheckoutDetails($token);

        // if response ACK value is not SUCCESS or SUCCESSWITHWARNING
        // we return back with error
        if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            return redirect('/')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment']);
        }

        // invoice id is stored in INVNUM
        // because we set our invoice to be xxxx_id
        // we need to explode the string and get the second element of array
        // witch will be the id of the invoice
        $order_id = explode('_9991', $response['INVNUM'])[1];

        // get cart data
        $cart = $this->getCart($order_id);

        

        // if payment is not recurring just perform transaction on PayPal
        // and get the payment status
        $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];


        // find invoice by id
        $order = Order::find($order_id);

        // set invoice status
        $order->status = $status;
     
        // save the invoice
        $order->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message
        if ($order->paid) {
            return redirect()->route('confirmation.index',$order_id)->with('success_message', 'Thank you! Your payment has been successfully accepted!');
       
        }
        
        return redirect('/checkout')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment for Order ' . $invoice->id . '!']);
    }

    public function paypalCheckoutCancel(Request $request) {
        dd("canceled");
        // find invoice by id
        $order = Order::find($order_id);

        // set invoice status
        $order->status = $status;

     
        // save the invoice
        $order->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message
        if ($order->paid) {
            return redirect('/thankyou')->with(['code' => 'success', 'message' => 'Order ' . $order_id . ' has been paid successfully!']);
        }
        
        return redirect('/checkout')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment for Order ' . $invoice->id . '!']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function restaurantIndex()
    { 
        if (Cart::instance('restaurant')->count() == 0) {
            return redirect()->route('restaurant.category');
        }
        return view('restaurant.checkout')->with([
            'discount' => getNumbersRestaurant()->get('discount'),
            'newSubtotal' => getNumbersRestaurant()->get('newSubtotal'),
            'newTax' => getNumbersRestaurant()->get('newTax'),
            'newTotal' => getNumbersRestaurant()->get('newTotal'),
        ]);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CateringIndex()
    { 
        if (Cart::instance('catering')->count() == 0) {
            return redirect()->route('catering.category');
        }
        return view('catering.checkout')->with([
            'discount' => getNumbersCatering()->get('discount'),
            'newSubtotal' => getNumbersCatering()->get('newSubtotal'),
            'newTax' => getNumbersCatering()->get('newTax'),
            'newTotal' => getNumbersCatering()->get('newTotal'),
        ]);

    }

    public function restaurantPaypalCheckout(CheckoutRequest $request) {
        // get new invoice id
        $order_id = RestaurantOrder::count() + 1;
            
        // Get the cart data
        $cart = $this->getCartRestaurant($order_id);
        // create new invoice
           $order = RestaurantOrder::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'billing_email' => $request->email,
            'billing_name' => $request->name,
            'billing_address' => $request->address,
            'billing_city' => $request->city,
            'billing_province' => $request->province,
            'billing_country' => $request->province,
            'billing_postalcode' => $request->postalcode,
            'billing_phone' => $request->phone,
            'billing_name_on_card' => $request->name_on_card,
            'billing_discount' => getNumbers()->get('discount'),
            'billing_discount_code' => getNumbers()->get('code'),
            'billing_subtotal' => getNumbers()->get('newSubtotal'),
            'billing_tax' => getNumbers()->get('newTax'),
            'billing_total' => getNumbers()->get('newTotal'),
            'error' => "",
        ]);
        // Insert into order_product table
        foreach (Cart::instance('restaurant')->content() as $item) {
            RestaurantOrderProduct::create([
                'order_id' => $order_id,
                'product_id' => $item->model->id,
                'quantity' => $item->qty,
                'variant' => $item->options->variant,
                'price' => $item->price,
                ]);
        }
        // send a request to paypal 
        // paypal should respond with an array of data
        // the array should contain a link to paypal's payment system
        $response = $this->provider->setExpressCheckout($cart);
        // if there is no link redirect back with error message
        if (!$response['paypal_link']) {
            print_r($cart);
            echo "die at paypal_link is absent";
            dd($response);
          return redirect('/')->with(['code' => 'danger', 'message' => 'Something went wrong with PayPal']);
          // For the actual error message dump out $response and see what's in there
        }
      
        // redirect to paypal
        // after payment is done paypal
        // will redirect us back to $this->expressCheckoutSuccess
        return redirect($response['paypal_link']);
      }
      private function getCartRestaurant($order_id)
      {
       
          return [
              // if payment is not recurring cart can have many items
              // with name, price and quantity
              'items' =>  [
                  [
                  'name' => 'Boston Burger',
                  'price' => round(getNumbersRestaurant()->get('newTotal'),2),
                  'qty' => 1,
                  ]
              ],
  
              // return url is the url where PayPal returns after user confirmed the payment
              'return_url' => url('/restaurant/paypal-checkout-success'),
              // every invoice id must be unique, else you'll get an error from paypal
              'invoice_id' => config('paypal.invoice_prefix') . '__9991' . $order_id,
              'invoice_description' => "Order #" . $order_id . " Invoice",
              'cancel_url' => url('/restaurant/paypal-checkout-cancel'),
              // total is calculated by multiplying price with quantity of all cart items and then adding them up
              // in this case total is 20 because Product 1 costs 10 (price 10 * quantity 1) and Product 2 costs 10 (price 5 * quantity 2)
              'total' => round(getNumbersRestaurant()->get('newTotal'),2)
          ];
      }
      public function restaurantPaypalCheckoutSuccess(Request $request) {


        $token = $request->get('token');

        $PayerID = $request->get('PayerID');

        // initaly we paypal redirects us back with a token
        // but doesn't provice us any additional data
        // so we use getExpressCheckoutDetails($token)
        // to get the payment details
        $response = $this->provider->getExpressCheckoutDetails($token);

        // if response ACK value is not SUCCESS or SUCCESSWITHWARNING
        // we return back with error
        if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            return redirect('/')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment']);
        }

        // invoice id is stored in INVNUM
        // because we set our invoice to be xxxx_id
        // we need to explode the string and get the second element of array
        // witch will be the id of the invoice
        $order_id = explode('__9991', $response['INVNUM'])[1];

        // get cart data
        $cart = $this->getCartRestaurant($order_id);

        

        // if payment is not recurring just perform transaction on PayPal
        // and get the payment status
        $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];


        // find invoice by id
        $order = RestaurantOrder::find($order_id);

        // set invoice status
        $order->status = $status;
     
        // save the invoice
        $order->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message
        if ($order->paid) {
            return redirect()->route('confirmation.restaurant-index',$order_id)->with('success_message', 'Thank you! Your payment has been successfully accepted!');
       
        }
        
        return redirect('/restaurant/checkout')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment for Order ' . $invoice->id . '!']);
    }

    public function restaurantPaypalCheckoutCancel(Request $request) {
        dd("canceled");
        // find invoice by id
        $order = RestaurantOrder::find($order_id);

        // set invoice status
        $order->status = $status;

     
        // save the invoice
        $order->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message
        if ($order->paid) {
            return redirect('/restaurant/thankyou')->with(['code' => 'success', 'message' => 'Order ' . $order_id . ' has been paid successfully!']);
        }
        
        return redirect('/restaurant/checkout')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment for Order ' . $invoice->id . '!']);
    }

    public function cateringPaypalCheckout(CheckoutRequest $request) {
        // get new invoice id
        $order_id = CateringOrder::count() + 1;
            
        // Get the cart data
        $cart = $this->getCartCatering($order_id);
        // create new invoice
           $order = CateringOrder::create([
            'user_id' => auth()->user() ? auth()->user()->id : null,
            'billing_email' => $request->email,
            'billing_name' => $request->name,
            'billing_address' => $request->address,
            'billing_city' => $request->city,
            'billing_province' => $request->province,
            'billing_country' => $request->province,
            'billing_postalcode' => $request->postalcode,
            'billing_phone' => $request->phone,
            'billing_name_on_card' => $request->name_on_card,
            'billing_discount' => getNumbers()->get('discount'),
            'billing_discount_code' => getNumbers()->get('code'),
            'billing_subtotal' => getNumbers()->get('newSubtotal'),
            'billing_tax' => getNumbers()->get('newTax'),
            'billing_total' => getNumbers()->get('newTotal'),
            'error' => "",
        ]);

        // Insert into order_product table
        foreach (Cart::instance('catering')->content() as $item) {
            CateringOrderProduct::create([
                'order_id' => $order_id,
                'product_id' => $item->model->id,
                'quantity' => $item->qty,
                'variant' => $item->options->variant,
                'price' => $item->price,
                ]);
        }
        // send a request to paypal 
        // paypal should respond with an array of data
        // the array should contain a link to paypal's payment system
        $response = $this->provider->setExpressCheckout($cart);
        // if there is no link redirect back with error message
        if (!$response['paypal_link']) {
            print_r($cart);
            echo "die at paypal_link is absent";
            dd($response);
          return redirect('/')->with(['code' => 'danger', 'message' => 'Something went wrong with PayPal']);
          // For the actual error message dump out $response and see what's in there
        }
      
        // redirect to paypal
        // after payment is done paypal
        // will redirect us back to $this->expressCheckoutSuccess
        return redirect($response['paypal_link']);
      }
      private function getCartCatering($order_id)
      {
          return [
              // if payment is not recurring cart can have many items
              // with name, price and quantity
              'items' =>  [
                  [
                  'name' => 'Boston Burger',
                  'price' => round(getNumbersCatering()->get('newTotal'),2),
                  'qty' => 1,
                  ]
              ],
  
              // return url is the url where PayPal returns after user confirmed the payment
              'return_url' => url('/catering/paypal-checkout-success'),
              // every invoice id must be unique, else you'll get an error from paypal
              'invoice_id' => config('paypal.invoice_prefix') . '___9991' . $order_id,
              'invoice_description' => "Order #" . $order_id . " Invoice",
              'cancel_url' => url('/catering/paypal-checkout-cancel'),
              // total is calculated by multiplying price with quantity of all cart items and then adding them up
              // in this case total is 20 because Product 1 costs 10 (price 10 * quantity 1) and Product 2 costs 10 (price 5 * quantity 2)
              'total' => round(getNumbersCatering()->get('newTotal'),2)
          ];
      }
      public function cateringPaypalCheckoutSuccess(Request $request) {


        $token = $request->get('token');

        $PayerID = $request->get('PayerID');

        // initaly we paypal redirects us back with a token
        // but doesn't provice us any additional data
        // so we use getExpressCheckoutDetails($token)
        // to get the payment details
        $response = $this->provider->getExpressCheckoutDetails($token);

        // if response ACK value is not SUCCESS or SUCCESSWITHWARNING
        // we return back with error
        if (!in_array(strtoupper($response['ACK']), ['SUCCESS', 'SUCCESSWITHWARNING'])) {
            return redirect('/')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment']);
        }

        // invoice id is stored in INVNUM
        // because we set our invoice to be xxxx_id
        // we need to explode the string and get the second element of array
        // witch will be the id of the invoice
        $order_id = explode('___9991', $response['INVNUM'])[1];

        // get cart data
        $cart = $this->getCartCatering($order_id);

        

        // if payment is not recurring just perform transaction on PayPal
        // and get the payment status
        $payment_status = $this->provider->doExpressCheckoutPayment($cart, $token, $PayerID);
        $status = $payment_status['PAYMENTINFO_0_PAYMENTSTATUS'];


        // find invoice by id
        $order = CateringOrder::find($order_id);

        // set invoice status
        $order->status = $status;
     
        // save the invoice
        $order->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message
        if ($order->paid) {
            return redirect()->route('confirmation.catering-index',$order_id)->with('success_message', 'Thank you! Your payment has been successfully accepted!');
       
        }
        
        return redirect('/catering/checkout')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment for Order ' . $invoice->id . '!']);
    }

    public function cateringPaypalCheckoutCancel(Request $request) {
        dd("canceled");
        // find invoice by id
        $order = CateringOrder::find($order_id);

        // set invoice status
        $order->status = $status;

     
        // save the invoice
        $order->save();

        // App\Invoice has a paid attribute that returns true or false based on payment status
        // so if paid is false return with error, else return with success message
        if ($order->paid) {
            return redirect('/catering/thankyou')->with(['code' => 'success', 'message' => 'Order ' . $order_id . ' has been paid successfully!']);
        }
        
        return redirect('/catering/checkout')->with(['code' => 'danger', 'message' => 'Error processing PayPal payment for Order ' . $invoice->id . '!']);
    }
}
