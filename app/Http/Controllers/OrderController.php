<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Order;

class OrderController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        if (Auth::user()->id !== $order->user_id) {
            return back()->withErrors('You do not have access to this!');
        }
        $products = $order->products;

        return view('account.order')->with([
            'order' => $order,
            'products' => $products,
        ]);
    }

}
