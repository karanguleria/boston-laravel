<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductVariant;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\OrderProduct;
use App\CateringItem;
use App\RestaurantItem;

class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
        return view('cart')->with([
            'discount' => getNumbers()->get('discount'),
            'newTax' => getNumbers()->get('newTax'),
            'newSubtotal' => getNumbers()->get('newSubtotal'),
            'newTotal' => getNumbers()->get('newTotal')
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Product  $product
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Product $product, Request $request)
    { 

        $varientId = @($request->varient) ?? 0 ;
        if(@$varientId){
            $varient = ProductVariant::where("id",$varientId)->first();
            Cart::instance('default')->add($product->id, $product->name, $request->quantityCount, $varient->price,['variant' => $varient->name])
                ->associate('App\Product');
        }else{
            Cart::instance('default')->add($product->id, $product->name, $request->quantityCount, $product->price)
            ->associate('App\Product');
        }
        return redirect()->route('cart.index')->with('success_message', 'Item was added to your cart!');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cart::instance('default')->update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::instance('default')->remove($id);
        return redirect()->back()->with('success_message', "Item removed form cart");
    }

 /**
     * Store a newly created resource in storage.
     *
     * @param  \App\RestaurantItem  $product
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function restaurantStore(RestaurantItem $item, Request $request)
    { 
            Cart::instance('restaurant')->add($item->id, $item->name, $request->quantityCount, $item->price)
            ->associate('App\RestaurantItem');
        return redirect()->route('cart.restaurant-index')->with('success_message', 'Item was added to your cart!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function restaurantIndex()
    {
        return view('restaurant.cart')->with([
            'discount' => getNumbersRestaurant()->get('discount'),
            'newTax' => getNumbersRestaurant()->get('newTax'),
            'newSubtotal' => getNumbersRestaurant()->get('newSubtotal'),
            'newTotal' => getNumbersRestaurant()->get('newTotal')
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restaurantUpdate(Request $request, $id)
    {
        Cart::instance('restaurant')->update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function restaurantDestroy($id)
    {
        Cart::instance('restaurant')->remove($id);
        return redirect()->back()->with('success_message', "Item removed form cart");
    }

 /**
     * Store a newly created resource in storage.
     *
     * @param  \App\CateringItem  $product
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function cateringStore(CateringItem $item, Request $request)
    { 
            Cart::instance('catering')->add($item->id, $item->name, $request->quantityCount, $item->price)
            ->associate('App\CateringItem');
        return redirect()->route('cart.catering-index')->with('success_message', 'Item was added to your cart!');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cateringIndex()
    {
        return view('catering.cart')->with([
            'discount' => getNumbersCatering()->get('discount'),
            'newTax' => getNumbersCatering()->get('newTax'),
            'newSubtotal' => getNumbersCatering()->get('newSubtotal'),
            'newTotal' => getNumbersCatering()->get('newTotal')
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cateringUpdate(Request $request, $id)
    {
        Cart::instance('catering')->update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');
        return response()->json(['success' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cateringDestroy($id)
    {
        Cart::instance('catering')->remove($id);
        return redirect()->back()->with('success_message', "Item removed form cart");
    }

}
