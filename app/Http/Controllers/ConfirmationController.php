<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Illuminate\Support\Facades\Auth;
use App\CateringOrder;
use App\RestaurantOrder;
use App\CateringOrderProduct;
use App\RestaurantOrderProduct;

class ConfirmationController extends Controller
{

    public function show($orderId)
    {
          if (! session('success_message')){
            dd("no Success message");
            return redirect('/');
        }
        $order = Order::find($orderId);
        if (Auth::user()->id !== $order->user_id) {
            return back()->withErrors('You do not have access to this!');
        }
        $products = $order->products;

        return view('thankyou')->with([
            'order' => $order,
            'products' => $products,
        ]);
    }

    public function cateringShow($orderId)
    {
        //   if (! session('success_message')){
        //     dd("no Success message");
        //     return redirect('/');
        // }
        $order = CateringOrder::find($orderId);
        if (Auth::user()->id !== $order->user_id) {
            return back()->withErrors('You do not have access to this!');
        }
        $products = CateringOrderProduct::where('order_id',$order->id)->get();

        return view('catering.thankyou')->with([
            'order' => $order,
            'products' => $products,
        ]);
    }
    public function restaurantShow($orderId)
    {
        //   if (! session('success_message')){
        //     dd("no Success message");
        //     return redirect('/');
        // }
        $order = RestaurantOrder::find($orderId);
        if (Auth::user()->id !== $order->user_id) {
            return back()->withErrors('You do not have access to this!');
        }
        $products = RestaurantOrderProduct::where('order_id',$order->id)->get();
        // $products = $order->products;

        return view('restaurant.thankyou')->with([
            'order' => $order,
            'products' => $products,
        ]);
    }

}
