<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CateringCategory;
use App\CateringMenu;
use App\CateringItem;

class CateringController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function category()
    {
        $categories = CateringMenu::all();
        return view('catering.category')->with([
            'categories'=>$categories
        ]);
    } 

  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories()
    {
        $categories = CateringCategory::all();
        // dd($categories);
        return view('catering.categories')->with([
            'categories'=>$categories
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderNow()
    {
        return view('catering.order-now');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderDelivery()
    {
        return view('catering.order-delivery');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $paginate = 4;
        $product = CateringItem::where('slug', $id)->firstOrFail();

        // $productVarients = ProductVariant::where('product_id',$product->id)->get();
        
        //Getting Related Products
        // $category = $product->categories[0]->slug;

        // $relatedProducts = Product::with('categories')->whereHas('categories', function ($query) use ($category) {
        //     $query->where('slug', $category);
        // })->where('slug','!=', $id);
        // $relatedProducts = $relatedProducts->paginate($paginate);

        return view('catering.product')->with([
            'product' => $product,
            // 'productVarients' => $productVarients,
            // 'relatedProducts' => $relatedProducts
        ]);
    }
    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
