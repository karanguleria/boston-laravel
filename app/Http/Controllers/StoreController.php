<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('store.index')->with([
            'categories'=>$categories
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $pagination=10;
        $categories = Category::all();
        
        $products = Product::with('categories')->whereHas('categories', function ($query) use ($slug) {
              $query->where('slug', $slug);
        });
        $categoryName = optional($categories->where('slug', $slug)->first())->name;

        $products = $products->paginate($pagination);
        return view('store.show')->with([
            'products' => $products,
            'categoryName'=>$categoryName
        ]);
    }

}
