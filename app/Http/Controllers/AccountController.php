<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Order;
use App\Like;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          // check if user is not admin
     if(Auth::user()->role_id == 1){
        return redirect('admin');
     }else{
         $orders = Order::where('user_id',Auth::user()->id)->paginate(99);
         $wishlists = Like::where('type',2)->where('user_id',Auth::user()->id)->get();
         return view('account.index',compact('orders','wishlists'));
     }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        User::where('id',Auth::user()->id)->update([
            'name' => request('name'),
            'last_name' => request('last_name')
        ]);

        return back()->with("success_message","Details Updated Successfully ");

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request)
    {
        $password_old = request('password');
        $password_new = request('password_new');
        $password_retype = request('password_retype');
        if($password_new != $password_retype){
            return back()->withErrors('Password doesn\'t match');
        }
        $password_current = User::where('id',Auth::user()->id)->first()->password; 
        if(Hash::check($password_old, $password_current)){
         User::where('id', Auth::user()->id)->update([
             'password' => bcrypt($password_new)
         ]);
         Auth::logout();
         return back()->with('success_message','Password Updated Successfully');
        }else{
            return back()->withErrors('Old Password doesn\'t match');
        }
     
// dd($oldPassword);

        dd(request());

    }
}
