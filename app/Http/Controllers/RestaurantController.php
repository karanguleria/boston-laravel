<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RestaurantMenu;
use App\RestaurantItem;

class RestaurantController extends Controller
{
  /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function category()
    {
        $categories = RestaurantMenu::all();
        return view('restaurant.category')->with([
            'categories'=>$categories
        ]);
    } 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = RestaurantItem::where('slug', $id)->firstOrFail();

        return view('restaurant.product')->with([
            'product' => $product,
        ]);
    }
    
}
