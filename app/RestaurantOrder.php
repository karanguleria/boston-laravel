<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantOrder extends Model
{
       protected $fillable = [
        'user_id', 'billing_email', 'billing_name', 'billing_address', 'billing_city','billing_country',
        'billing_province', 'billing_postalcode', 'billing_phone', 'billing_name_on_card', 'billing_discount', 'billing_discount_code', 'billing_subtotal', 'billing_tax', 'billing_total', 'payment_gateway', 'error','pickup_date','pickup_time'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function products()
    {
        return $this->belongsToMany('App\RestaurantItem')->withPivot('quantity')->withPivot('variant')->withPivot('price');;
    }

    public function getPaidAttribute() {
    	if ($this->payment_status == 'pending') {
    		return false;
    	}
    	return true;
    }
}
