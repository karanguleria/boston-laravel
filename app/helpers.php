<?php

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use App\Like;
use App\Rating;

function productImage($path)
{
    return $path && file_exists('storage/'.$path) ? asset('storage/'.$path) : asset('images/not-found.jpg');
}

function presentPrice($price)
{
    // return $price ;
    return money_format('$ %i', $price);
}

function getNumbers()
{
    $tax = config('cart.tax')/100;
    $discount = session()->get('coupon')['discount'] ?? 0;
    $code = session()->get('coupon')['name'];
    $newSubtotal = Cart::instance('default')->subTotal() - $discount;
    if($newSubtotal < 0){
        $newSubtotal = 0;
    }  
    $newTax = $newSubtotal * $tax;
    $newTotal =  $newSubtotal*(1+$tax);
    return collect([
        'discount'=>$discount,
        'tax' => $tax,
        'code' => $code,
        'newSubtotal' => $newSubtotal,
        'newTax' => $newTax,
        'newTotal' => $newTotal
    ]);
}

function getNumbersRestaurant()
{
    $tax = config('cart.tax')/100;
    $discount = session()->get('coupon')['discount'] ?? 0;
    $code = session()->get('coupon')['name'];
    $newSubtotal = Cart::instance('restaurant')->subTotal() - $discount;
    if($newSubtotal < 0){
        $newSubtotal = 0;
    }  
    $newTax = $newSubtotal * $tax;
    $newTotal =  $newSubtotal*(1+$tax);
    return collect([
        'discount'=>$discount,
        'tax' => $tax,
        'code' => $code,
        'newSubtotal' => $newSubtotal,
        'newTax' => $newTax,
        'newTotal' => $newTotal
    ]);
}

function getNumbersCatering()
{
    $tax = config('cart.tax')/100;
    $discount = session()->get('coupon')['discount'] ?? 0;
    $code = session()->get('coupon')['name'];
    $newSubtotal = Cart::instance('catering')->subTotal() - $discount;
    if($newSubtotal < 0){
        $newSubtotal = 0;
    }  
    $newTax = $newSubtotal * $tax;
    $newTotal =  $newSubtotal*(1+$tax);
    return collect([
        'discount'=>$discount,
        'tax' => $tax,
        'code' => $code,
        'newSubtotal' => $newSubtotal,
        'newTax' => $newTax,
        'newTotal' => $newTotal
    ]);
}
function presentDate($date)
{
    return Carbon::parse($date)->format('M d, Y');
}

function setActiveCategory($category, $output = 'active')
{
    return request()->category == $category ? $output : '';
}
function formitizeProducts($products) 
{
    foreach($products as $key=>$product)
    {
        $products[$key]['wishlistColour'] ='';
        if(Auth::user()){
            $wishlist = Like::where('type',2)->where('parent_id',$product->id)->where('user_id',Auth::user()->id)->count();
            if($wishlist){
                $products[$key]['wishlistColour'] ='liked';          
            }
        
        }
        $ratingAverage = Rating::where('product_id',$product->id)->where('status','Approved')->avg('rating');
        $ratingAverage = round($ratingAverage,2);
        $products[$key]['rating']=$ratingAverage;

            $rating = round($ratingAverage);
            $rating_html ='<div class="starRev">
                <ul>';
                if(@$rating){
                    for($i=1; $i<$rating; $i++){
                        $rating_html .='<li><i title='.$ratingAverage.' class="fas fa-star"></i></li>';
                    }
                    
                    if($rating > $ratingAverage ){
                        $rating_html .='<li><i title='.$ratingAverage.' class="fas fa-star-half"></i></li>';
                    }else{
                    $rating_html .='<li><i title='.$ratingAverage.' class="fas fa-star"></i></li>';
                    }
                   }
                else{
                    $rating_html .='<li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li><li><i class="far fa-star"></i></li>';
                }
                $rating_html .='</ul></div>';
        $product['rating_html'] = $rating_html;

        $products[$key]['slug'] = route('shop.show',$product->slug);
        $products[$key]['image'] = imageUrl($product->image && file_exists('storage/'.$product->image) ? Voyager::image($product->image): asset('images/not-found.jpg'),null,'260') ;
        $products[$key]['wishlist'] = route('shop.wishlist',$product->id);
        
    } 
    return $products;
}
function imageUrl($path, $width = NULL, $height = NULL,$quality=NULL,$crop=NULL) {
 
    if(!$width && !$height) {
    $url = env('IMAGE_URL') . $path;
    } else {
    $url = url('/') . '/timthumb.php?src=' . env('IMAGE_URL') . $path;
    if(isset($width)) {
    $url .= '&w=' . $width; 
    }
    if(isset($height) && $height>0) {
    $url .= '&h=' .$height;
    }
    if(isset($crop))
    {
    $url .= '&zc='.$crop;
    }
    else
    {
    $url .= '&zc=1';
    }
   if(isset($quality))
    {
    $url .='&q='.$quality.'&s=1';
    }
    else
    {
    $url .='&q=95&s=1';
    }
   }
  
  return $url;
 }
