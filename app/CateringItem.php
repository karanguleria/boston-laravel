<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CateringItem extends Model
{
    static function getMenuItems($menuId,$count)
    {
        if($count){
     return CateringItem::where('category_id',$menuId)->take($count)->get();
        }else{
         return CateringItem::where('category_id',$menuId)->get();
     }
    }
}
