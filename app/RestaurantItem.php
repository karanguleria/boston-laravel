<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RestaurantItem extends Model
{
    static function getMenuItems($menuId,$count)
   {
       if($count){
    return RestaurantItem::where('category_id',$menuId)->take($count)->get();
       }else{
        return RestaurantItem::where('category_id',$menuId)->get();
    }
   }
}
