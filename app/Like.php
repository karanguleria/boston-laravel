<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    public function parent()
    {
        if($this->type==1){
        return  $this->belongsTo('App\Post','parent_id','id');
        }
        else{
        return  $this->belongsTo('App\Product','parent_id','id');
        }
    }
}
