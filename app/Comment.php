<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Comment extends Model
{
    protected $fillable = ['post_id','commnet','user_id'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }
}
