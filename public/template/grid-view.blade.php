<div class="listening-grid">
    <div class="row">
        <div class="col-12 col-sm-6 col-lg-6 col-xl-4 productGridMn" ng-repeat="x in data.products.data | orderBy:sortBy">
        <div class="produt-grid">
            <div class="listeningImg">                
                    <img style="height:260px" ng-src="{{x.image}}">
              
                <div class="productHover">
                    <button type="button"  data-id="{{x.id}}" class="btn btn-primary" data-toggle="modal" data-target="#product_view" ng-click="updateModel($event)">
                        <i  class="fas fa-eye"></i>
                    </button>
                    <button>
                        <a href="{{x.slug}}">  <i class="fas fa-link"></i> </a>
                    </button>
                </div>
            </div>
            <div class="listeningText">
            <h4><a href="{{x.slug}}">{{x.name | uppercase | limitTo:30}}</a></h4>
            <div class="ratingdiv" ng-bind-html="x.rating_html"></div>
            <h3>{{x.price | currency}}</h3>
            <a href="{{x.slug}}" class="view_btn">SHOP NOW</a>
            <!-- <ul>
                
            <li ng-show="{{loginuser}}">
                <a href="#" class="disabledAnchor" data-id="{{x.id}}" ng-click="triggerLike($event)"><i class="disabledAnchor classpost{{x.id}} {{x.wishlistColour}} fas fa-heart" aria-hidden="true"></i></a>
            </li>
                <li>
                </li>
            </ul> -->
            </div>
        </div>
        </div>
        <div class="col-12 text-center loadMore" ng-show="loadMoreShow">
        <a href="#" ng-model="productCounter" ng-click="setCounter()" class="view_btn disabledAnchor">Load more</a>
        </div>
    </div>
</div>