<div class="Details-grid">
    <div class="row">
        <div class="col-12 productGridMn" ng-repeat="x in data.products.data | orderBy:sortBy">
            <div class="produt-grid listViw">
                <div class="row no-gutters">
                <div class="col-12 col-lg-5 col-xl-4">
                    <div class="listeningImg">
                    <a href="{{x.slug}}">
                    <img ng-src="{{x.image}}">
                    </a>
                    </div>
                </div>
                <div class="col-12 col-lg-7 col-xl-8">  
                    <div class="listeningText text-left">
                        <div class="detail-Txt">
                            <h4><a href="{{x.slug}}">{{x.name | uppercase}}</a></h4>
                             <div class="ratingdiv" ng-bind-html="x.rating_html"></div>
                            <h3>{{x.price | currency}}</h3>
                            <a href="{{x.slug}}" class="view_btn">SHOP NOW</a>
                        </div>
                        <ul class="listbuttonHov">
                           <!--  <li ng-show="{{loginuser}}">
                            <a href="#" class="disabledAnchor" data-id="{{x.id}}" ng-click="triggerLike($event)"><i class="disabledAnchor classpost{{x.id}} {{x.wishlistColour}} fas fa-heart" aria-hidden="true"></i></a>
                        </li> -->
                            <li>
                            <button type="button"  data-id="{{x.id}}" class="btn btn-primary" data-toggle="modal" data-target="#product_view" ng-click="updateModel($event)">
                    <i  class="fas fa-eye"></i>
                </button>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>  
            </div>
        </div>
        <div class="col-12 text-center loadMore" ng-show="loadMoreShow">
            <a href="#" ng-model="productCounter" ng-click="setCounter()" class="view_btn disabledAnchor">Load more</a>
        </div>
    </div>
</div>