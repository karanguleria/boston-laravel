<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Pages
Route::get('/', 'LandingPageController@index')->name('landing-page');
Route::get('/menu', 'LandingPageController@menuIndex')->name('menu');
Route::view('/about', 'pages.about')->name('about');
Route::view('/location', 'pages.location')->name('location');
// Route::view('/contact-us', 'pages.contactUs')->name('contactUs');
Route::view('/thank-you', 'pages.thank-you')->name('thank-you');
Route::view('/return-policy', 'pages.return-policy')->name('returnPolicy');
Route::view('/faq', 'pages.faq')->name('faq');
Route::view('/privacy-policy', 'pages.privacy-policy')->name('privacyPolicy');
Route::view('/terms', 'pages.terms')->name('terms');
Route::post('/subscribe','LandingPageController@subscribe')->name('subscribe');
Route::get('/contact-us', 'LandingPageController@contactShow')->name('contactUs');
Route::post('/contact-us','LandingPageController@contactStore')->name('contactus.store');



// Media Section
Route::get('/blogs', 'BlogController@index')->name('blog.index');
Route::get('/blog/{blog}', 'BlogController@show')->name('blog.show');
Route::get('/media', 'BlogController@gallery')->name('blog.gallery');
Route::get('/videos', 'BlogController@videos')->name('blog.videos');
Route::get('/video/{video}', 'BlogController@video')->name('blog.video');
Route::get('/photos', 'BlogController@photos')->name('blog.photos');
Route::get('/photo/{photo}', 'BlogController@photo')->name('blog.photo');
Route::post('/comment/{post}','BlogController@commentStore')->name('blog.comment.store')->middleware('auth');
Route::post('/like/{post}','BlogController@likeStore')->name('blog.like.store')->middleware('auth');




// Shopping process
Route::get('/store', 'StoreController@index')->name('store.index');
Route::get('/shop', 'ShopController@index')->name('shop.index');
Route::get('/shopfilter', 'ShopController@filter')->name('shop.filter');
Route::get('/store/varientGet', 'ShopController@product')->name('shop.product');
Route::post('/store/varientRemove', 'ShopController@varientRemove')->name('shop.varientRemove');
Route::post('/store/varientAdd', 'ShopController@varientAdd')->name('shop.varientAdd');
Route::get('/shop/{product}', 'ShopController@show')->name('shop.show');
Route::post('/showajax', 'ShopController@showajax')->name('shop.showajax');
Route::post('/showajaxCatering', 'ShopController@showajaxCatering')->name('shop.showajax-catering');
Route::post('/wishlist','ShopController@wishlist')->name('shop.wishlist')->middleware('auth');
Route::post('/rating','ShopController@rating')->name('shop.rating')->middleware('auth');
Route::post('/review/{post}','ShopController@reviewStore')->name('shop.review')->middleware('auth');

//Shop Cart
Route::group(['prefix' => '/cart'],function(){
    Route::get('/', 'CartController@index')->name('cart.index');
    Route::post('/{product}', 'CartController@store')->name('cart.store');
    Route::patch('/{product}', 'CartController@update')->name('cart.update');
    Route::delete('/{product}', 'CartController@destroy')->name('cart.destroy');
});

// Shop Coupan code 
Route::post('/coupon', 'CouponsController@store')->name('coupon.store');
Route::delete('/coupon', 'CouponsController@destroy')->name('coupon.destroy');

// Shop Checkout
Route::get('/checkout', 'CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::get('/paypal-checkout', 'CheckoutController@paypalCheckout')->name('checkout.paypal')->middleware('auth');
Route::get('/paypal-checkout-success', 'CheckoutController@paypalCheckoutSuccess')->middleware('auth');
Route::get('/paypal-checkout-cancel', 'CheckoutController@paypalCheckoutCancel')->middleware('auth');
Route::get('/thankyou/{order}', 'ConfirmationController@show')->name('confirmation.index')->middleware('auth');



// Restaurant Menu 
Route::group(['prefix' => '/restaurant'],function(){
    Route::get('/category', 'RestaurantController@category')->name('restaurant.category');
    Route::get('/view/{item}', 'RestaurantController@show')->name('restaurant.show');
});

// Restaurant Cart
Route::group(['prefix' => '/restaurant-cart'],function(){
    Route::get('/', 'CartController@restaurantIndex')->name('cart.restaurant-index');
    Route::post('/{item}', 'CartController@restaurantStore')->name('cart.restaurant-store');
    Route::patch('/{item}', 'CartController@restaurantUpdate')->name('cart.restaurant-update');
    Route::delete('/{item}', 'CartController@restaurantDestroy')->name('cart.restaurant-destroy');
});

// Restaurant Checkout
Route::group(['prefix' => '/restaurant'],function(){
Route::get('/checkout', 'CheckoutController@restaurantIndex')->name('checkout.restaurant-index')->middleware('auth');
Route::get('/paypal-checkout', 'CheckoutController@restaurantPaypalCheckout')->name('checkout.restaurant-paypal')->middleware('auth');
Route::get('/paypal-checkout-success', 'CheckoutController@restaurantPaypalCheckoutSuccess')->name('checkout.restaurant-paypal-success')->middleware('auth');
Route::get('/paypal-checkout-cancel', 'CheckoutController@restaurantPaypalCheckoutCancel')->name('checkout.restaurant-paypal-cancel')->middleware('auth');
Route::get('/thankyou/{order}', 'ConfirmationController@restaurantShow')->name('confirmation.restaurant-index')->middleware('auth');
});


// Catering Menu
Route::group(['prefix' => '/catering'],function(){
    Route::get('/category', 'CateringController@category')->name('catering.category');
    Route::get('/view/{item}', 'CateringController@show')->name('catering.show');
});

// Catering Section
Route::get('/catering-categories', 'CateringController@categories')->name('catering.categories');
Route::get('/order-now', 'CateringController@orderNow')->name('catering.orderNow');
Route::get('/order-delivery', 'CateringController@orderDelivery')->name('catering.orderDelivery');
Route::view('/cateringCollection', 'catering.collection')->name('catering.collection');

// Catering Cart
Route::group(['prefix' => '/catering-cart'],function(){
    Route::get('/', 'CartController@cateringIndex')->name('cart.catering-index');
    Route::post('/{item}', 'CartController@cateringStore')->name('cart.catering-store');
    Route::patch('/{item}', 'CartController@cateringUpdate')->name('cart.catering-update');
    Route::delete('/{item}', 'CartController@cateringDestroy')->name('cart.catering-destroy');
});

// Catering Checkout
Route::group(['prefix' => '/catering'],function(){
    Route::get('/checkout', 'CheckoutController@cateringIndex')->name('checkout.catering-index')->middleware('auth');
    Route::get('/paypal-checkout', 'CheckoutController@cateringPaypalCheckout')->name('checkout.catering-paypal')->middleware('auth');
    Route::get('/paypal-checkout-success', 'CheckoutController@cateringPaypalCheckoutSuccess')->name('checkout.catering-paypal-success')->middleware('auth');
    Route::get('/paypal-checkout-cancel', 'CheckoutController@cateringPaypalCheckoutCancel')->name('checkout.catering-paypal-cancel')->middleware('auth');
    Route::get('/thankyou/{order}', 'ConfirmationController@cateringShow')->name('confirmation.catering-index')->middleware('auth');
    });


// My Account
Route::group(['prefix' => '/my-account','middleware'=>['auth']],function(){
    Route::get('/','AccountController@index')->name('myaccount.index');
    Route::patch('/update','AccountController@update')->name('myaccount.update');
    Route::patch('/updatePassword','AccountController@updatePassword')->name('myaccount.updatePassword');
});

Route::get('/order/{order}','OrderController@show')->name('orders.show');
// Route::get('/retailpartner','LandingPageController@retailIndex')->name('retailpartner.index');
// Route::post('/retailpartner','LandingPageController@retailStore')->name('retailpartner.store');

// Route::get('/thankyou', 'ConfirmationController@index')->name('confirmation.index')->middleware('auth');


Route::view('/pages', 'html.pages')->name('pages');

// Route::view('/location', 'html.location')->name('location');
// Route::view('/giftcard', 'html.giftcard')->name('giftcard');
// Route::view('/giveback', 'html.giveback')->name('giveback');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
