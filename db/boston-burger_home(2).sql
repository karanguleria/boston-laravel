-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 14, 2019 at 12:49 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `boston-burger_home`
--

-- --------------------------------------------------------

--
-- Table structure for table `accolades`
--

CREATE TABLE `accolades` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PUBLISHED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `accolades`
--

INSERT INTO `accolades` (`id`, `title`, `slug`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Mashable', 'mashable', 'accolades/January2019/Y6CKyO0tmxfhCDJaEf0M.png', 'Indulge in 2016\'s Sweetest Food Trend', 'PUBLISHED', '2019-01-09 06:14:39', '2019-01-09 06:14:39'),
(2, 'Zagat', 'zagat', 'accolades/January2019/2Ydo3an2YuVQIEDM19bl.png', '10 Boston Dishes Making America Great Again', 'PUBLISHED', '2019-01-09 06:15:20', '2019-01-09 06:15:20'),
(3, 'Yahoo', 'yahoo', 'accolades/January2019/d1T31PEGSjwds5SFDa1l.png', '32 Drinks to Add to Your Boston Bucket List', 'PUBLISHED', '2019-01-09 06:15:40', '2019-01-09 06:15:40'),
(4, 'Men\'s Health', 'men-s-health', 'accolades/January2019/HUwAGEZ6weCAty4zZcLP.png', 'The 25 Best Burgers in America', 'PUBLISHED', '2019-01-09 06:17:16', '2019-01-09 06:17:16'),
(5, 'Ocbs', 'ocbs', 'accolades/January2019/8cG6sKbkhDd3gQklS5U9.png', 'Freak Frappes at Boston Burger Company', 'PUBLISHED', '2019-01-09 06:17:38', '2019-01-09 06:17:38'),
(6, 'Trip Advider', 'trip-advider', 'accolades/January2019/Mb2tKZIxv0DDnekyb8X4.png', '4.5 Star<br>Rating', 'PUBLISHED', '2019-01-09 06:18:02', '2019-01-09 06:18:02');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2019-01-09 05:59:02', '2019-01-09 05:59:02'),
(2, NULL, 1, 'Category 2', 'category-2', '2019-01-09 05:59:02', '2019-01-09 05:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `data_rows`
--

CREATE TABLE `data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_rows`
--

INSERT INTO `data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, NULL, 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, NULL, 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, NULL, 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, NULL, 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":0}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, NULL, 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, NULL, 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, NULL, 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, NULL, 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, NULL, 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, NULL, 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, NULL, 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, NULL, 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, NULL, 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, NULL, 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, NULL, 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, NULL, 4),
(49, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, NULL, 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, NULL, 7),
(52, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, NULL, 8),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, NULL, 12),
(57, 7, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(58, 7, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(59, 7, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 3),
(60, 7, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 4),
(61, 7, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"PUBLISHED\",\"options\":{\"PUBLISHED\":\"PUBLISHED\",\"DRAFT\":\"DRAFT\",\"PENDING\":\"PENDING\"}}', 5),
(62, 7, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(63, 7, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(64, 8, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(65, 8, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(66, 8, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(67, 8, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(68, 8, 'description', 'text_area', 'Description', 1, 1, 1, 1, 1, 1, '{}', 5),
(69, 8, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"PUBLISHED\",\"options\":{\"PUBLISHED\":\"PUBLISHED\",\"DRAFT\":\"DRAFT\",\"PENDING\":\"PENDING\"}}', 6),
(70, 8, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(71, 8, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(72, 9, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(73, 9, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(74, 9, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(75, 9, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(76, 9, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{}', 5),
(77, 9, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"PUBLISHED\",\"options\":{\"PUBLISHED\":\"PUBLISHED\",\"DRAFT\":\"DRAFT\",\"PENDING\":\"PENDING\"}}', 6),
(78, 9, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 7),
(79, 9, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 8),
(80, 10, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(81, 10, 'food_menu_id', 'text', 'Food Menu Id', 1, 1, 1, 1, 1, 1, '{}', 2),
(82, 10, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 3),
(83, 10, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 4),
(84, 10, 'image', 'image', 'Image', 0, 0, 1, 1, 1, 1, '{}', 5),
(85, 10, 'description', 'text_area', 'Description', 0, 0, 1, 1, 1, 1, '{}', 6),
(86, 10, 'price', 'text', 'Price', 0, 0, 0, 0, 0, 0, '{}', 7),
(87, 10, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"PUBLISHED\",\"options\":{\"PUBLISHED\":\"PUBLISHED\",\"DRAFT\":\"DRAFT\",\"PENDING\":\"PENDING\"}}', 8),
(88, 10, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 9),
(89, 10, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 10),
(90, 10, 'food_item_belongsto_food_menu_relationship', 'relationship', 'Food Menu', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\FoodMenu\",\"table\":\"food_menus\",\"type\":\"belongsTo\",\"column\":\"food_menu_id\",\"key\":\"id\",\"label\":\"title\",\"pivot_table\":\"accolades\",\"pivot\":\"0\",\"taggable\":\"0\"}', 11),
(91, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(92, 11, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(93, 11, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(94, 11, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(95, 11, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"PUBLISHED\",\"options\":{\"PUBLISHED\":\"PUBLISHED\",\"DRAFT\":\"DRAFT\",\"PENDING\":\"PENDING\"}}', 5),
(96, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(97, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7),
(98, 12, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, '{}', 1),
(99, 12, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '{}', 2),
(100, 12, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}', 3),
(101, 12, 'image', 'image', 'Image', 0, 1, 1, 1, 1, 1, '{}', 4),
(102, 12, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"PUBLISHED\",\"options\":{\"PUBLISHED\":\"PUBLISHED\",\"DRAFT\":\"DRAFT\",\"PENDING\":\"PENDING\"}}', 5),
(103, 12, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, '{}', 6),
(104, 12, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '{}', 7);

-- --------------------------------------------------------

--
-- Table structure for table `data_types`
--

CREATE TABLE `data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `details` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `data_types`
--

INSERT INTO `data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 0, NULL, '2019-01-09 05:58:54', '2019-01-09 05:58:54'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2019-01-09 05:58:54', '2019-01-09 05:58:54'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2019-01-09 05:58:54', '2019-01-09 05:58:54'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2019-01-09 05:59:01', '2019-01-09 05:59:01'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2019-01-09 05:59:03', '2019-01-09 05:59:03'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(7, 'presses', 'presses', 'Press', 'Presses', NULL, 'App\\Press', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-01-09 06:02:22', '2019-01-09 06:08:10'),
(8, 'accolades', 'accolades', 'Accolade', 'Accolades', NULL, 'App\\Accolade', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-01-09 06:03:01', '2019-01-09 06:08:19'),
(9, 'food_menus', 'food-menus', 'Food Menu', 'Food Menus', NULL, 'App\\FoodMenu', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-01-09 06:05:05', '2019-01-09 06:35:40'),
(10, 'food_items', 'food-items', 'Food Item', 'Food Items', NULL, 'App\\FoodItem', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-01-09 06:09:23', '2019-01-11 01:55:28'),
(11, 'homepage_sliders', 'homepage-sliders', 'Homepage Slider', 'Homepage Sliders', NULL, 'App\\HomepageSlider', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-01-10 01:45:54', '2019-01-10 01:48:46'),
(12, 'retail_partners', 'retail-partners', 'Retail Partner', 'Retail Partners', NULL, 'App\\RetailPartner', NULL, NULL, NULL, 1, 0, '{\"order_column\":null,\"order_display_column\":null,\"order_direction\":\"asc\",\"default_search_key\":null}', '2019-01-10 13:40:55', '2019-01-10 13:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `food_items`
--

CREATE TABLE `food_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `food_menu_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `price` double(8,2) DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PUBLISHED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food_items`
--

INSERT INTO `food_items` (`id`, `food_menu_id`, `title`, `slug`, `image`, `description`, `price`, `status`, `created_at`, `updated_at`) VALUES
(2, '1', 'BIG PAPI BURGER', 'big-papi-burger', 'food-items/January2019/PJ3APi7ATpWgz1XvK3oF.png', 'smoked bacon, griddled hot dog, fried egg*, guacamole, pickled red onions, lettuce, tomato, Papi sauce - *a portion of proceeds will be donated to The David Ortiz Children’s Fund* 17.50', NULL, 'PUBLISHED', '2019-01-09 23:39:00', '2019-01-11 01:54:49'),
(4, '1', 'THE KITCHEN SINK', 'the-kitchen-sink', NULL, 'fried egg, ham, bacon, sautéed peppers, mushrooms, onions, american, cheddar, provolone, BBC sauce', NULL, 'PUBLISHED', '2019-01-11 01:55:45', '2019-01-11 01:55:45'),
(6, '1', 'THE KING', 'the-king', NULL, 'peanut butter, bacon, fried bananas dusted in cinnamon & sugar', NULL, 'PUBLISHED', '2019-01-11 01:56:28', '2019-01-11 01:56:28'),
(7, '1', 'BOSTON BURGER', 'boston-burger', NULL, '(with or without cheese) lettuce, tomato, onions', NULL, 'PUBLISHED', '2019-01-11 01:56:49', '2019-01-11 01:56:49'),
(8, '1', 'NORTHENDER', 'northender', NULL, 'fresh mozzarella, tomato, balsamic mayo', NULL, 'PUBLISHED', '2019-01-11 01:57:04', '2019-01-11 01:57:04'),
(9, '1', 'THE HOT MESS', 'the-hot-mess', NULL, 'bacon, sweet potato fries, homemade thousand island dressing, diced pickles, jalapeño, red onion, lettuce, american *featured on the Rachael Ray Show', NULL, 'PUBLISHED', '2019-01-11 01:57:24', '2019-01-11 01:57:24'),
(10, '1', 'THE SOPHIE', 'the-sophie', NULL, 'prosciutto, goat cheese, candied walnuts, fig jam, arugula tossed in a lemon vinaigrette, balsamic reduction', NULL, 'PUBLISHED', '2019-01-11 01:57:53', '2019-01-11 01:57:53'),
(11, '1', 'MAC ATTACK', 'mac-attack', 'food-items/January2019/zf7R7r0jiVo1OJCTEaae.png', 'homemade 4-cheese mac & cheese, bacon *featured on Diner\'s, Drive-Ins, and Dives', NULL, 'PUBLISHED', '2019-01-11 01:58:31', '2019-01-11 01:58:31'),
(12, '1', 'GREEN MONSTAH', 'green-monstah', NULL, 'guac, pic de gallo, cheddar jack', NULL, 'PUBLISHED', '2019-01-11 01:58:46', '2019-01-11 01:58:46'),
(13, '1', 'WHISKEY TANGO FOXTROT', 'whiskey-tango-foxtrot', NULL, 'pulled pork, mac & cheese, onion ring, pickles, bbq sauce', NULL, 'PUBLISHED', '2019-01-11 01:59:28', '2019-01-11 01:59:28'),
(14, '1', 'THE JUMBO', 'the-jumbo', NULL, 'a pound of double-stacked beef, cheddar, sautéed onions, bacon, bbq sauce', NULL, 'PUBLISHED', '2019-01-11 01:59:52', '2019-01-11 01:59:52'),
(15, '1', 'VERMONSTER', 'vermonster', NULL, 'sautéed apples & red onion, bacon, maple mayo, sharp cheddar', NULL, 'PUBLISHED', '2019-01-11 02:00:09', '2019-01-11 02:00:09'),
(16, '1', 'ALPINE', 'alpine', NULL, 'garlic parm mushrooms, swiss', NULL, 'PUBLISHED', '2019-01-11 02:00:41', '2019-01-11 02:00:41'),
(17, '1', 'ARTERY CLOGGER', 'artery-clogger', NULL, 'beer-battered and deep-fried burger, bacon, bbq sauce, american', NULL, 'PUBLISHED', '2019-01-11 02:01:01', '2019-01-11 02:01:01'),
(18, '1', '420 BURGER', '420-burger', NULL, 'mozzarella sticks, fried mac & cheese, onion rings, fries, bacon, golden bbq sauce, american', NULL, 'PUBLISHED', '2019-01-11 02:01:26', '2019-01-11 02:01:26'),
(19, '1', 'ALL AMERICAN', 'all-american', NULL, 'bacon, bbq sauce, creamy blue cheese, american cheese, lettuce, tomato', NULL, 'PUBLISHED', '2019-01-11 02:02:52', '2019-01-11 02:02:52'),
(20, '1', 'WAIKIKI BEACH', 'waikiki-beach', NULL, 'grilled pineapple, ham, bacon, teriyaki', NULL, 'PUBLISHED', '2019-01-11 02:02:56', '2019-01-11 02:02:56'),
(21, '1', 'KILLER BEE', 'killer-bee', NULL, 'bacon, a stack of beer-battered onion rings, honey bbq sauce, american cheese', NULL, 'PUBLISHED', '2019-01-11 02:03:00', '2019-01-11 02:03:00'),
(22, '1', 'BURGER BOMB', 'burger-bomb', NULL, 'sautéed peppers, mushrooms, and onions, american', NULL, 'PUBLISHED', '2019-01-11 02:04:25', '2019-01-11 02:04:25'),
(23, '1', 'THE TEXAN', 'the-texan', NULL, 'homemade chili, coleslaw, onion, cheddar', NULL, 'PUBLISHED', '2019-01-11 02:05:05', '2019-01-11 02:05:05'),
(24, '2', 'STRAWBERRY SHORTCAKE', 'strawberry-shortcake', NULL, 'vanilla ice cream, strawberry jam, strawberries, shortcake', NULL, 'PUBLISHED', '2019-01-11 02:05:41', '2019-01-11 02:05:41'),
(25, '3', 'CHICKEN PANINI', 'chicken-panini', NULL, 'fresh mozzarella, roasted red peppers, tomato, mixed greens, balsamic vinaigrette', NULL, 'PUBLISHED', '2019-01-11 02:06:07', '2019-01-11 02:06:07'),
(26, '4', 'PIG PILE', 'pig-pile', NULL, 'homemade potato chips, pulled pork, bacon, sausage, cole slaw, chopped pickles, melted cheddar jack cheese and drizzled with 2 types of bbq and homemade ranch', NULL, 'PUBLISHED', '2019-01-11 02:06:36', '2019-01-11 02:06:36'),
(27, '5', 'GARDEN', 'garden', NULL, 'tomato, cucumber, carrot, red onion, iceberg lettuce', NULL, 'PUBLISHED', '2019-01-11 02:07:01', '2019-01-11 02:07:01');

-- --------------------------------------------------------

--
-- Table structure for table `food_menus`
--

CREATE TABLE `food_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PUBLISHED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `food_menus`
--

INSERT INTO `food_menus` (`id`, `title`, `slug`, `image`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'BURGERS', 'burgers', 'food-menus/January2019/p2ZzueBHg3ud1Lfccc3p.jpg', 'All burgers and sandwiches are served with homemade potato chips, and choice of baked beans or slaw. Any burger can be served as a bowl. Sub any burger with a turkey or veggie patty: +$1', 'PUBLISHED', '2019-01-09 06:35:26', '2019-01-09 06:35:26'),
(2, '#FREAKFRAPPES', 'freakfrappes', 'food-menus/January2019/Wnivb3uMXYnlkktVS9O3.jpg', NULL, 'PUBLISHED', '2019-01-09 06:36:25', '2019-01-09 06:36:25'),
(3, 'SANDWICHES', 'sandwiches', 'food-menus/January2019/ULzKJ6GM0jNaGGHM6XLu.jpg', 'All $13.50. Choose from: grilled or fried chicken breast and either deli roll or focaccia', 'PUBLISHED', '2019-01-09 06:36:59', '2019-01-09 06:36:59'),
(4, 'APPETIZERS', 'appetizers', 'food-menus/January2019/M7SPgPprU936GOTWxhmB.jpg', NULL, 'PUBLISHED', '2019-01-09 06:37:29', '2019-01-09 06:37:29'),
(5, 'SALADS', 'salads', 'food-menus/January2019/rKG0MWVj5wGBzC6164QZ.jpg', 'add a burger patty or grilled chicken +4.00, add fried chicken +5.00. dressings: italian, caesar, greek, balsamic vinaigrette, blue cheese, ranch, 1000 island, lemon vinaigrette', 'PUBLISHED', '2019-01-09 06:38:01', '2019-01-09 06:38:01'),
(6, 'FRIES', 'fries', 'food-menus/January2019/l5eAXk2Xg6f5H9KJiPpo.jpg', NULL, 'PUBLISHED', '2019-01-09 06:38:00', '2019-01-14 01:47:02');

-- --------------------------------------------------------

--
-- Table structure for table `homepage_sliders`
--

CREATE TABLE `homepage_sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PUBLISHED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homepage_sliders`
--

INSERT INTO `homepage_sliders` (`id`, `title`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Chocolate Frappe', 'chocolate-frappe', 'homepage-sliders/January2019/eCSxhWMzEC7YJ7ol1F12.png', 'PUBLISHED', '2019-01-10 01:49:18', '2019-01-10 01:49:18'),
(2, 'MAC ATTACK<br>burger', 'mac-attack-br-burger', 'homepage-sliders/January2019/vvW767aOKAhudJ0WMiGQ.png', 'PUBLISHED', '2019-01-10 01:49:47', '2019-01-10 01:49:47'),
(3, 'NUTELLA Frappe', 'nutella-frappe', 'homepage-sliders/January2019/j6oz4R24jNBOLaf7oKyl.png', 'PUBLISHED', '2019-01-10 01:50:03', '2019-01-10 01:50:03'),
(4, 'HOT MESS<br>burger', 'hot-mess-br-burger', 'homepage-sliders/January2019/MUxXclq2vmLpNbDwSwK3.png', 'PUBLISHED', '2019-01-10 01:50:31', '2019-01-10 01:50:31'),
(5, 'Milk Chocolately<br>Chance', 'milk-chocolately-br-chance', 'homepage-sliders/January2019/pyRivM0S3vKeim4TvaPD.png', 'PUBLISHED', '2019-01-10 01:50:51', '2019-01-10 01:50:51'),
(6, 'NUTELLA Frappes', 'nutella-frappes', 'homepage-sliders/January2019/3Gxjhz602wMhLCnzzRom.png', 'PUBLISHED', '2019-01-10 02:16:00', '2019-01-10 02:18:16');

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2019-01-09 05:58:56', '2019-01-09 05:58:56'),
(2, 'site', '2019-01-10 04:22:33', '2019-01-10 04:22:33'),
(3, 'footer left', '2019-01-10 04:28:54', '2019-01-10 04:28:54'),
(4, 'footer middle', '2019-01-10 04:29:08', '2019-01-10 04:29:08'),
(5, 'footer right', '2019-01-10 04:29:18', '2019-01-10 04:29:18');

-- --------------------------------------------------------

--
-- Table structure for table `menu_items`
--

CREATE TABLE `menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu_items`
--

INSERT INTO `menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2019-01-09 05:58:56', '2019-01-09 05:58:56', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 5, '2019-01-09 05:58:56', '2019-01-09 05:58:56', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2019-01-09 05:58:56', '2019-01-09 05:58:56', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2019-01-09 05:58:56', '2019-01-09 05:58:56', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 9, '2019-01-09 05:58:57', '2019-01-09 05:58:57', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 10, '2019-01-09 05:58:57', '2019-01-09 05:58:57', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 11, '2019-01-09 05:58:57', '2019-01-09 05:58:57', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 12, '2019-01-09 05:58:57', '2019-01-09 05:58:57', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 13, '2019-01-09 05:58:57', '2019-01-09 05:58:57', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 14, '2019-01-09 05:58:57', '2019-01-09 05:58:57', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 8, '2019-01-09 05:59:02', '2019-01-09 05:59:02', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 6, '2019-01-09 05:59:04', '2019-01-09 05:59:04', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 7, '2019-01-09 05:59:05', '2019-01-09 05:59:05', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 13, '2019-01-09 05:59:09', '2019-01-09 05:59:09', 'voyager.hooks', NULL),
(15, 1, 'Presses', '', '_self', NULL, NULL, NULL, 15, '2019-01-09 06:02:22', '2019-01-09 06:02:22', 'voyager.presses.index', NULL),
(16, 1, 'Accolades', '', '_self', NULL, NULL, NULL, 16, '2019-01-09 06:03:01', '2019-01-09 06:03:01', 'voyager.accolades.index', NULL),
(17, 1, 'Food Menus', '', '_self', NULL, NULL, NULL, 17, '2019-01-09 06:05:06', '2019-01-09 06:05:06', 'voyager.food-menus.index', NULL),
(18, 1, 'Food Items', '', '_self', NULL, NULL, NULL, 18, '2019-01-09 06:09:23', '2019-01-09 06:09:23', 'voyager.food-items.index', NULL),
(19, 1, 'Homepage Sliders', '', '_self', NULL, NULL, NULL, 19, '2019-01-10 01:45:54', '2019-01-10 01:45:54', 'voyager.homepage-sliders.index', NULL),
(20, 2, 'ABOUT', '', '_self', NULL, '#000000', NULL, 20, '2019-01-10 04:23:00', '2019-01-10 12:41:55', 'about', 'null'),
(21, 2, 'MENU', '', '_self', NULL, '#000000', NULL, 21, '2019-01-10 04:23:29', '2019-01-10 12:42:22', 'menu', 'null'),
(22, 2, 'GALLERY', '', '_self', NULL, '#000000', NULL, 22, '2019-01-10 04:23:52', '2019-01-10 12:42:38', 'gallery', 'null'),
(23, 2, 'CATERING', '', '_self', NULL, '#000000', NULL, 23, '2019-01-10 04:24:04', '2019-01-14 02:15:31', 'catering.index', 'null'),
(24, 2, 'STORE', '', '_self', NULL, '#000000', NULL, 24, '2019-01-10 04:24:18', '2019-01-10 12:42:59', 'store.index', 'null'),
(25, 5, 'INSTAGRAM', 'https://www.instagram.com', '_blank', NULL, '#000000', NULL, 25, '2019-01-10 04:29:41', '2019-01-10 13:05:50', NULL, ''),
(26, 5, 'TWITTER', 'https://twitter.com/', '_blank', NULL, '#000000', NULL, 26, '2019-01-10 04:29:52', '2019-01-10 13:05:55', NULL, ''),
(27, 5, 'FACEBOOK', 'http://facebook.com/', '_blank', NULL, '#000000', NULL, 27, '2019-01-10 04:30:00', '2019-01-10 13:06:08', NULL, ''),
(28, 5, 'YOUTUBE', 'http://youtube.com/', '_blank', NULL, '#000000', NULL, 28, '2019-01-10 04:30:13', '2019-01-10 13:06:22', NULL, ''),
(29, 5, 'EMAIL US', 'mailto:catering@bostonburgerco.com', '_self', NULL, '#000000', NULL, 29, '2019-01-10 04:30:22', '2019-01-10 13:06:56', NULL, ''),
(30, 4, 'GIFT CARDS', '', '_self', NULL, '#000000', NULL, 30, '2019-01-10 04:30:57', '2019-01-10 13:02:38', 'giftcard', 'null'),
(31, 4, 'BLOG', '', '_self', NULL, '#000000', NULL, 31, '2019-01-10 04:31:05', '2019-01-10 13:00:44', 'blog.index', 'null'),
(32, 4, 'LOCATIONS', '', '_self', NULL, '#000000', NULL, 32, '2019-01-10 04:31:13', '2019-01-10 13:01:55', 'location', 'null'),
(33, 4, 'GIVE BACK', '', '_self', NULL, '#000000', NULL, 33, '2019-01-10 04:31:23', '2019-01-10 13:03:37', 'giveback', 'null'),
(34, 3, 'ABOUT', '', '_self', NULL, '#000000', NULL, 34, '2019-01-10 04:31:38', '2019-01-10 12:43:35', 'about', 'null'),
(35, 3, 'MENU', '', '_self', NULL, '#000000', NULL, 35, '2019-01-10 04:31:47', '2019-01-10 12:43:46', 'menu', 'null'),
(36, 3, 'GALLERY', '', '_self', NULL, '#000000', NULL, 36, '2019-01-10 04:31:55', '2019-01-10 12:43:56', 'gallery', 'null'),
(37, 3, 'CATERING', '', '_self', NULL, '#000000', NULL, 37, '2019-01-10 04:32:03', '2019-01-10 12:44:08', 'catering', 'null'),
(38, 3, 'STORE', '', '_self', NULL, '#000000', NULL, 38, '2019-01-10 04:32:10', '2019-01-10 12:44:23', 'store.index', 'null'),
(39, 3, 'ORDER NOW', '', '_self', NULL, '#000000', NULL, 39, '2019-01-10 04:32:20', '2019-01-10 12:44:54', 'order', 'null'),
(40, 1, 'Retail Partners', '', '_self', NULL, NULL, NULL, 40, '2019-01-10 13:40:55', '2019-01-10 13:40:55', 'voyager.retail-partners.index', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_01_01_000000_add_voyager_user_fields', 1),
(4, '2016_01_01_000000_create_data_types_table', 1),
(5, '2016_01_01_000000_create_pages_table', 1),
(6, '2016_01_01_000000_create_posts_table', 1),
(7, '2016_02_15_204651_create_categories_table', 1),
(8, '2016_05_19_173453_create_menu_table', 1),
(9, '2016_10_21_190000_create_roles_table', 1),
(10, '2016_10_21_190000_create_settings_table', 1),
(11, '2016_11_30_135954_create_permission_table', 1),
(12, '2016_11_30_141208_create_permission_role_table', 1),
(13, '2016_12_26_201236_data_types__add__server_side', 1),
(14, '2017_01_13_000000_add_route_to_menu_items_table', 1),
(15, '2017_01_14_005015_create_translations_table', 1),
(16, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 1),
(17, '2017_03_06_000000_add_controller_to_data_types_table', 1),
(18, '2017_04_11_000000_alter_post_nullable_fields_table', 1),
(19, '2017_04_21_000000_add_order_to_data_rows_table', 1),
(20, '2017_07_05_210000_add_policyname_to_data_types_table', 1),
(21, '2017_08_05_000000_add_group_to_settings_table', 1),
(22, '2017_11_26_013050_add_user_role_relationship', 1),
(23, '2017_11_26_015000_create_user_roles_table', 1),
(24, '2018_03_11_000000_add_user_settings', 1),
(25, '2018_03_14_000000_add_details_to_data_types_table', 1),
(26, '2018_03_16_000000_make_settings_value_nullable', 1),
(27, '2019_01_09_110801_create_presses_table', 1),
(28, '2019_01_09_112501_create_accolades_table', 2),
(30, '2019_01_09_112754_create_food_items_table', 3),
(31, '2018_08_08_100000_create_telescope_entries_table', 4),
(32, '2019_01_09_112740_create_food_menus_table', 5),
(34, '2019_01_10_071255_create_homepage_sliders_table', 6),
(35, '2019_01_10_172250_create_retail_partners_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2019-01-09 05:59:06', '2019-01-09 05:59:06');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2019-01-09 05:58:57', '2019-01-09 05:58:57'),
(2, 'browse_bread', NULL, '2019-01-09 05:58:57', '2019-01-09 05:58:57'),
(3, 'browse_database', NULL, '2019-01-09 05:58:57', '2019-01-09 05:58:57'),
(4, 'browse_media', NULL, '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(5, 'browse_compass', NULL, '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(6, 'browse_menus', 'menus', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(7, 'read_menus', 'menus', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(8, 'edit_menus', 'menus', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(9, 'add_menus', 'menus', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(10, 'delete_menus', 'menus', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(11, 'browse_roles', 'roles', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(12, 'read_roles', 'roles', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(13, 'edit_roles', 'roles', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(14, 'add_roles', 'roles', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(15, 'delete_roles', 'roles', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(16, 'browse_users', 'users', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(17, 'read_users', 'users', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(18, 'edit_users', 'users', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(19, 'add_users', 'users', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(20, 'delete_users', 'users', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(21, 'browse_settings', 'settings', '2019-01-09 05:58:58', '2019-01-09 05:58:58'),
(22, 'read_settings', 'settings', '2019-01-09 05:58:59', '2019-01-09 05:58:59'),
(23, 'edit_settings', 'settings', '2019-01-09 05:58:59', '2019-01-09 05:58:59'),
(24, 'add_settings', 'settings', '2019-01-09 05:58:59', '2019-01-09 05:58:59'),
(25, 'delete_settings', 'settings', '2019-01-09 05:58:59', '2019-01-09 05:58:59'),
(26, 'browse_categories', 'categories', '2019-01-09 05:59:02', '2019-01-09 05:59:02'),
(27, 'read_categories', 'categories', '2019-01-09 05:59:02', '2019-01-09 05:59:02'),
(28, 'edit_categories', 'categories', '2019-01-09 05:59:02', '2019-01-09 05:59:02'),
(29, 'add_categories', 'categories', '2019-01-09 05:59:02', '2019-01-09 05:59:02'),
(30, 'delete_categories', 'categories', '2019-01-09 05:59:02', '2019-01-09 05:59:02'),
(31, 'browse_posts', 'posts', '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(32, 'read_posts', 'posts', '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(33, 'edit_posts', 'posts', '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(34, 'add_posts', 'posts', '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(35, 'delete_posts', 'posts', '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(36, 'browse_pages', 'pages', '2019-01-09 05:59:05', '2019-01-09 05:59:05'),
(37, 'read_pages', 'pages', '2019-01-09 05:59:05', '2019-01-09 05:59:05'),
(38, 'edit_pages', 'pages', '2019-01-09 05:59:05', '2019-01-09 05:59:05'),
(39, 'add_pages', 'pages', '2019-01-09 05:59:05', '2019-01-09 05:59:05'),
(40, 'delete_pages', 'pages', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(41, 'browse_hooks', NULL, '2019-01-09 05:59:09', '2019-01-09 05:59:09'),
(42, 'browse_presses', 'presses', '2019-01-09 06:02:22', '2019-01-09 06:02:22'),
(43, 'read_presses', 'presses', '2019-01-09 06:02:22', '2019-01-09 06:02:22'),
(44, 'edit_presses', 'presses', '2019-01-09 06:02:22', '2019-01-09 06:02:22'),
(45, 'add_presses', 'presses', '2019-01-09 06:02:22', '2019-01-09 06:02:22'),
(46, 'delete_presses', 'presses', '2019-01-09 06:02:22', '2019-01-09 06:02:22'),
(47, 'browse_accolades', 'accolades', '2019-01-09 06:03:01', '2019-01-09 06:03:01'),
(48, 'read_accolades', 'accolades', '2019-01-09 06:03:01', '2019-01-09 06:03:01'),
(49, 'edit_accolades', 'accolades', '2019-01-09 06:03:01', '2019-01-09 06:03:01'),
(50, 'add_accolades', 'accolades', '2019-01-09 06:03:01', '2019-01-09 06:03:01'),
(51, 'delete_accolades', 'accolades', '2019-01-09 06:03:01', '2019-01-09 06:03:01'),
(52, 'browse_food_menus', 'food_menus', '2019-01-09 06:05:05', '2019-01-09 06:05:05'),
(53, 'read_food_menus', 'food_menus', '2019-01-09 06:05:05', '2019-01-09 06:05:05'),
(54, 'edit_food_menus', 'food_menus', '2019-01-09 06:05:05', '2019-01-09 06:05:05'),
(55, 'add_food_menus', 'food_menus', '2019-01-09 06:05:05', '2019-01-09 06:05:05'),
(56, 'delete_food_menus', 'food_menus', '2019-01-09 06:05:06', '2019-01-09 06:05:06'),
(57, 'browse_food_items', 'food_items', '2019-01-09 06:09:23', '2019-01-09 06:09:23'),
(58, 'read_food_items', 'food_items', '2019-01-09 06:09:23', '2019-01-09 06:09:23'),
(59, 'edit_food_items', 'food_items', '2019-01-09 06:09:23', '2019-01-09 06:09:23'),
(60, 'add_food_items', 'food_items', '2019-01-09 06:09:23', '2019-01-09 06:09:23'),
(61, 'delete_food_items', 'food_items', '2019-01-09 06:09:23', '2019-01-09 06:09:23'),
(62, 'browse_homepage_sliders', 'homepage_sliders', '2019-01-10 01:45:54', '2019-01-10 01:45:54'),
(63, 'read_homepage_sliders', 'homepage_sliders', '2019-01-10 01:45:54', '2019-01-10 01:45:54'),
(64, 'edit_homepage_sliders', 'homepage_sliders', '2019-01-10 01:45:54', '2019-01-10 01:45:54'),
(65, 'add_homepage_sliders', 'homepage_sliders', '2019-01-10 01:45:54', '2019-01-10 01:45:54'),
(66, 'delete_homepage_sliders', 'homepage_sliders', '2019-01-10 01:45:54', '2019-01-10 01:45:54'),
(67, 'browse_retail_partners', 'retail_partners', '2019-01-10 13:40:55', '2019-01-10 13:40:55'),
(68, 'read_retail_partners', 'retail_partners', '2019-01-10 13:40:55', '2019-01-10 13:40:55'),
(69, 'edit_retail_partners', 'retail_partners', '2019-01-10 13:40:55', '2019-01-10 13:40:55'),
(70, 'add_retail_partners', 'retail_partners', '2019-01-10 13:40:55', '2019-01-10 13:40:55'),
(71, 'delete_retail_partners', 'retail_partners', '2019-01-10 13:40:55', '2019-01-10 13:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2019-01-09 05:59:04', '2019-01-09 05:59:04'),
(5, 1, 1, '10 Tips for Making the Perfect Burger', NULL, NULL, '<p>Here at BBC, we\'re pretty picky about how we make our burgers. For good reason, that is. To make sure YOU have the best, tastiest experience at our restaurants. Whether you\'re going with the All American burger (it\'s a classic!) or as wild as the WTF (we named it that for a reason), here\'s how we make them awesome.</p>\r\n<ol id=\"yui_3_17_2_1_1547205167985_359\">\r\n<li>\r\n<p><strong>Use 80/20 ground chuck.</strong> A relatively high fat content guarantees a juicy burger. Trust us, it&rsquo;s worth the splurge. If you&rsquo;re going to eat a burger, it might as well taste damn good.</p>\r\n</li>\r\n<li>\r\n<p><strong>Make a thumbprint in the middle of the patty before cooking.</strong> Use your thumb to make an impression in the center of the burger to prevent it from puffing up. Otherwise, you get a plump center and you&rsquo;ll want to smash it down -- a big no no, especially while cooking!</p>\r\n</li>\r\n<li>\r\n<p><strong>Season the burger well.</strong> We use a special rub here at Boston Burger Company, but at the very least, you should liberally sprinkle your patty with salt and pepper.</p>\r\n</li>\r\n<li>\r\n<p><strong>Cook your burger over high heat.</strong> Make sure your grill or pan is hot hot hot. This will produce a really great crust. Allow the burger to cook on the first side for 3-5 minutes until you...</p>\r\n</li>\r\n<li>\r\n<p><strong>Flip once!</strong> And only once. But don&rsquo;t rush the process. A perfect crust will take at least 3 minutes. Otherwise your meat will fall apart and you&rsquo;ll lose that perfect round shape. It needs to fit the bun, after all.&nbsp;Flip the burger and continue cooking until a crust has formed on the bottom and the burger is cooked to your desired doneness. An additional 4 minutes will give you a medium-cooked burger -- chef&rsquo;s preference!</p>\r\n</li>\r\n<li>\r\n<p><strong>Know how long to cook it.</strong> We here at BBC prefer a burger that is pink and juicy in the middle -- somewhere between medium-rare and medium. Here&rsquo;s some cooking times to help you during the process. Rare: 6 minutes. Medium-rare: 7 minutes. Medium: 8 minutes. Medium-well: 9 minutes. Well-done: 11 minutes.</p>\r\n</li>\r\n<li>\r\n<p><strong>Don&rsquo;t press the burger!</strong> Do not, we repeat, do NOT press the burger down with the spatula as it cooks. Pressing down on it will squeeze out all the delicious juices, taking all the moisture and flavor with it. For the love of everything burgers, just let the patty sit and cook. &nbsp;</p>\r\n</li>\r\n<li>\r\n<p><strong>Add cheese! </strong>And maybe more than one type. A mix of two cheeses will really intensify the burger&rsquo;s overall flavor. A good ol&rsquo; slice of American is perfect, but if you feel like spicing things up with a slice of Cheddar Jack -- do your thing!</p>\r\n</li>\r\n<li>\r\n<p><strong>Add some steam to help the cheese melt.</strong> Once you&rsquo;ve topped the burger patty with cheese, add a few splashes of water and immediately cover the pan. Allow the water to steam and, for about 30 seconds, let cheese completely melt. There&rsquo;s nothing better than a burger with a patty covered in warm, gooey cheese.</p>\r\n</li>\r\n<li id=\"yui_3_17_2_1_1547205167985_358\">\r\n<p id=\"yui_3_17_2_1_1547205167985_357\"><strong>Use soft buns.</strong> When you lift your burger to the face, a soft bun molds itself around the patty. This ensures the best burger to bun ratio for each bite. No forks allowed!</p>\r\n</li>\r\n</ol>', 'posts/January2019/jZGwFgFjz5f0gAB7F897.JPG', '10-tips-for-making-the-perfect-burger', NULL, NULL, 'PUBLISHED', 0, '2019-01-11 05:46:36', '2019-01-11 05:46:36');

-- --------------------------------------------------------

--
-- Table structure for table `presses`
--

CREATE TABLE `presses` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PUBLISHED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `presses`
--

INSERT INTO `presses` (`id`, `title`, `image`, `slug`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Food Network', 'presses/January2019/aJ987xmdMuu4loe1rWn4.png', 'food-network', 'PUBLISHED', '2019-01-09 06:12:30', '2019-01-09 06:12:30'),
(2, 'Rachael Ray', 'presses/January2019/DEdtmptf6UjqZtJiULEY.png', 'rachael-ray', 'PUBLISHED', '2019-01-09 06:13:08', '2019-01-09 06:13:08'),
(3, 'Huffpost', 'presses/January2019/JCoJR5jLa08CXuf7CEzd.png', 'huffpost', 'PUBLISHED', '2019-01-09 06:13:27', '2019-01-09 06:13:27'),
(4, 'Esquire', 'presses/January2019/1i84Nq95Fqh5tva4SbaJ.png', 'esquire', 'PUBLISHED', '2019-01-09 06:13:47', '2019-01-09 06:13:47'),
(5, 'bravo', 'presses/January2019/br5tg2vo4xkHXfCnBdOy.png', 'bravo', 'PUBLISHED', '2019-01-09 06:14:02', '2019-01-09 06:14:02');

-- --------------------------------------------------------

--
-- Table structure for table `retail_partners`
--

CREATE TABLE `retail_partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'PUBLISHED',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `retail_partners`
--

INSERT INTO `retail_partners` (`id`, `title`, `slug`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Wholes Food Market', 'wholes-food-market', 'retail-partners/January2019/Pt0iZHgmBdf1ye08WglR.png', 'PUBLISHED', '2019-01-10 13:41:00', '2019-01-11 00:52:30'),
(2, 'Star market', 'star-market', 'retail-partners/January2019/4G3jjlVhYVnY7tsYacLD.png', 'PUBLISHED', '2019-01-10 13:42:00', '2019-01-11 00:52:11'),
(3, 'Hannaford', 'hannaford', 'retail-partners/January2019/TYOCVSCZd4XyqnPnwAuL.png', 'PUBLISHED', '2019-01-10 13:42:00', '2019-01-11 00:52:02'),
(4, 'Rache Bro', 'rache-bro', 'retail-partners/January2019/gk9P1I45Ww4acATzS5eU.png', 'PUBLISHED', '2019-01-10 13:43:00', '2019-01-11 00:51:48');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2019-01-09 05:58:57', '2019-01-09 05:58:57'),
(2, 'user', 'Normal User', '2019-01-09 05:58:57', '2019-01-09 05:58:57');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'BOSTON BURGER COMPANY', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Boston Burger Company', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', 'settings/January2019/9Y6y6nHR5XqD3LJ09ubk.png', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', NULL, '', 'text', 6, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Admin Panel', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Boston Burger Company', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', 'settings/January2019/fQXprTk9Ed78HB80ILiW.png', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', NULL, '', 'text', 1, 'Admin'),
(11, 'site.copyright_text', 'Copyright Text', '2019 All Rights Reserved', NULL, 'text', 4, 'Site'),
(12, 'social-links.facebook', 'facebook', 'https://www.facebook.com/', NULL, 'text', 8, 'Social Links'),
(13, 'social-links.youtube', 'Youtube', 'https://www.youtube.com/', NULL, 'text', 7, 'Social Links'),
(14, 'social-links.linkedin', 'Linkedin', 'https://in.linkedin.com/', NULL, 'text', 9, 'Social Links'),
(15, 'social-links.twitter', 'Twitter', 'https://twitter.com', NULL, 'text', 10, 'Social Links'),
(16, 'social-links.instagram', 'Instagram', 'https://www.instagram.com/', NULL, 'text', 11, 'Social Links'),
(24, 'home-page.banner-img', 'Banner Image', 'settings/January2019/n6lYAnvUyn5JOepsrusJ.png', NULL, 'image', 14, 'Home Page'),
(25, 'home-page.catering-image', 'Catering image', 'settings/January2019/xC39uanFqrbxrmEtcnCA.png', NULL, 'image', 15, 'Home Page'),
(26, 'home-page.product-section-image', 'Product Section Image', 'settings/January2019/zeO9K1YXRJNs4GCviT1p.jpg', NULL, 'image', 16, 'Home Page'),
(27, 'home-page.photo-video-background-image', 'Photo Video Background Image', 'settings/January2019/lmTeflcefjAf37lYWLVq.jpg', NULL, 'image', 17, 'Home Page'),
(28, 'home-page.instagram-background-image', 'Instagram Background Image', 'settings/January2019/cIASYw0Ja0TbsootGjLi.jpg', NULL, 'image', 18, 'Home Page'),
(29, 'home-page.blog-background-image', 'Blog background Image', 'settings/January2019/CgzAEDFAl21wAIKLDtKj.jpg', NULL, 'image', 19, 'Home Page'),
(30, 'home-page.giveback-section-image', 'Give Back Section Image', 'settings/January2019/JxtSZOhBvB9HbsWkz7rH.jpg', NULL, 'image', 20, 'Home Page'),
(31, 'home-page.about-section-image', 'About Section Image', 'settings/January2019/1tC6DKoVVTqMvOk3ZaHf.png', NULL, 'image', 21, 'Home Page'),
(32, 'home-page.about_content', 'About Content', '<p><strong>Have you ever wanted to indulge in some of the wildest and craziest creations when it comes to food?</strong> If so, then you\'ve found the right place.</p>\r\n<p>Welcome to the <strong>Boston Burger Company</strong>, a full-service, casual American restaurant, where you don\'t have to feel guilty about indulging in the most recognizable and unique <strong>burgers</strong> and <strong>#freakfrappes.</strong></p>', NULL, 'rich_text_box', 23, 'Home Page'),
(36, 'home-page.location_content', 'Locations Content', '<p>We welcome you to come out and experience one of our unique <strong>Boston Burger Company locations.</strong></p>\r\n<p>Come and see what all of the talking is about.</p>', NULL, 'rich_text_box', 24, 'Home Page'),
(37, 'home-page.banner_content', 'Banner Content', '<h1>WTF<strong>?</strong></h1>\r\n<h3>TRY OUR WTF BURGER</h3>\r\n<h4>PULLED PORK, MAC &amp; CHEESE<br /> ONION RING, PICKLES, BBQ SAUCE</h4>', NULL, 'rich_text_box', 22, 'Home Page'),
(38, 'home-page.catering-content', 'Catering Content', '<p>Thanks for your interest in having Boston Burger Company cater your next event. We offer onsite catering with our food truck or portable grills. Our drop-off catering option is great for parties and office meetings. Let us know where you need the food, and we\'ll bring it to you!</p>\r\n<h5>For orders and info,</h5>\r\n<h5>reach us at: <a href=\"mailto:catering@bostonburgerco.com\">catering@bostonburgerco.com</a></h5>\r\n<h5>or 781.537.6192.</h5>', NULL, 'rich_text_box', 25, 'Home Page'),
(39, 'home-page.burger_box_content', 'Burger Box Content', '<ul>\r\n<li>Custom Creations</li>\r\n<li>Delivered Monthly</li>\r\n<li>Great Prices</li>\r\n<li>Feeds Multiple People</li>\r\n</ul>', NULL, 'rich_text_box', 26, 'Home Page'),
(40, 'home-page.burger_box_tag_line', 'Burger Box Tag Line', '<p>Bring <strong>Boston Burger Company</strong> home with you with our <strong>BurgaBox</strong>. <strong>Indulge</strong> and <strong>experience</strong> crafted meals from our kitchen to yours.</p>', NULL, 'rich_text_box', 27, 'Home Page'),
(41, 'site.banner_tagline', 'Banner Tagline', 'AS SEEN ON THE', NULL, 'text', 28, 'Site'),
(44, 'about-us.title', 'Title', 'OUR STORY', NULL, 'text', 31, 'About Us'),
(45, 'about-us.banner_title', 'Banner Title', 'About Us', NULL, 'text', 30, 'About Us'),
(46, 'about-us.image', 'Image', 'settings/January2019/wat5dnK4SuuQEeHH6L7H.jpg', NULL, 'image', 32, 'About Us'),
(47, 'about-us.content', 'Content', '<p>Have you ever wanted to indulge in some of the wildest and craziest creations when it comes to food? If so, then you\'ve found the right place.</p>\r\n<p>Welcome to the Boston Burger Company, a full-service, casual American restaurant, where you don\'t have to feel guilty about indulging in the most recognizable and unique burgers and #freakfrappes.</p>\r\n<p><strong> Located in Boston, Cambridge, and Somerville, we serve the area\'s most recognizable and unique burgers. </strong></p>\r\n<p>We focus on creative burgers, #freakfrappes, gourmet fries, and an excellent selection of craft beers. We also make great appetizers, sandwiches, and salads. All of our sauces and dressings are scratch made. We combine over-the-top food, outstanding service, local art, and cool tunes.</p>\r\n<p>Our goal is to have every guest at the BBC walk out the door and tell their friends about the awesome experience they had!</p>', NULL, 'rich_text_box', 33, 'About Us'),
(48, 'about-us.image-tagline', 'Tag Line', '<h3>BURGERS WITH</h3>\r\n<h2>attitude</h2>\r\n<h3>AND FREAK FRAPPES</h3>\r\n<h3>THAT WILL</h3>\r\n<h2>blow your mind.</h2>', NULL, 'rich_text_box', 34, 'About Us'),
(49, 'site.banner_icon', 'Banner Icon', 'settings/January2019/mBWa7KLSDPADslhr0tdb.png', NULL, 'image', 35, 'Site');

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries`
--

CREATE TABLE `telescope_entries` (
  `sequence` bigint(20) UNSIGNED NOT NULL,
  `uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch_id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `family_hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `should_display_on_index` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `telescope_entries_tags`
--

CREATE TABLE `telescope_entries_tags` (
  `entry_uuid` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `telescope_monitoring`
--

CREATE TABLE `telescope_monitoring` (
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `translations`
--

CREATE TABLE `translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `translations`
--

INSERT INTO `translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2019-01-09 05:59:06', '2019-01-09 05:59:06'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2019-01-09 05:59:07', '2019-01-09 05:59:07'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2019-01-09 05:59:07', '2019-01-09 05:59:07');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `settings` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `email`, `avatar`, `email_verified_at`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`) VALUES
(1, 1, 'Admin', 'admin@bostonburgerco.com', 'users/default.png', NULL, '$2y$10$NE9riAEzknWLsmerHbf3iOZlIJK.HxyWzBdyWtbroAXT4qP5/Fxda', 'C3A9ixfqG8QXl44FFk077FcjhbIQwPsUJ2tF2k2AuVgeoDFDnjncolg6fK1k', '{\"locale\":\"en\"}', '2019-01-09 05:59:02', '2019-01-11 06:25:29');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE `user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accolades`
--
ALTER TABLE `accolades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `accolades_slug_unique` (`slug`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `data_types`
--
ALTER TABLE `data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `food_items`
--
ALTER TABLE `food_items`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `food_items_slug_unique` (`slug`);

--
-- Indexes for table `food_menus`
--
ALTER TABLE `food_menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `food_menus_slug_unique` (`slug`);

--
-- Indexes for table `homepage_sliders`
--
ALTER TABLE `homepage_sliders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `homepage_sliders_slug_unique` (`slug`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `presses`
--
ALTER TABLE `presses`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `presses_slug_unique` (`slug`);

--
-- Indexes for table `retail_partners`
--
ALTER TABLE `retail_partners`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `retail_partners_slug_unique` (`slug`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `telescope_entries`
--
ALTER TABLE `telescope_entries`
  ADD PRIMARY KEY (`sequence`),
  ADD UNIQUE KEY `telescope_entries_uuid_unique` (`uuid`),
  ADD KEY `telescope_entries_batch_id_index` (`batch_id`),
  ADD KEY `telescope_entries_type_should_display_on_index_index` (`type`,`should_display_on_index`),
  ADD KEY `telescope_entries_family_hash_index` (`family_hash`);

--
-- Indexes for table `telescope_entries_tags`
--
ALTER TABLE `telescope_entries_tags`
  ADD KEY `telescope_entries_tags_entry_uuid_tag_index` (`entry_uuid`,`tag`),
  ADD KEY `telescope_entries_tags_tag_index` (`tag`);

--
-- Indexes for table `translations`
--
ALTER TABLE `translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accolades`
--
ALTER TABLE `accolades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `data_rows`
--
ALTER TABLE `data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `data_types`
--
ALTER TABLE `data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `food_items`
--
ALTER TABLE `food_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `food_menus`
--
ALTER TABLE `food_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `homepage_sliders`
--
ALTER TABLE `homepage_sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `menu_items`
--
ALTER TABLE `menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `presses`
--
ALTER TABLE `presses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `retail_partners`
--
ALTER TABLE `retail_partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `telescope_entries`
--
ALTER TABLE `telescope_entries`
  MODIFY `sequence` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `translations`
--
ALTER TABLE `translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `data_rows`
--
ALTER TABLE `data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `menu_items`
--
ALTER TABLE `menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `telescope_entries_tags`
--
ALTER TABLE `telescope_entries_tags`
  ADD CONSTRAINT `telescope_entries_tags_entry_uuid_foreign` FOREIGN KEY (`entry_uuid`) REFERENCES `telescope_entries` (`uuid`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
