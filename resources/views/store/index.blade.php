@extends('layouts.master')

@section('banner-title',"SHOP")
@section('body-class',"subPages")

@section('content')

  @include('partials.inner-banner')
    <div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container">
          <h3 class="sub_head_mn">SHOP BOSTON BURGER COMPANY</h3>
					<div class="store">
						@forelse($categories as $category)
						<div class="row catering-content">
							<div class="col-lg-6 col-12 image-col">
							<div class="image-box">
								<img src="{{ Voyager::image($category->image)}}" alt="{{ $category->name}}"/>
							</div>
							</div>
							<div class="col-lg-6 col-12 text-col">
								<div class="text-box">
									<h3 class="sub_head_mn">{{ $category->name}}</h3>
									{!! $category->description !!}
									<a href="{{ route('shop.index',['category' => $category->slug]) }}" class="view_btn">order Now</a>
								</div>
							</div>
						</div><!-- content row-->
						@empty
							<div class="alert alert-danger">
								No Category Found
							</div>
						@endforelse
					</div><!-- store end-->
        </div>
    @include('partials.sub-footer')	
      </div>  
    </div>

    @include('partials.footer')	



@endsection

@section('extra-js-footer')
    
@endsection