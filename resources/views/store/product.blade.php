@extends('layouts.master')

<!-- @section('banner-title',"Product") -->

@section('body-class','blog-main')

@section('content')

<!-- @include('partials.product-banner') -->

<style>
.rating {
    color: #a9a9a9;
    margin: 0;
    padding: 0;
}
ul.rating {
    display: inline-block;
}
.rating li {
    list-style-type: none;
    display: inline-block;
    padding: 1px;
    text-align: center;
    font-weight: bold;
    cursor: pointer;
    font-size:20px;
}
.rating .filled {
    color: orangered;
}
.clear{
  margin-top:20px;
}
i.fas.fa-heart.liked {
    color: #e1261c;
}
.listeningImg{
  text-align:center
}
</style>

    <div class="product-lisening-main innerPagesBg">
      <div class="container">

      @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="productPg">
          <div class="row">
            <div class="col-12 col-md-7 productLeft">
              
              <div id="owl-1" class="owl-carousel owl-theme">
                  <div class="item">
                    <img src="{{ imageUrl(productImage($product->image),'670') }}"  class="image" alt="{{ $product->name }}">
                  </div>
                  @if ($product->images)
                    @foreach (json_decode($product->images, true) as $image)
                    <div class="item">
                        <img class="image" src="{{ imageUrl(productImage($image),'670') }}" alt="{{ $product->name }}">
                    </div>
                    @endforeach
                @endif
                  
                    
                                
              </div>


              <div id="owl-2" class="owl-carousel owl-theme">
                  <div class="item">
                    <img src="{{ imageUrl(productImage($product->image),'150','100') }}"  class="image" alt="{{ $product->name }}">
                  </div>
                  @if ($product->images)
                    @foreach (json_decode($product->images, true) as $image)
                    <div class="item">
                        <img  class="image" src="{{ imageUrl(productImage($image),'150','100') }}" alt="{{ $product->name }}">
                    </div>
                    @endforeach
                @endif
                         
              </div>
            </div>
            <div class="col-12 col-md-5 mt-5 mt-md-0 productRight">
              <h3>{{ strtoupper($product->name) }}</h3>
              @if(@$productVarients)
                @foreach($productVarients as $productVarient)
                  @php
                    $lastVarientId = $productVarient->id ;
                    $lastVarientPrice = $productVarient->price ;
                  @endphp
                @endforeach
              @endif
            @if((@$product->display_price) && (!@$lastVarientPrice))
            <h2 class="product-price"><span style="text-decoration: line-through;padding-right: 20px">{{presentPrice($product->display_price)}}</span>{{ @($lastVarientPrice) ? presentPrice($lastVarientPrice) : presentPrice($product->price) }}</h2>
            @else
            <h2 class="product-price">{{ @($lastVarientPrice) ? presentPrice($lastVarientPrice) : presentPrice($product->price) }}</h2>
            @endif
             
             
              <div class="proReview">
              @php
                $ratingAverage = App\Rating::where('product_id',$product->id)->where('status','Approved')->avg('rating');
                $ratingAverage = round($ratingAverage,2);
                $rating = round($ratingAverage);
              @endphp
                      <div class="starRev">
                        <ul>
                        @if(@$rating)
                            @for($i=1; $i<$rating; $i++)  
                              <li><i title="{{$ratingAverage}}" class="fas fa-star"></i></li>
                            @endfor
                            
                            @if($rating > $ratingAverage )
                              <li><i title="{{$ratingAverage}}" class="fas fa-star-half"></i></li>
                            @else
                            <li><i title="{{$ratingAverage}}" class="fas fa-star"></i></li>
                            @endif

                        @else
                        <li><i class="far fa-star"></i></li>
                        <li><i class="far fa-star"></i></li>
                        <li><i class="far fa-star"></i></li>
                        <li><i class="far fa-star"></i></li>
                        <li><i class="far fa-star"></i></li>
                        @endif
                      </ul>
                    </div>
                 
                @php $reviews = App\Review::where('product_id',$product->id)->where('status','Approved')->count(); @endphp
                  <a href="#" class="disabledAnchor">
                  @if(@$reviews)  
                    {{$reviews}} customers reviews 
                  @else
                    {{$reviews}} customers review 
                  @endif  
                 </a>
                
              </div>
            <p>{{ $product->details }}</p>
              <div class="productdtl" ng-controller="qtyCtrl">
              <div class="proQty">
                <h4>Quantity</h4>
                  
    

                  <div class="qtyCount">
                   <button class="minusBtn" ng-click="decrement()">-</button>
                      <span>@{{count}}</span>
                    <button class="plusBtn" ng-click="increment()">+</button>
                  </div>

              </div>
              <div class="buttnSize">
                @if(@$lastVarientPrice)
                <h4>Size</h4>
                  @foreach($productVarients as $productVarient)
                    <button class="varients{{ ($lastVarientId == $productVarient->id) ? ' selected' : '' }}" data-price="{{ $productVarient->price }}" data-id="{{ $productVarient->id }}">{{ $productVarient->name }}</button>
                    @php $varientLast = $productVarient->id @endphp
                  @endforeach
                @endif
                
               
              </div>
              <form action="{{ route('cart.store', $product) }}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="view_btn"><i class="fab fa-opencart"></i>Add to Cart</button>
                    <input type="hidden" class="varient-selected" name="varient" value="{{ (@$varientLast) ? $varientLast : ''  }}">
                    <input type="hidden"  name="quantityCount" value="@{{count}}">
                </form>
                   <div class="blogDtlComment">
                  <div class="share-section">
        <a target="_blank" href="https://twitter.com/share?url={{ urlencode(url()->current())}}&via=TWITTER_HANDLE&text={{$product->name}}" class="btn-media twitter">
            <i class="fab fa-twitter" aria-hidden="true"></i>
          </a>
          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn-media facebook">
            <i class="fab fa-facebook" aria-hidden="true"></i>
          </a>
          <a target="_blank" href="https://plus.google.com/share?url={{ urlencode(url()->current()) }}" class="btn-media google-plus">
            <i class="fab fa-google-plus" aria-hidden="true"></i>
          </a>
            </div>
                   </div>
            </div>

            </div>
          </div>
        </div>



        <div class="productDesTab">



            <div class="TabCtrlMain" ng-controller="TabController">
              <div class="proTabHead">
                <ul class="nav nav-pills nav-stacked">
                  <li ng-class="{ active: isSet(1) }">
                      <a href ng-click="setTab(1)">Description</a>
                  </li>
                  <li ng-class="{ active: isSet(2) }">
                      <a href ng-click="setTab(2)">Reviews</a>
                  </li>
                  <li ng-class="{ active: isSet(3) }">
                      <a href ng-click="setTab(3)">FAQs</a>
                  </li>
                </ul>
              </div>
              <div class="proTabText">
                  <div ng-show="isSet(1)">
                  {!! $product->description !!}
                    </div>
                  <div ng-show="isSet(2)">
                  @if(@Auth::user())
                  Rate Product
                  <div ng-controller="StarCtrl"> 
                    <div class=col-md-12>
                        <div ng-repeat="rating in ratings">
                            <div star-rating rating-value="rating.current" max="rating.max" on-rating-selected="getSelectedRating(rating)"></div>
                        </div>
                        <p ng-bind="ratingsuccess">
                    </div>
                        <!-- <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary" data-ng-click="sendRate()">Send</button>
                        </div> -->
                </div>
                @endif
                <div class="comment-section">
          <h2> Review</h2>

          @if(@Auth::user())
            <form action="{{ route('shop.review',$product->id) }}" method="POST">
              @csrf
              @method('POST')
              <textarea name="comment"  cols="50" rows="3"></textarea>
              <button type="submit" class="view_btn">Submit</button>
            </form>
          @endif

          @php 
          $reviews = App\Review::where('product_id',$product->id)->where('status','Approved')->get();
          @endphp
            @forelse($reviews as $review)
            <div class="commentShow">
          @php 
          $rating = App\Rating::where('product_id',$product->id)->where('status','Approved')->where('user_id',$review->user->id)->first();
          @endphp
          @if(@$rating)
           <div class="rating-user">{{$rating->rating}}</div>
          @endif
              <div class="commentShowtitle">{{$review->user->name}} <span>{{$review->user->email}}</span></div>
              <div class="commentTxt">{{$review->description}}</div>
            </div>
          
          @empty
          <div class="alert alert-error">No reviews Found</div>
          @endforelse
        </div>
                  </div>
                  <div ng-show="isSet(3)">
                  FAQ section
                    </div>
              </div>
            </div>
        </div>

        @if(@$relatedProducts[0])
        <div class="relatedProduct" ng-controller="likeCtrl">
          <h2 class="head_mn">Related Products</h2>
          <div class="row">
          @forelse(@$relatedProducts as $relatedProduct)
                 <div class="col-12 col-sm-6 col-lg-6 col-xl-3 productGridMn">
                  <div class="produt-grid">
                    <div class="listeningImg">
                      <img  src="{{ imageUrl(productImage($relatedProduct->image),null,'260') }}" alt="{{ $relatedProduct->name }}">
                    </div>
                    <div class="listeningText">
                      <h4><a href="{{route('shop.show',$relatedProduct->slug)}}">{{ strtoupper($relatedProduct->name) }}</a></h4>
                      <h3>{{ presentPrice($relatedProduct->price) }}</h3>
                      <a href="{{route('shop.show',$relatedProduct->slug)}}" class="view_btn">Add to Cart</a>
                      @if(@Auth::user())
                      <ul>
                        <li>
                          @php  $liked = App\Like::where('type',2)->where('parent_id',$relatedProduct->id)->where('user_id',Auth::user()->id)->count();  @endphp
                        <a href="#" class="disabledAnchor" data-id="{{$relatedProduct->id}}" ng-click="triggerLike($event)">
                          <i class="disabledAnchor classpost{{$relatedProduct->id}} {{ @($liked)? 'liked':'' }}  fas fa-heart" aria-hidden="true"></i>
                        </a>
                        </li>
                        <!-- <li>
                          <a href="#">
                            <i class="fas fa-eye"></i>
                          </a>
                        </li> -->
                      </ul>
                      @endif
                    </div>
                  </div>
                </div>
                @empty
                <!--<div class="alert alert-danger btn-block text-center"> No Items available</div>-->
            @endforelse 
            
          </div>
        </div>
        @endif
      </div>
    </div>

    
 @include('partials.sub-footer')	
 @include('partials.footer')	

@endsection

@section('extra-js-footer')

<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript">      
       
       $(document).ready(function() {
        $(".varients").click(function(){
            $(".product-price").text("$ USD "+$(this).data('price'));
            $(".varient-selected").val($(this).data('id'));
            $(".varients").removeClass('selected');
            $(this).addClass('selected');
        });
       });
</script>

    <script type="text/javascript">
      $(document).ready(function(){
  
        var owl_1 = $('#owl-1');
        var owl_2 = $('#owl-2');
        
        owl_1.owlCarousel({
          loop:true,
          margin:10,
          nav:false,
          items: 1,
          dots: false
        });
        
        owl_2.owlCarousel({
          margin:10,
          nav: true,
          items: 4,
          dots: false
        });
        
        owl_2.find(".item").click(function(){
          var slide_index = owl_2.find(".item").index(this);
          owl_1.trigger('to.owl.carousel',[slide_index,300]);
        });
        
        // Custom Button
      });
      var app = angular.module('myapp', ['simple-accordion']);


  app.controller('qtyCtrl', ['$scope', function($scope) {
    $scope.count = 1;
    $scope.increment = function() {
      $scope.count++;
    };
    $scope.decrement = function() {
      console.log($scope.count);
      if($scope.count <= 1){
        $scope.count = 1 ;
       }
      else{
        $scope.count--;
        }
    };
  }]);
    app.controller('likeCtrl', ['$scope','$http', function($scope,$http) {
    $scope.triggerLike = function($event){
                    // console.log($event.currentTarget);
                    var product_id = $event.currentTarget.getAttribute('data-id');
                $http({
                  method: 'post',
                  url:"{{ route('shop.wishlist') }}",
                  data: {product_id: product_id }
                }).then(function successCallback(response) {
                      $scope.status = response.data;
                      var myEl = angular.element( document.querySelector( '.classpost'+product_id ) );
                      if($scope.status.success=="liked"){
                        myEl.addClass('liked');
                      }else{
                        myEl.removeClass('liked');
                      }
                });
          }
  }]);
  // var app = angular.module('myapp', []);

  app.controller('StarCtrl', ['$scope','$http', function ($scope,$http) {
    $scope.rating = 0;
    $scope.ratingsuccess = "";
    $scope.ratings = [
    //   {
    //     current: 5,
    //     max: 10
    // }
    // ,
     {
        current: 3,
        max: 5
    }];

    $scope.getSelectedRating = function (rating) {
        console.log(rating);
        $scope.ratingsuccess = "";
        $http({
                  method: 'post',
                  url:"{{ route('shop.rating') }}",
                  data: {
                    product_id : {{$product->id}},
                    rating : rating
                         }
                }).then(function successCallback(response) {
                      $scope.dataproduct = response.data;
                      console.log($scope.dataproduct);
                      $scope.ratingsuccess = "Rating updated Successfully"
                });
    }
}]);

app.directive('starRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
              
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});
  app.controller('TabController', ['$scope', function($scope) {
      $scope.tab = 1;

      $scope.setTab = function(newTab){
        $scope.tab = newTab;
      };

      $scope.isSet = function(tabNum){
        return $scope.tab === tabNum;
      };
  }]);

              app.controller('classCtrl', function ($scope) {
                $scope.isActive = false;
                $scope.activeButton = function() {
                  $scope.isActive = !$scope.isActive;
                }  
              });

              app.controller('MainCtrl', function($scope) {

              });

              angular.module('simple-accordion', [])
                  .directive('simpleAccordion', function($compile, $timeout) {
                      return {
                          restrict: 'AEC',
                          controller: function($scope) {
                              $scope.current = null;
                              $scope.height = [];
                              $scope.zero = {
                                  height: 0
                              };
                              $scope.toggle = function(i) {
                                  $scope.current = $scope.current === i ? null : i;
                              };
                          },
                          link: function(scope, el, attrs) {
                            var itemSelector = attrs.itemSelector || '.ftr_accordian , .burger_block',
                                titleSelector = attrs.titleSelector || '.ftr_head_mob, .burger_tab_mob',
                                contentSelector = attrs.contentSelector || '.ftr_link_mob, .burger_block_text';
                            $timeout(function(){
                              var items = el[0].querySelectorAll(itemSelector);
                              for (var i in items) {
                                if (angular.isObject(items[i])) {
                                  var title = items[i].querySelectorAll(titleSelector)[0];
                                  var content = items[i].querySelectorAll(contentSelector)[0];
                                  scope.height.push({
                                    'max-height': (content.offsetHeight + 10) + 'px'
                                  });
                                  angular.element(items[i]).addClass('item').attr({
                                    'ng-class': '{\'open\':current == ' + i + '}'
                                  });
                                  angular.element(title).addClass('title').attr('ng-click', 'toggle(' + i + ')');
                                  angular.element(content).addClass('content').attr({
                                    'ng-style': 'current == ' + i + '?height[' + i + ']:zero'
                                  });;

                                }
                              }
                              $compile(angular.element(el).contents())(scope);
                            });
                          }
                      }
                  });
            

            $('.burger_tab').on('click', function(){
              $('.burger_tab.active').removeClass('active');
              $(this).addClass('active');
            });

            $(document).ready(function() {
              var topArrow = $(".topArrow");
              $(topArrow).click(function() {
                $('html, body').animate({
                  scrollTop: 0
                }, 800);
                return false;
              }); 
            }); 
            

    </script>
@endsection