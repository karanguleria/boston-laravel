@extends('layouts.master')

@section('banner-title',"Products")

@section('body-class','blog-main')

@section('extra-css')
<link rel='stylesheet' href='https://rawgit.com/rzajac/angularjs-slider/master/dist/rzslider.css'>
<style>
i.fas.fa-heart.liked {
    color: #e1261c;
}
</style>
@endsection

@section('content')
<div  ng-controller="GridController">
<div class="banner inner_banner gallery_banner">
      <div class="container_outer">
        <div class="banner_mn">
          <div class="banner_text">
            <h1>@{{data.categoryName|uppercase}}</h1>
          </div>
        </div>  
      </div>
    </div>
    <div class="product-lisening-main sub_pages">
      <div class="container">
      @include('partials.success-error')
        <div class="row">
          <div class="col Gallery_sidebar d-md-none">
            <div class="selectViewMn">
              <h3>SELECT VIEW</h3>
              <div class="selectView">
                <div class="viewClick">
                  <ul>
                    <li>
                      <span ng-click="gridClick()">
                        <i class="fas fa-th"></i>
                      </span>
                    </li>
                    <li>
                      <span ng-click="listClick()">
                        <i class="fas fa-list-ul"></i>
                      </span>
                    </li>
                  </ul>
                </div>
                <div class="sort">
                  <i class="fas fa-chevron-down"></i>
                  <select ng-model="sort" ng-change="getProducts()">
                  <option value="default">Default</option>
                    <option value="low_high">Low to High</option>
                    <option value="high_low">High to Low</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="priceRange">
              <h3>FILTER BY PRICE</h3>
               <div class="wrapper">
                  <article>
                    <rzslider rz-slider-model="minRangeSlider.minValue" rz-slider-high="minRangeSlider.maxValue" rz-slider-options="minRangeSlider.options"></rzslider>
                    <div class="rangeMain">
                      <div class="price-range">PRICE: 
                        <span class="priceRg">$@{{minRangeSlider.minValue}}</span>
                        <span  class="priceLn"></span>
                        <span class="priceRg">$@{{minRangeSlider.maxValue}}</span>
                      </div>
                      <!-- <div class="filterButton">
                        <button class="view_btn">Filter</button>
                      </div> -->
                    </div>  
                  </article>
                </div>
            </div>
          </div>
          <!-- @{{data}} -->
          <div class="col gallery_left">
          <div ng-include="viewType"></div>    
          </div>
          
          @include('partials.product-sidebar')
       
          <div class="modal fade product_view" id="product_view">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  
                    <div class="modal-body">
                        <div class="row">
                          <div class="col-12 col-md-7 productLeft">
                                  <img ng-src="@{{dataproduct.product.image}}">
                          </div>
                          <div class="col-12 col-md-5 mt-5 mt-md-0 productRight">
                            <h4>@{{dataproduct.product.name}}</h4>
                            <div ng-bind-html="pice_html"></div>
                    
                    
                                <div class="proReview">
                              
                                <div ng-bind-html="rating_html"></div>
                                <a href="#" class="disabledAnchor">
                                @{{dataproduct.product.review}} customers reviews 
                                </a>
                                </div>
                                <div ng-bind-html="pdescription"></div>
                                      <div class="productdtl" ng-controller="qtyCtrl">
                                            <div class="proQty">
                                              <h4>Quantity</h4>
                                                <div class="qtyCount">
                                                <button class="minusBtn" ng-click="decrement()">-</button>
                                                    <span>@{{count}}</span>
                                                  <button class="plusBtn" ng-click="increment()">+</button>
                                                </div>
                                            </div>
                                            <div class="buttnSize" ng-bind-html="varient_html">
                                            </div>
                                            <form action="@{{dataproduct.product.addtoCart}}" method="POST">
                                              {{ csrf_field() }}
                                              <button type="submit" class="view_btn"><i class="fab fa-opencart"></i>SHOP NOW</button>
                                              <input type="hidden" class="varient-selected" name="varient" value="@{{dataproduct.product.varientLast}}">
                                              <input type="hidden"  name="quantityCount" value="@{{count}}">
                                            </form>
                                            <div class="blogDtlComment">
                                              <div class="share-section">
                                                <a target="_blank" href="@{{dataproduct.product.tlink}}" class="btn-media twitter">
                                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                                  </a>
                                                  <a target="_blank" href="@{{dataproduct.product.flink}}" class="btn-media facebook">
                                                    <i class="fab fa-facebook" aria-hidden="true"></i>
                                                  </a>
                                                  <a target="_blank" href="@{{dataproduct.product.glink}}" class="btn-media google-plus">
                                                    <i class="fab fa-google-plus" aria-hidden="true"></i>
                                                  </a>
                                              </div>
                                            </div>
                                        </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    </div>
    
 @include('partials.sub-footer')	
 @include('partials.footer')	


@endsection

@section('extra-js-footer')
<script src="https://cdnjs.cloudflare.com/ajax/libs/angular-ui-bootstrap/0.14.3/ui-bootstrap-tpls.js"></script>
<script src="https://rawgit.com/rzajac/angularjs-slider/master/dist/rzslider.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script type="text/javascript">      
       
       $(document).ready(function() {
        $('body').on('click','.varients',function(){
          $(".product-price").text("$ "+$(this).data('price'));
            $(".varient-selected").val($(this).data('id'));
            $(".varients").removeClass('selected');
            $(this).addClass('selected');
        });
      });
</script>
<script type="text/javascript">
  var app = angular.module('myapp', ['ui.bootstrap', 'simple-accordion','ngSanitize']);
  // var app = angular.module('myapp', ['rzModule', 'ui.bootstrap', 'simple-accordion','ngSanitize']);

        app.controller('GridController', function ($scope, $rootScope, $timeout, $uibModal,$http,$sce) {
            //Range slider config
            $scope.minRangeSlider = {
                minValue: 5,
                maxValue: 250,
                options: {
                    floor: 10,
                    ceil: 250,
                    step: 1,
                    onEnd: function(id) {
                        console.log('on end ' + id); // logs 'on end slider-id'
                        $scope.getProducts();
                    }
                },
            };
            $scope.category_name="{{ app('request')->input('category') }}";
            $scope.productCounter=1;
            $rootScope.viewType = 'template/grid-view.blade.php';
            $rootScope.sortBy = '+name';
            $scope.loadMoreShow = true


            $scope.gridClick = function(){
              $rootScope.viewType = 'template/grid-view.blade.php';
          }
          $scope.listClick = function(){
              $rootScope.viewType = 'template/list-view.blade.php';
          }
          $scope.getProducts = function (){
            $http({
            method: 'get',
          url:"{{ route('shop.filter') }}?category="+$scope.category_name+"&pricemin="+$scope.minRangeSlider.minValue+"&pricemax="+$scope.minRangeSlider.maxValue+"&productCount="+$scope.productCounter,
          // url:"{{ route('shop.filter') }}?category="+$scope.category_name+"&sort="+$scope.sort+"&pricemin="+$scope.minRangeSlider.minValue+"&pricemax="+$scope.minRangeSlider.maxValue+"&productCount="+$scope.productCounter,
          }).then(function successCallback(response) {
            $scope.data = response.data;
            for (products in $scope.data.products.data) {
             $scope.data.products.data[products].rating_html = $sce.trustAsHtml($scope.data.products.data[products].rating_html);
            } 
            if($scope.data.products.total >  $scope.data.productCount){
            }else{
              $scope.loadMoreShow=false;
            }
             $scope.loginuser = $scope.data.loginuser;
          });
        };

        $scope.setCatname = function($var){
          $scope.category_name =$var;
          $scope.getProducts();
        }
        $scope.setCounter =function(){
        $scope.productCounter++
        $scope.getProducts();
        }
        $scope.getProducts();
        $scope.triggerLike = function($event){
                    var product_id = $event.currentTarget.getAttribute('data-id');
                $http({
                  method: 'post',
                  url:"{{ route('shop.wishlist') }}",
                  data: {product_id: product_id }
                }).then(function successCallback(response) {
                      $scope.status = response.data;
                      var myEl = angular.element( document.querySelector( '.classpost'+product_id ) );
                      if($scope.status.success=="liked"){
                        console.log('like added');
                        myEl.addClass('liked');
                      }else{
                        console.log('like removed');
                        myEl.removeClass('liked');
                      }
                });
          }
      $scope.updateModel = function($event){
        var product_id = $event.currentTarget.getAttribute('data-id');
                $http({
                  method: 'post',
                  url:"{{ route('shop.showajax') }}",
                  data: {product_id: product_id }
                }).then(function successCallback(response) {
               
                      $scope.dataproduct = response.data;
                      
                      $scope.pdescription = $sce.trustAsHtml($scope.dataproduct.product.detail);
                      $scope.rating_html = $sce.trustAsHtml($scope.dataproduct.product.rating_html);
                      $scope.pice_html = $sce.trustAsHtml($scope.dataproduct.product.pice_html);
                      $scope.varient_html = $sce.trustAsHtml($scope.dataproduct.product.varient_html);
                });
        
      }

        });


        app.controller('qtyCtrl', ['$scope', function($scope) {
            $scope.count = 1;
            $scope.increment = function() {
              $scope.count++;
            };
            $scope.decrement = function() {
              console.log($scope.count);
              if($scope.count <= 1){
                $scope.count = 1 ;
              }
              else{
                $scope.count--;
                }
            };
          }]);


              app.controller('classCtrl', function ($scope) {
                $scope.isActive = false;
                $scope.activeButton = function() {
                  $scope.isActive = !$scope.isActive;
                }  
              });

              app.controller('MainCtrl', function($scope) {

              });
              
     



</script>
@endsection