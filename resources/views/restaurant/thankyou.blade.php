@extends('layouts.master')

<!-- @section('banner-title',"Thank You Restaurant") -->

@section('body-class','blog-main')

@section('content')

<!-- @include('partials.product-banner') -->
    <div class="product-lisening-main innerPagesBg">
      <div class="container">
        <div class="thankyouSec">
          <div class="thankInner">
            <div class="thankTxt">
              <h3>Thank You </h3>
              <p>YOUR RESTAURANT ORDER HAS BEEN RECEIVED.</p>
            </div>
          </div>
        </div>





    <div class="CheckoutMain">
            <div class="row">
              <div class="col-12 col-md-6 checkoutLeft">
                <div class="checkout_account">
                  <h3>CUSTOMER DETAILS</h3>
                  <p><b>Phone: +{{ $order->billing_phone}}</b></p>
                  <p>{{ Auth::user()->email}}</p>
                </div>


                <div class="checkout_account">
                  <h3>BILLING ADDRESS</h3>
                  <p>{{ $order->billing_name }}</p>
                  <p>{{ $order->billing_email }}</p>
                  <P>{{ $order->billing_address }}</P>
                  <P>{{ $order->billing_city }}  - {{ $order->billing_postalcode}}</P>
                  <P>{{ $order->billing_province}}, {{ $order->billing_country}} </P>
                </div>

                <div class="backHome d-none d-md-block">
                  <a href="{{ url('/')}}" class="view_btn"><i class="fas fa-long-arrow-alt-left"></i> &nbsp; Back to Home</a>
                </div>




                




                

              </div>
              <div class="col-12 col-md-6 ThankyouRight checkoutRight">

                <div class="productTable productCart ">
                  <table class="table table-bordered text-center">
                    <thead>
                      <tr>
                        <th scope="col" class="text-left">PRODUCTS</th>
                        <th scope="col">PRICE</th>
                      </tr>
                    </thead>
                    <tbody>   
                    @foreach ($products as $product)
                    @php  
                    $product_detail =  App\RestaurantItem::where('id',$product->product_id)->first();
                    @endphp
                      <tr>
                        <td class="text-left align-middle">
                          <a href="#">
                            <div class="cart_sec">
                              <div class="cartText">
                                <h4>{{ strtoupper(str_limit($product_detail->name, 25, '...')) }}</h4>
                                  @if($product->variant)
                                  <p>Size : {{ $product->variant }}</p>
                                  @endif
                                <p>Quantity : {{ $product->quantity }}</p>
                                <p>Price : {{ presentPrice($product->price) }}</p>
                              </div>
                            </div>
                          </a>  
                        </td>
                        <td class="align-middle">{{ presentPrice($product->price*$product->quantity) }}</td>
                      </tr>
                      @endforeach
                    
                      <tr>
                        <td class="check_sub">
                          <span>Subtotal</span>
                        </td>
                        <td>
                          <span>{{ presentPrice($order->billing_subtotal + $order->billing_discount) }}</span>
                        </td>
                      </tr> 
                      @if(@$order->billing_discount_code)
                      <tr>
                        <td class="check_sub">
                          <span>Discount ({{ presentPrice($order->billing_discount_code) }})</span>
                        </td>
                        <td>
                          <span>-{{ presentPrice($order->billing_discount) }}</span>
                        </td>
                      </tr> 
                      <tr>
                        <td class="check_sub">
                          <span>New Subtotal</span>
                        </td>
                        <td>
                          <span>{{ presentPrice($order->billing_subtotal) }}</span>
                        </td>
                      </tr>
                      @endif
                      <tr>
                        <td class="check_sub">
                          Tax
                        </td>
                        <td>
                        {{ presentPrice($order->billing_tax) }}
                        </td>
                      </tr>
                      <tr>
                        <td class="check_sub">
                          Total
                        </td>
                        <td>
                        {{ presentPrice($order->billing_total) }}
                        </td>
                      </tr>
                      
                      
                      
                      
                    </tbody>
                  </table>
                </div>
                

              </div>
            </div>
          </div>






      </div>
    </div>

@include('partials.sub-footer')	
@include('partials.footer')	


@endsection

   
