@extends('layouts.master')

@section('body-class',"about-page")

@section('banner-title',"About Us")

@section('content')

  @include('partials.inner-banner')


    <div class="sub_pages our_story" id="ourstory">
      <div class="sub_pages_sec">
        <div class="container">
          <h3 class="sub_head_mn">{{ setting('about-us.title') }}</h3>
          <div class="row">
            <div class="col about_text">
            {!! setting('about-us.content') !!}    
            </div>
            <div class="col about_img_Sec">
              <img src="{{ url('storage/'.setting('about-us.image')) }}">
              <div class="abt_sec_txt">
              {!! setting('about-us.image-tagline') !!}
              </div>
            </div>
          </div>
        </div>
@include('partials.sub-footer')
      </div>  
    </div>

    
 @include('partials.footer')


    

@endsection
