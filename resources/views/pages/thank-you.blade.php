@extends('layouts.master')
@section('body-class',"thank-Page")
@section('banner-title',"THANK YOU!")

@section('content')

  @include('partials.inner-banner')



<!-- @include('partials.product-banner') -->
    <div class="product-lisening-main sub_pages">
      <div class="sub_pages_sec">
            <div class="container">
              <div class="thankyouSec">
                <div class="thankInner">
                  <div class="thankTxt">
                    <h3>Thank You</h3>
                    <p>Thank you for contacting us. Our team will be in touch with you within 24 hours.</p>
                  </div>
                </div>
              </div>

            </div>
          @include('partials.shop')
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
      </div>      
    </div>
 
    @include('partials.footer')	

	



@endsection

   
