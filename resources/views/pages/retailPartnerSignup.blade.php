@extends('layouts.master')

@section('banner-title',"Retail Partner")

@section('content')

@include('partials.inner-banner')
<div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container">
@include('partials.success-error')
            <p>Thank you for your interest in becoming a Boston Burger Brand Retail Partner.
                A representative will be in contact within the week.
                Please provide as much detail as possible so we can direct your requests to the correct team member.</p>
            <form action="{{ route('retailpartner.store')}}" method="POST">
                @csrf
                @method('POST')
                <div class="modal-body" style="padding: 5px;">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                            <input class="form-control" name="firstname" placeholder="Firstname" type="text" required autofocus />
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6" style="padding-bottom: 10px;">
                            <input class="form-control" name="lastname" placeholder="Lastname" type="text" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                            <input class="form-control" name="email" placeholder="E-mail" type="email" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                            <input class="form-control" name="phone" placeholder="Phone" type="text" required />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <textarea style="resize:vertical;" class="form-control" placeholder="Short Description ..." rows="6" name="description" required></textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-success" value="Save">Save</button>
            </form> 
        </div>

@include('partials.sub-footer')	
    </div>
</div>

@include('partials.footer')	


@endsection

@section('extra-js-footer')
    
@endsection