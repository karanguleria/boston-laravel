@extends('layouts.master')
@section('content')

  <div class="innerPagesBg return-policy">
      <div class="sub_pages_sec">
        <div class="container">
        <h2 class="head_mn">Terms and Conditions</h2>
          <div class="row">
                <div class="col-12 section-content">
                {!! setting('terms-conditions-page.content') !!}
            </div>
          </div>
        </div>
    
    @include('partials.sub-footer')	
      </div>
    </div>

    
@include('partials.footer')	





@endsection