@extends('layouts.master')
@section('content')

  <div class="innerPagesBg return-policy">
      <div class="sub_pages_sec">
        <div class="container">
        <h2 class="head_mn">PRIVACY POLICY</h2>
          <div class="row">
                <div class="col-12 section-content">
                 {!! setting('privacy-policy-page.content') !!}
            </div>
          </div>
        </div>
        @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
      </div>
    </div>


    @include('partials.footer')	




@endsection