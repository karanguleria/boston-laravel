@extends('layouts.master')
@section('content')
<div class="innerPagesBg return-policy">
      <div class="sub_pages_sec">
        <div class="container">
        <h2 class="head_mn">FAQ</h2>  
            <div  ng-controller="MainCtrl">
                <div class="faqMn_Acc" simple-accordion>
                  @php
                  $faqs = App\Faq::all();
                  @endphp
                  @forelse($faqs as $faq)
                        <div class="faq_accordian">
                            <h4 class="faq_head">{{ $faq->title }}</h4>
                            <div class="faq_text">
                              <p>{!! $faq->description !!}</p>
                            </div>
                        </div> 
                  @empty
                      <div class="alert alert-danger">No Faq Found</div>
                  @endforelse
                </div>
            </div>
        </div>
        @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
      </div>  
    </div>

    @include('partials.footer')	
@endsection