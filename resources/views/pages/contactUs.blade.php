
@extends('layouts.master')

@section('body-class',"blog-main")

@section('content')

<div class="contactPg innerPagesBg">
      <div class="container">
      @if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif

            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <h2 class="head_mn">Contact Us</h2>
        <form action="{{ route('contactus.store')}}" method="POST" id="contact_us">
          @csrf
          <div class="row">
            <div class="col-12 col-md-6">
              <input type="text" name="first_name" placeholder="First Name" required="required">
            </div>
            <div class="col-12 col-md-6">
              <input type="text" name="last_name" placeholder="Last Name" required="required">
            </div>
            <div class="col-12 col-md-6">
              <input type="email" name="email" placeholder="Email Address" required="required">
            </div>
            <div class="col-12 col-md-6">
              <input type="tel" name="phone" placeholder="Phone Number" required="required">
            </div>
            <div class="col-12">
              <h3>Subject</h3>
              <select required="required" id="subject" name="subject" >
                <option value="catering">Catering & Events</option>
                <option value="Online">Online Orders</option>
                <option value="Restaurant">Restaurant Orders</option>
                <option value="Menu">Menu</option>
                <option value="Partnership">Partnership</option>
                <option value="Careers">Careers</option>
                <option value="Other">Other</option>
              </select>
            </div>
            <div class="CcateringEvent">
              <div class="col-12">
                <h3>Date of Event:</h3>
                <div class="datepicker">
                  <i class="fas fa-calendar-alt"></i>
                <input type="date" name="date_of_event">
              </div>
                <h3>Type of Event:</h3>
                <div class="eventChk">
                  <input name="corporate_catering"  type="checkbox" id="chk1">
                  <label for="chk1">Corporate Catering</label>
                </div>
                <div class="eventChk">
                  <input name="social_event_catering"  type="checkbox" id="chk2">
                  <label for="chk2">Social Event Catering</label>
                </div>
                <div class="eventChk">
                  <input name="concession_catering" type="checkbox" id="chk3">
                  <label for="chk3">Concession Catering</label>
                </div>
                <div class="eventChk">
                  <input name="wedding" type="checkbox" id="chk4">
                  <label for="chk4">Wedding</label>
                </div>
                <div class="eventChk">
                  <input name="golf_tournament" type="checkbox" id="chk5">
                  <label for="chk5">Golf Tournament</label>
                </div>
                <div class="eventChk">
                  <input name="food_truck" type="checkbox" id="chk6">
                  <label for="chk6">Food Truck</label>
                </div>
              </div>
            </div>
            <div class="col-12" required="required">
              <textarea name="message" required="required" placeholder="Message"></textarea>
            </div>
            <div class="col-12">
              <button class="view_btn">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>



    @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
    @include('partials.footer')	



@endsection

@section('extra-js-footer')
<script>
$(document).ready(function(){
  $("#subject").change(function(){
    var subject = $(this).val();
    if(subject =="catering"){
      $(".CcateringEvent").slideDown();
    }else{
      $(".CcateringEvent").slideUp();
    }
  });
  // $("#contact_us").submit(function(e){
  //     e.preventDefault();
  //     $(".subscribeSec").hide();
  //   $(".thanksSec").show();
  // });
 
            // if((window.location.href).search("type=")>1){
            //   console.log("ASdasd");
            //   $("#subject").val("catering");
            //     }
          
            // console.log(cat);
            // if(cat){
            //     if(cat =="sandwiches"){
                  
            //     }else if(cat =="freakfrappes"){
            //         $scope.clickfreakfrappes();
            //     }else if(cat =="salads"){
            //         $scope.clicksalads();
            //     }else if(cat =="appetizers"){
            //         $scope.clickappetizers();
            //     }else{
            //         $scope.clickburgers();
            //     }
	
});
</script>

@endsection