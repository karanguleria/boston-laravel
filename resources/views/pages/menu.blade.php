@extends('layouts.master')


@section('content')

  <div ng-controller="myctrl">
  <div class="banner inner_banner gallery_banner">
      <div class="container_outer">
        <div class="banner_mn">
          <div class="banner_text">
            <h1>Menu</h1>
            <ul>
                <li>
                <span ng-click="cateringClick()">
                Catering
                </span>
            </li>
            <li>
                <span ng-click="restaurantClick()">
                Restaurant
                </span>
            </li>
        </ul>
          </div>
        </div>  
      </div>
    </div>

    <div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container_outer">
        
        <div class="resturantSection" ng-show="showRestaurant">
        <h3 class="sub_head_mn">RESTAURANT GETTING HUNGRY, ORDER NOW</h3>
          <div class="d-none d-md-block">
            <div class="burger_menu">
                <div class="burger_menu_text">
                    @forelse($restaurantMenus as $k=>$restaurantMenu)
                    <div ng-show="show{{ $restaurantMenu->slug }}" class="burger_txt">
                        @php 
                            $menuItems = App\RestaurantItem::getMenuItems($restaurantMenu->id,20);
                        @endphp
                        @forelse($menuItems as $l=>$menuItem)
                            <div class="{{ ( $l%2 ? 'bustn_burger_rt' : 'bustn_burger_lt')}} @if($menuItem->images) @php echo  (count(json_decode($menuItem->images, true)) > 1 ) ? 'multipleImage':''; @endphp @endif ">
                            <div class="titleSec @if($menuItem->images) @php echo  (count(json_decode($menuItem->images, true)) > 1 ) ? 'multipleImage':''; @endphp @endif "> 
                                  <a href="" data-id="{{$menuItem->id}}" data-type="restaurant" data-toggle="modal" data-target="#product_view" ng-click="updateModel($event)">
                                     <h3>
                                         {{ $menuItem->name }} 
                                  @if ($menuItem->images)
                                        @foreach (json_decode($menuItem->images, true) as $k =>$image)
                                            @if($k == 0)
                                            <img src="{{ Voyager::image($image)}}" alt="{{ $menuItem->name }}">
                                            @endif
                                            @if($k == 1)
                                            <img class="secimg" src="{{ Voyager::image($image)}}" alt="{{ $menuItem->name }}">
                                            @endif
                                        @endforeach
                                    @endif
                                </h3></a>
                                @if(@$menuItem-> price)
                                        <span class="foodprice"> $ {{ $menuItem-> price}}</span>
                                    @endif
                                </div>
                                <p>{{ $menuItem->description }}</p>
                            </div>
                        @empty
                        <div class="alert alert-danger">No Menu Item found</div>
                        @endforelse
                        <a class="burger_new" href="{{ route('menu') }}">
                            <span></span>
                        </a>                          
                    </div>
                    @empty
                        <div class="alert alert-danger">No Menu found</div>
                    @endforelse
                </div>
                <div class="burger_menu_tab">
                    @forelse($restaurantMenus as $k=>$restaurantMenu)
                        <div ng-click="click{{ $restaurantMenu->slug }}()" class="burger_tab class{{ $restaurantMenu->slug }} {{ $k == 0 ? 'active' :'' }} ">
                            <img alt="{{ $restaurantMenu->name }}" src="{{ Voyager::image($restaurantMenu->image) }}">
                            <h3>{{ $restaurantMenu->name }}</h3>
                        </div>
                    @empty
                        <div class="alert alert-danger">No Menu found</div>
                    @endforelse
                </div>
            </div>
        </div>
        <div class="d-md-none">
            <div ng-controller="MainCtrl" class="burger_menu_tab" simple-accordion>
            @forelse($restaurantMenus as $k=>$restaurantMenu)
        <div class="burger_block">
            <div class="burger_tab_mob class{{ $restaurantMenu->slug }}">
                <img alt="{{ $restaurantMenu-> name}}" src="{{ Voyager::image($restaurantMenu->image) }}">
                <h3>{{ $restaurantMenu-> name}}</h3>
            </div>
            <div class="burger_block_text">
                <div class="burger_txt">
                    @php 
                    $menuItems =App\RestaurantItem::getMenuItems($restaurantMenu->id,20);
                    @endphp
                    @forelse($menuItems as $l=>$menuItem)
                    <div class="bustn_burger_lt">
                    <div class="titleSec">  
                    <a href="" data-id="{{$menuItem->id}}" data-type="restaurant" data-toggle="modal" data-target="#product_view" ng-click="updateModel($event)"><h3>{{ $menuItem-> name}}</h3></a>
                        @if(@$menuItem-> price)
                            <span class="foodprice"> $ {{ $menuItem-> price}}</span>
                        @endif
                    </div>
                        <p>{{ $menuItem-> description}}</p>
                    </div>
                    @empty
                        <div class="alert alert-danger">No Menu Item found</div>
                    @endforelse
                    <a class="view_btn" href="{{ route('menu') }}">
                        View ALL
                    </a>
                </div>
            </div>
        </div>    
        @empty
            <div class="alert alert-danger">No Menu found</div>
        @endforelse    
            </div>
        </div>
        </div>
        <div class="cateringSection" ng-show="showCatering">
                <h3 class="sub_head_mn">CATERING GETTING HUNGRY, ORDER NOW</h3>
                <div class="d-none d-md-block">
                <div class="burger_menu">
                    <div class="burger_menu_text">
                        @forelse($cateringMenus as $k=>$cateringMenu)
                        <div ng-show="show{{ $cateringMenu->slug }}" class="burger_txt">
                            @php 
                                $menuItems = App\CateringItem::getMenuItems($cateringMenu->id,20);
                            @endphp
                            @forelse($menuItems as $l=>$menuItem)
                                <div class="{{ ( $l%2 ? 'bustn_burger_rt' : 'bustn_burger_lt')}} @if($menuItem->images) @php echo  (count(json_decode($menuItem->images, true)) > 1 ) ? 'multipleImage':''; @endphp @endif ">
                                <div class="titleSec">  
                                <a href="" data-id="{{$menuItem->id}}" data-type="catering" data-toggle="modal" data-target="#product_view" ng-click="updateModel($event)">
                                <h3>{{ $menuItem->name }} 
                                  @if ($menuItem->images)
                                        @foreach (json_decode($menuItem->images, true) as $k =>$image)
                                            @if($k == 0)
                                            <img src="{{ Voyager::image($image)}}" alt="{{ $menuItem->name }}">
                                            @endif
                                            @if($k == 1)
                                            <img class="secimg" src="{{ Voyager::image($image)}}" alt="{{ $menuItem->name }}">
                                            @endif
                                        @endforeach
                                    @endif
                                </h3></a>
                                    @if(@$menuItem-> price)
                                            <span class="foodprice"> $ {{ $menuItem-> price}}</span>
                                        @endif
                                    </div>
                                    <p>{{ $menuItem->description }}</p>
                                </div>
                            @empty
                            <div class="alert alert-danger">No Menu Item found</div>
                            @endforelse
                            <a class="burger_new" href="{{ route('menu') }}">
                                <span></span>
                            </a>                          
                        </div>
                        @empty
                            <div class="alert alert-danger">No Menu found</div>
                        @endforelse
                    </div>
                    <div class="burger_menu_tab">
                        @forelse($cateringMenus as $k=>$cateringMenu)
                            <div ng-click="click{{ $cateringMenu->slug }}()" class="burger_tab class{{ $cateringMenu->slug }} {{ $k == 0 ? 'active' :'' }} ">
                                <img alt="{{ $cateringMenu->name }}" src="{{ Voyager::image($cateringMenu->image) }}">
                                <h3>{{ $cateringMenu->name }}</h3>
                            </div>
                        @empty
                            <div class="alert alert-danger">No Menu found</div>
                        @endforelse
                    </div>
                </div>
            </div>
            <div class="d-md-none">
                <div ng-controller="MainCtrl" class="burger_menu_tab" simple-accordion>
                @forelse($cateringMenus as $k=>$cateringMenu)
            <div class="burger_block">
                <div class="burger_tab_mob class{{ $cateringMenu->slug }}">
                    <img alt="{{ $cateringMenu-> name}}" src="{{ Voyager::image($cateringMenu->image) }}">
                    <h3>{{ $cateringMenu-> name}}</h3>
                </div>
                <div class="burger_block_text">
                    <div class="burger_txt">
                        @php 
                        $menuItems =App\CateringItem::getMenuItems($cateringMenu->id,20);
                        @endphp
                        @forelse($menuItems as $l=>$menuItem)
                        <div class="bustn_burger_lt">
                        <div class="titleSec">  
                        <a href="" data-id="{{$menuItem->id}}" data-type="restaurant" data-toggle="modal" data-target="#product_view" ng-click="updateModel($event)"><h3>{{ $menuItem-> name}}</h3></a>
                            @if(@$menuItem-> price)
                                <span class="foodprice"> $ {{ $menuItem-> price}}</span>
                            @endif
                        </div>
                            <p>{{ $menuItem-> description}}</p>
                        </div>
                        @empty
                            <div class="alert alert-danger">No Menu Item found</div>
                        @endforelse
                        <a class="view_btn" href="{{ route('menu') }}">
                            View ALL
                        </a>
                    </div>
                </div>
            </div>    
            @empty
                <div class="alert alert-danger">No Menu found</div>
            @endforelse    
                </div>
            </div>
        </div>
        </div>
      </div>  

      <div class="modal fade product_view" id="product_view">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  
                    <div class="modal-body">
                        <div class="row">
                          <div class="col-12">
                            <h4>@{{dataproduct.product.name}}</h4>
                            <h2 class="product-price">@{{dataproduct.product.price}}</h2>
                            <div ng-bind-html="image"></div>
                                <div class="proReview">
                               
                                <div ng-bind-html="pdescription"></div>
                                      <div class="productdtl" ng-controller="qtyCtrl">
                                            <div class="proQty">
                                              <h4>Quantity</h4>
                                                <div class="qtyCount">
                                                <button class="minusBtn" ng-click="decrement()">-</button>
                                                    <span>@{{count}}</span>
                                                  <button class="plusBtn" ng-click="increment()">+</button>
                                                </div>
                                            </div>
                                            
                                            <form action="@{{dataproduct.product.addtoCart}}" method="POST">
                                              {{ csrf_field() }}
                                              <button type="submit" class="view_btn"><i class="fab fa-opencart"></i>SHOP NOW</button>
                                              <input type="hidden"  name="quantityCount" value="@{{count}}">
                                            </form>
                                            <div class="blogDtlComment">
                                              <div class="share-section">
                                                <a target="_blank" href="@{{dataproduct.product.tlink}}" class="btn-media twitter">
                                                    <i class="fab fa-twitter" aria-hidden="true"></i>
                                                  </a>
                                                  <a target="_blank" href="@{{dataproduct.product.flink}}" class="btn-media facebook">
                                                    <i class="fab fa-facebook" aria-hidden="true"></i>
                                                  </a>
                                                  <a target="_blank" href="@{{dataproduct.product.glink}}" class="btn-media google-plus">
                                                    <i class="fab fa-google-plus" aria-hidden="true"></i>
                                                  </a>
                                              </div>
                                            </div>
                                        </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
    </div>
    </div>
          @php
          $footer=[
              'press' => true,
              'accolades' => true,
              'testimonial' => true,
              'about' => true,
              'location'=>true,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	

    @include('partials.footer')	


@endsection

@section('extra-js-footer')
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.min.js"></script>
<script>

var app = angular.module("myapp", ['simple-accordion','ngSanitize']);
        app.controller("myctrl", function($scope,$http,$sce){

            $scope.showRestaurant=true;
            $scope.cateringClick = function(){
              $scope.showCatering = true;
              $scope.showRestaurant = false;
                        }
          $scope.restaurantClick = function(){
              $scope.showRestaurant = true;
              $scope.showCatering = false;
          }
          $scope.updateModel = function($event){
        var product_id = $event.currentTarget.getAttribute('data-id');
        var product_type = $event.currentTarget.getAttribute('data-type');
                $http({
                  method: 'post',
                  url:"{{ route('shop.showajax-catering') }}",
                  data: {
                      product_id: product_id,
                    product_type:product_type
                   }
                }).then(function successCallback(response) {
                      $scope.dataproduct = response.data;
                      $scope.pdescription = $sce.trustAsHtml($scope.dataproduct.product.description);
                      $scope.image = $sce.trustAsHtml($scope.dataproduct.product.image);
                });
        
      }
      jQuery.urlParams = function(name){
            if((window.location.href).search("type=")>1){
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                return results[1] || 0;
                $scope.showCatering = true;
                $scope.showRestaurant = false;
                }
            }
            var type = jQuery.urlParams('type');
                $scope.showCatering = true;
                $scope.showRestaurant = false;

            $scope.showburgers = true;
            $scope.clickburgers = function () 
            {
                $scope.showburgers = true;
                $scope.showfreakfrappes = false;
                $scope.showsandwiches = false;
                $scope.showappetizers = false;
                $scope.showsalads = false;
            }
            $scope.clickfreakfrappes = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= true;
                $scope.showsandwiches= false;
                $scope.showappetizers= false;
                $scope.showSalad= false;
            }
            $scope.clicksandwiches = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= false;
                $scope.showsandwiches= true;
                $scope.showappetizers= false;
                $scope.showsalads= false;
            }
            $scope.clickappetizers = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= false;
                $scope.showsandwiches= false;
                $scope.showappetizers= true;
                $scope.showsalads= false;
            }
            $scope.clicksalads = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= false;
                $scope.showsandwiches= false;
                $scope.showappetizers= false;
                $scope.showsalads= true;
            }
            jQuery.urlParam = function(name){
            if((window.location.href).search("cat=")>1){
                var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
                return results[1] || 0;
                }
            }
            var cat = jQuery.urlParam('cat');
            console.log(cat);
            if(cat){
                if(cat =="sandwiches"){
                    $scope.clicksandwiches();
                }else if(cat =="freakfrappes"){
                    $scope.clickfreakfrappes();
                }else if(cat =="salads"){
                    $scope.clicksalads();
                }else if(cat =="appetizers"){
                    $scope.clickappetizers();
                }else{
                    $scope.clickburgers();
                }
                angular.element(".class"+cat).triggerHandler('click');
             //   document.querySelector(".class"+cat).click();
            }
        });
        app.controller('qtyCtrl', ['$scope', function($scope) {
            $scope.count = 1;
            $scope.increment = function() {
              $scope.count++;
            };
            $scope.decrement = function() {
              console.log($scope.count);
              if($scope.count <= 1){
                $scope.count = 1 ;
              }
              else{
                $scope.count--;
                }
            };
          }]);

        app.controller('classCtrl', function ($scope) {
          $scope.isActive = false;
          $scope.activeButton = function() {
            $scope.isActive = !$scope.isActive;
          }  
        });

        app.controller('MainCtrl', function($scope) {

         });

        angular.module('simple-accordion', [])
          .directive('simpleAccordion', function($compile, $timeout) {
              return {
                  restrict: 'AEC',
                  controller: function($scope) {
                      $scope.current = null;
                      $scope.height = [];
                      $scope.zero = {
                          height: 0
                      };
                      $scope.toggle = function(i) {
                          $scope.current = $scope.current === i ? null : i;
                      };
                  },
                  link: function(scope, el, attrs) {
                    var itemSelector = attrs.itemSelector || '.ftr_accordian , .burger_block',
                        titleSelector = attrs.titleSelector || '.ftr_head_mob, .burger_tab_mob',
                        contentSelector = attrs.contentSelector || '.ftr_link_mob ul, .burger_block_text';
                    $timeout(function(){
                      var items = el[0].querySelectorAll(itemSelector);
                      for (var i in items) {
                        if (angular.isObject(items[i])) {
                          var title = items[i].querySelectorAll(titleSelector)[0];
                          var content = items[i].querySelectorAll(contentSelector)[0];
                          scope.height.push({
                            'max-height': (content.offsetHeight + 10) + 'px'
                          });
                          angular.element(items[i]).addClass('item').attr({
                            'ng-class': '{\'open\':current == ' + i + '}'
                          });
                          angular.element(title).addClass('title').attr('ng-click', 'toggle(' + i + ')');
                          angular.element(content).addClass('content').attr({
                            'ng-style': 'current == ' + i + '?height[' + i + ']:zero'
                          });
                        }
                      }
                      $compile(angular.element(el).contents())(scope);
                    });
                  }
              }
          });
          $('.burger_tab').on('click', function(){
            $('.burger_tab.active').removeClass('active');
            $(this).addClass('active');
          });
          $(document).ready(function(){
           
          });
    </script>
    
@endsection