@extends('layouts.master')

@section('body-class',"blog-main")


@section('content')



    <div class="innerPagesBg gallery_Sr gallery_videDtl">
      <div class="container"> 
        <div class="row">
          <div class="col gallery_left">
            <div class="blog-detail">
              <div class="gallery_section">
                <h2>{{ strtoupper($post->title) }}</h2>
              </div>
              <div class="extra" style="display:none">
                  {{ @$post->category->name ? $post->category->name : '' }}{{ date('F d, Y',strtotime($post->created_at)) }}
                  </div>
          
          <div class="gallery_video">
          <iframe width="560" height="315" src="{{$post->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </div>
          <div class="blog-dtl-text">
          {!! $post->body !!}
          </div>
        
        <div class="comment-section" style="display:none">
          <h2> Comments</h2>

          @if(@Auth::user())
          <form action="{{ route('blog.comment.store',$post->id) }}" method="POST">
            @csrf
            @method('POST')
            <textarea name="comment"  cols="50" rows="3"></textarea>
            <button type="submit" class="view_btn">Submit</button>
          </form>
          @endif

          @php 
          $comments = App\Comment::where('status','Approved')->where('post_id',$post->id)->get();
          @endphp
            @forelse($comments as $comment)
            <div class="commentShow">
              <div class="commentShowtitle">{{$comment->user->name}} <span>{{$comment->user->email}}</span></div>
              <div class="commentTxt">{{$comment->comment}}</div>
            </div>
          
          @empty
          <div class="alert alert-error">No comment Found</div>
          @endforelse
        </div>
       </div>
        <div class="bottmSlide">
            <div class="leftVideo">
                @if (isset($previous))
                    <a href="{{ route('blog.video',$previous->slug)}}">
                        <div class="btn-content">
                            <div class="btn-content-title"><i class="fa fa-arrow-left"></i> Previous Video</div>
                            <!-- <p class="btn-content-subtitle">{{ $previous->title }}</p> -->
                        </div>
                    </a>
                @endif
            </div>

          <div class="blogDtlComment" >
              <div class="likes-section" style="display:none;float:right">
              @php echo  $likes = App\Like::where('type',1)->where('parent_id',$post->id)->count();  @endphp

              @if(@Auth::user())
                <form action="{{ route('blog.like.store',$post->id) }}" method="POST">
                  @csrf
                  @method('POST')
                  @php $like = App\Like::where('type',1)->where('parent_id',$post->id)->where('user_id',Auth::user()->id)->count(); @endphp
                  @if($like)
                  <button type="submit"> <i class="liked fas fa-heart" aria-hidden="true"></i> <span>Like(s)</span></button>
                  @else
                  <button type="submit"> <i class="fas fa-heart" aria-hidden="true"></i> <span>Like(s)</span></button>
                  @endif
                  
                </form>
                @endif
              </div>


              <div class="share-section">
              <a target="_blank" href="https://twitter.com/share?url={{ urlencode(url()->current())}}&via=TWITTER_HANDLE&text={{$post->title}}" class="btn-media twitter">
                  <i class="fab fa-twitter" aria-hidden="true"></i>
                </a>
                <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn-media facebook">
                  <i class="fab fa-facebook" aria-hidden="true"></i>
                </a>
                <a target="_blank" href="https://plus.google.com/share?url={{ urlencode(url()->current()) }}" class="btn-media google-plus">
                  <i class="fab fa-google-plus" aria-hidden="true"></i>
                </a>
              </div>
            </div>
            <div class="rightVideo">
                @if (isset($next))
                <a href="{{ route('blog.video',$next->slug)}}">
                    <div class="btn-content">
                        <div class="btn-content-title">Next Video <i class="fa fa-arrow-right"></i></div>
                        <!-- <p class="btn-content-subtitle">{{ $next->title }}</p> -->
                    </div>
                </a>
                @endif
            </div>
        </div>
          </div>

          @include('partials.blog-sidebar')

        </div>

      </div>
      
    </div>
    @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
    @include('partials.footer')	
@endsection

@section('extra-js-footer')
    
@endsection