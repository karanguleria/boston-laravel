@extends('layouts.master')


@section('content')


    <div class="innerPagesBg gallery_Sr">
      <div class="container">
        <div class="row">
          <div class="col gallery_left">
            @foreach($videos as $video)
            <div class="gallery_section glryVideo">
              <div class="gallery_video">
              <div class="product-section-thumbnail">
              <a href="{{ route('blog.video',$video->slug)}}"><img src="{{ URL('storage/'.$video->image) }}" alt="{{ $video->title }}"></a>
                    </div>
                   <!--  <div class="bugur_video">
                      <div class="more_video">
                        <a href="{{ route('blog.video',$video->slug)}}">{{ $video->title }}</a>
                      </div>
                      <div class="br_video">
                      <a href="{{ route('blog.video',$video->slug)}}"><span>watch Video</span></a>
                      </div>
                    </div> -->
              </div>
              <div class="blog-shareSec">
                  <h2> <a href="{{ route('blog.video',$video->slug)}}">{{ strtoupper($video->title) }}</a></h2>
                  <div class="blog_text">
                    <ul>
                          <li style="display:none">
                            <a href="#" class="disabledAnchor"><i class="fa fa-comment" aria-hidden="true"></i>
                            <span>@php echo $comments = App\Comment::where('post_id',$video->id)->where('status','Approved')->count(); @endphp
                            Comments</span></a>
                          </li>
                          <li style="display:none">
                            <a href="#" class="disabledAnchor"><i class="fa fa-heart" aria-hidden="true"></i>
                            <span> @php echo $likes = App\Like::where('parent_id',$video->id)->where('type',1)->count(); @endphp 
                            Likes</span></a>
                          </li>
                          <li>
                            <a href="#" class="disabledAnchor sharebtn" id="post{{$video->id}}">
                              <i class="fa fa-share" aria-hidden="true"></i>
                              <span>Share</span>
                            </a>
                            <div class="share-section post{{$video->id}}" style="display:none">
                            <a target="_blank" href="https://twitter.com/share?url={{ urlencode(url()->current())}}&via=TWITTER_HANDLE&text={{$video->title}}" class="btn-media twitter">
                                <i class="fab fa-twitter" aria-hidden="true"></i> <span>Twitter</span>
                              </a>
                              <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn-media facebook">
                                <i class="fab fa-facebook" aria-hidden="true"></i> <span>Facebook</span>
                              </a>
                              <a target="_blank" href="https://plus.google.com/share?url={{ urlencode(url()->current()) }}" class="btn-media google-plus">
                                <i class="fab fa-google-plus" aria-hidden="true"></i> <span>Google</span>
                              </a>
                            </div>
                          </li>
                        </ul>
                      </div>
             </div>
            </div>
            @endforeach
            <div class="more more_list_item">
              <a href="javascript:void(0)" class="view_btn" id="homeloadmore">View More</a>
            </div>
          </div>
          @include('partials.blog-sidebar')
        </div>
      </div>
      
    </div>

    @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
    @include('partials.footer')	
@endsection

@section('extra-js-footer')
  <script>
    $(document).ready(function(){
        $('.gallery_section').css('display', 'none');
        //        -------------load more-------------
        $(".gallery_section").slice(0, 6).show();
        if ($(".gallery_section").length < 6) {
            $(".more_list_item").fadeOut('slow');
        }
        $(".more_list_item a").on('click', function (e) {
            e.preventDefault();
            $(".gallery_section:hidden").slice(0, 6).slideDown();
            if ($(".gallery_section:hidden").length === 0) {
                $(".more_list_item").fadeOut('slow');
            }
        });
   $(".sharebtn").click(function(){
     var section_class = $(this).attr('id');
     $("."+section_class).slideToggle();

   });
});
</script>
    
@endsection