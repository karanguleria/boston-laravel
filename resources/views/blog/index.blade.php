@extends('layouts.master')

@section('body-class',"blog-main")


@section('content')


  <div class="innerPagesBg gallery_Sr">
        <div class="container">
          <div class="row">
            <div class="col gallery_left">
              @forelse($posts as $post)
                <div class="blog_section">
                  <div class="row">
                    <div class="col-md-6 col-12 blog_img">
                    <a href="{{ route('blog.show',$post->slug)}}"><img src="{{ URL('storage/'.$post->image) }}" alt="{{ $post->title }}"></a>
                    </div>
                    <div class="col-md-6 col-12 blog_text">
                    <a href="{{ route('blog.show',$post->slug)}}"><h3>{{ strtoupper($post->title) }}</h3></a>
                    <div style="display:none">
                      {{ @$post->category->name ? $post->category->name : '' }}
                      {{ date('F d, Y',strtotime($post->created_at)) }}
                    </div>
                    <ul>
                      <li style="display:none">
                        <a href="#" class="disabledAnchor"><i class="fa fa-comment" aria-hidden="true"></i>
                        <span>@php echo $comments = App\Comment::where('post_id',$post->id)->where('status','Approved')->count(); @endphp
                        Comments</span></a>
                      </li>
                      <li style="display:none">
                        <a href="#" class="disabledAnchor"><i class="fa fa-heart" aria-hidden="true"></i>
                        <span> @php echo $likes = App\Like::where('parent_id',$post->id)->where('type',1)->count(); @endphp 
                        Likes</span></a>
                      </li>
                      <li>
                        <a href="#" class="disabledAnchor sharebtn" id="post{{$post->id}}">
                          <i class="fa fa-share" aria-hidden="true"></i>
                          <span>Share</span>
                        </a>
                        <div class="share-section post{{$post->id}}" style="display:none">
                        <a target="_blank" href="https://twitter.com/share?url={{ urlencode(url()->current())}}&via=TWITTER_HANDLE&text={{$post->title}}" class="btn-media twitter">
                            <i class="fab fa-twitter" aria-hidden="true"></i> <span>Twitter</span>
                          </a>
                          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn-media facebook">
                            <i class="fab fa-facebook" aria-hidden="true"></i> <span>Facebook</span>
                          </a>
                          <a target="_blank" href="https://plus.google.com/share?url={{ urlencode(url()->current()) }}" class="btn-media google-plus">
                            <i class="fab fa-google-plus" aria-hidden="true"></i> <span>Google</span>
                          </a>
                        </div>
                      </li>
                    </ul>
                    <p>{{ $post->excerpt }}</p>
                    <a class="view_btn" href="{{ route('blog.show',$post->slug)}}">Read More</a>
                    </div>
                  </div>  
                </div>

                @empty
              <div class="alert alert-danger">
                No Posts available
              </div>

              @endforelse
            
            <div class="row lr-paginator">
              {{ $posts->links('') }}
            </div>
            
          </div>
          @include('partials.blog-sidebar')
        </div>
      </div>
  </div>
  @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
    @include('partials.footer')	

@endsection

@section('extra-js-footer')
<script>
$(document).ready(function(){
   $(".sharebtn").click(function(){
     var section_class = $(this).attr('id');
     $("."+section_class).slideToggle();

   });
});
</script>
    
@endsection