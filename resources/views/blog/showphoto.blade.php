@extends('layouts.master')

@section('body-class',"blog-main")


@section('content')


  
    <div class="innerPagesBg gallery_Sr gallery_videDtl">
      <div class="container"> 
        <div class="row">
          <div class="col gallery_left">
            <div class="blog-detail">
              <div class="gallery_section">
                <h2>{{ strtoupper($post->title) }}</h2>
              </div>
              <div class="extra" style="display:none">
                  {{ @$post->category->name ? $post->category->name : '' }}{{ date('F d, Y',strtotime($post->created_at)) }}
                  </div>
          
          <div class="gallery_video">
          <div id="owl-1" class="owl-carousel owl-theme">
                  <div class="item">
                    <img src="{{ imageUrl(productImage($post->image),'670','350') }}"  class="image" alt="{{ $post->title }}">
                  </div>
                  @if ($post->gallery)
                    @foreach (json_decode($post->gallery, true) as $image)
                    <div class="item">
                        <img class="image" src="{{ imageUrl(productImage($image),'670','350') }}" alt="{{ $post->title }}">
                    </div>
                    @endforeach
                @endif
                  
                    
                                
              </div>
          <div id="owl-2" class="owl-carousel owl-theme">
                  <div class="item">
                    <img src="{{ imageUrl(productImage($post->image),'150','100') }}"  class="image" alt="{{ $post->title }}">
                  </div>
                  @if ($post->gallery)
                    @foreach (json_decode($post->gallery, true) as $image)
                    <div class="item">
                        <img  class="image" src="{{ imageUrl(productImage($image),'150','100') }}" alt="{{ $post->title }}">
                    </div>
                    @endforeach
                @endif
                         
              </div>
              
          </div>
          <!-- <div class="blog-dtl-text">
          {!! $post->body !!}
          </div> -->
        </div>
        
        <div class="comment-section" style="display:none">
          <h2> Comments</h2>

          @if(@Auth::user())
          <form action="{{ route('blog.comment.store',$post->id) }}" method="POST">
            @csrf
            @method('POST')
            <textarea name="comment"  cols="50" rows="3"></textarea>
            <button type="submit" class="view_btn">Submit</button>
          </form>
          @endif

          @php 
          $comments = App\Comment::where('status','Approved')->where('post_id',$post->id)->get();
          @endphp
            @forelse($comments as $comment)
            <div class="commentShow">
              <div class="commentShowtitle">{{$comment->user->name}} <span>{{$comment->user->email}}</span></div>
              <div class="commentTxt">{{$comment->comment}}</div>
            </div>
          
          @empty
          <div class="alert alert-error">No comment Found</div>
          @endforelse
        </div>
        <div class="bottmSlide">
    <div class="leftVideo">

        @if (isset($previous))
            <a href="{{ route('blog.photo',$previous->slug)}}">
                <div class="btn-content">
                    <div class="btn-content-title"><i class="fa fa-arrow-left"></i> Previous Photo</div>
                    <!-- <p class="btn-content-subtitle">{{ $previous->title }}</p> -->
                </div>
            </a>
        @endif
    </div>
        <div class="blogDtlComment" >
        
        
        <div class="likes-section" style="display:none;float:right">
        @php echo  $likes = App\Like::where('type',1)->where('parent_id',$post->id)->count();  @endphp

        @if(@Auth::user())
          <form action="{{ route('blog.like.store',$post->id) }}" method="POST">
            @csrf
            @method('POST')
            @php $like = App\Like::where('type',1)->where('parent_id',$post->id)->where('user_id',Auth::user()->id)->count(); @endphp
            @if($like)
            <button type="submit"> <i class="liked fas fa-heart" aria-hidden="true"></i> <span>Like(s)</span></button>
            @else
            <button type="submit"> <i class="fas fa-heart" aria-hidden="true"></i> <span>Like(s)</span></button>
            @endif
            
          </form>
          @endif
        </div>


        <div class="share-section">
        <a target="_blank" href="https://twitter.com/share?url={{ urlencode(url()->current())}}&via=TWITTER_HANDLE&text={{$post->title}}" class="btn-media twitter">
            <i class="fab fa-twitter" aria-hidden="true"></i>
          </a>
          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn-media facebook">
            <i class="fab fa-facebook" aria-hidden="true"></i>
          </a>
          <a target="_blank" href="https://plus.google.com/share?url={{ urlencode(url()->current()) }}" class="btn-media google-plus">
            <i class="fab fa-google-plus" aria-hidden="true"></i>
          </a>
        </div>
        </div>
    <div class="rightVideo">

        @if (isset($next))
        <a href="{{ route('blog.photo',$next->slug)}}">
            <div class="btn-content">
                <div class="btn-content-title">Next Photo <i class="fa fa-arrow-right"></i></div>
               <!--  <p class="btn-content-subtitle">{{ $next->title }}</p> -->
            </div>
        </a>
        @endif
    </div>
</div>
       
          </div>

          @include('partials.blog-sidebar')

        </div>

      </div>
      
    </div>
    @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
    @include('partials.footer')	
@endsection

@section('extra-js-footer')
<script type="text/javascript">
      $(document).ready(function(){
  
        var owl_1 = $('#owl-1');
        var owl_2 = $('#owl-2');
        
        owl_1.owlCarousel({
          loop:true,
          margin:10,
          nav:false,
          items: 1,
          dots: false
        });
        
        owl_2.owlCarousel({
          margin:10,
          nav: true,
          items: 4,
          dots: false
        });
        
        owl_2.find(".item").click(function(){
          var slide_index = owl_2.find(".item").index(this);
          owl_1.trigger('to.owl.carousel',[slide_index,300]);
        });
      });
        </script>
@endsection