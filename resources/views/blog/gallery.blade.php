@extends('layouts.master')

@section('banner-title',"Media")

@section('content')

  @include('partials.inner-banner')

      <div class="sub_pages gallery_Sr">
        <div class="container">
          <div class="row">
            <div class="col gallery_left">
              @if(@$video)
              <div class="gallery_section glryVideo">
                <div class="gallery_video">
                  <iframe width="560" height="315" src="{{$video->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="bugur_video">
                    <div class="more_video">
                      <a href="{{route('blog.videos')}}">More Videos</a>
                    </div>
                    <div class="br_video">
                    <a href="{{route('blog.videos')}}"><span>{{strtoupper($video->type)}}</span></a>
                    </div>
                  </div>
                </div>
              <div class="blog-shareSec">   
                <h2>{{strtoupper($video->title)}}</h2>
              </div>
              </div>
              @endif
              @if(@$photo)
              <div class="gallery_section">
                <div class="gallery_video">
                @if ($photo->gallery)
                    <div class="product-section-thumbnail">
                        <img src="{{ URL('storage/'.json_decode($photo->gallery, true)[0]) }}" alt="product">
                    </div>
                  @endif
                  <div class="bugur_video">
                    <div class="more_video">
                      <a href="{{route('blog.photos')}}">More Photos</a>
                    </div>
                    <div class="br_video">
                      <span>Photos</span>
                    </div>
                  </div>
                </div>
              <div class="blog-shareSec">   
                <h2>{!!strtoupper($photo->title) !!}</h2>
              </div>
              </div>
              @endif
              @if(@$blog)
              <div class="gallery_section">
                <div class="gallery_video">
                    <div class="product-section-thumbnail">
                        <img src="{{ URL('storage/'.$blog->image) }}" alt="{{strtoupper($blog->title) }}">
                    </div>
                  <div class="bugur_video">
                    <div class="more_video">
                      <a href="{{route('blog.index')}}">More Blog</a>
                    </div>
                    <div class="br_video">
                      <span>Blog</span>
                    </div>
                  </div>
                </div>
              <div class="blog-shareSec">                
                <h2>{!!strtoupper($blog->title) !!}</h2>
              </div>  
              </div>
              @endif
              @if(@$video_next)
              <div class="gallery_section glryVideo">
                <div class="gallery_video">
                  <iframe width="560" height="315" src="{{$video_next->url}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                  <div class="bugur_video">
                    <div class="more_video">
                      <a href="{{route('blog.videos')}}">More Videos</a>
                    </div>
                    <div class="br_video">
                    <a href="{{route('blog.videos')}}"><span>{{strtoupper($video_next->type)}}</span></a>
                    </div>
                  </div>
                </div>
              <div class="blog-shareSec"> 
                <h2>{{strtoupper($video_next->title)}}</h2>
              </div>  
              </div>
              @endif
              @if(@$photo_next)
              <div class="gallery_section">
                <div class="gallery_video">
                @if ($photo_next->gallery)
                    <div class="product-section-thumbnail">
                        <img src="{{ URL('storage/'.json_decode($photo_next->gallery, true)[0]) }}" alt="product">
                    </div>
                  @endif
                  <div class="bugur_video">
                    <div class="more_video">
                      <a href="{{route('blog.photos')}}">More Photos</a>
                    </div>
                    <div class="br_video">
                      <span>Photos</span>
                    </div>
                  </div>
                </div>

              <div class="blog-shareSec"> 
                <h2>{!!strtoupper($photo_next->title) !!}</h2>
              </div>  
              </div>
              @endif
              @if(@$blog_next)
              <div class="gallery_section">
                <div class="gallery_video">
                    <div class="product-section-thumbnail">
                        <img src="{{ URL('storage/'.$blog->image) }}" alt="{{strtoupper($blog->title) }}">
                    </div>
                  <div class="bugur_video">
                    <div class="more_video">
                      <a href="{{route('blog.index')}}">More Blog</a>
                    </div>
                    <div class="br_video">
                      <span>Blog</span>
                    </div>
                  </div>
                </div>

              <div class="blog-shareSec"> 
                <h2>{!!strtoupper($blog->title) !!}</h2>
              </div>  
              </div>
              @endif
              <div class="more more_list_item">
              <a href="javascript:void(0);" class="view_btn" id="homeloadmore">View More</a>
            </div>
            </div>

            @include('partials.blog-sidebar')
            
          </div>
          @php
          $footer=[
              'press' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
        </div>
        
      </div>


    @include('partials.footer')	


@endsection

@section('extra-js-footer')
    

  <script>
     $(document).ready(function(){
        $('.gallery_section').css('display', 'none');
        //        -------------load more-------------
        $(".gallery_section").slice(0, 3).show();
        if ($(".gallery_section").length < 4) {
            $(".more_list_item").fadeOut('slow');
        }
        $(".more_list_item a").on('click', function (e) {
            e.preventDefault();
            $(".gallery_section:hidden").slice(0, 3).slideDown();
            if ($(".gallery_section:hidden").length === 0) {
                $(".more_list_item").fadeOut('slow');
            }
        });
      });
  </script>

@endsection