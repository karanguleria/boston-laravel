@extends('layouts.master')

<!-- @section('banner-title',"ORDER Delivery") -->

@section('content')

  <!-- @include('partials.inner-banner') -->
    <div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container">
          <h3 class="sub_head_mn">ORDER FOR DELIVERY</h3>
					<div class="store">
						<div class="row">
							<h2>GrubHub</h2>
							<div>
								<img src="images/Grubhub-logo.png" alt="">
								<p>GrubHub lets customers order food for delivery quickly and easily through their app. Similar to how Uber works for rides, a driver will show up to your location that you selected within the app with your order. It doesn’t get any easier than this. Prepare the orders that come in and wait for the driver to arrive at your restaurant to pick it up for you.</p>
							</div>
						</div>
						<div class="row">
							<h2>UberEATS</h2>
							<div>
								<img src="images/Uber-Eats.png" alt="">
								<p>UberEATS lets customers order food for delivery quickly and easily through their app. Similar to how Uber works for rides, a driver will show up to your location that you selected within the app with your order. It doesn’t get any easier than this. Prepare the orders that come in and wait for the driver to arrive at your restaurant to pick it up for you.</p>
							</div>
						</div>
						<div class="row">
							<h2>DoorDash</h2>
							<div>
								<img src="images/doordash.png" alt="">
								<p>DoorDash lets customers order food for delivery quickly and easily through their app. Similar to how Uber works for rides, a driver will show up to your location that you selected within the app with your order. It doesn’t get any easier than this. Prepare the orders that come in and wait for the driver to arrive at your restaurant to pick it up for you.</p>
							</div>
						</div>
					</div><!-- store end-->
        </div>	@php
          $footer=[
              'press' => false,
          ]; 
		@endphp
		
    @include('partials.sub-footer',$footer)	
      </div>  
    </div>

    @include('partials.footer')	



@endsection

@section('extra-js-footer')
   
@endsection