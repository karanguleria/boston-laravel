@extends('layouts.master')

@section('banner-title',"ORDER NOW")
@section('body-class','subPages')

@section('content')

  @include('partials.inner-banner')
    <div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container">
          <h3 class="sub_head_mn">ORDER NOW BOSTON BURGER COMPANY</h3>
					<div class="store">
					<div class="row catering-content">
							<div class="col-lg-6 col-12 image-col">
							<div class="image-box">
								<img src="images/finalcater2.jpg" alt="Order Catering"/>
							</div>
							</div>
							<div class="col-lg-6 col-12 text-col">
								<div class="text-box">
									<h3 class="sub_head_mn">Order Catering</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since it to make a type specimen book.</p>
									<a href="{{route('catering.category')}}" class="view_btn">ORDER NOW</a>
								</div>
							</div>
						</div>
						<div class="row catering-content">
							<div class="col-lg-6 col-12 image-col">
							<div class="image-box">
								<img src="images/finalcater2.jpg" alt="Order for Pick-up"/>
							</div>
							</div>
							<div class="col-lg-6 col-12 text-col">
								<div class="text-box">
									<h3 class="sub_head_mn">Order for Pick-up</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since it to make a type specimen book.</p>
									<a href="{{route('restaurant.category')}}" class="view_btn">ORDER NOW</a>
								</div>
							</div>
						</div>
						<div class="row catering-content">
							<div class="col-lg-6 col-12 image-col">
							<div class="image-box">
								<img src="images/finalcater2.jpg" alt="Order for Delivery"/>
							</div>
							</div>
							<div class="col-lg-6 col-12 text-col">
								<div class="text-box">
									<h3 class="sub_head_mn">Order for Delivery</h3>
									<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since it to make a type specimen book.</p>
									<a href="{{route('catering.orderDelivery')}}" class="view_btn">ORDER NOW</a>
								</div>
							</div>
						</div>
					</div><!-- store end-->
        </div>
					@php
          $footer=[
              'press' => true,
              'accolades' => true,
              'testimonial' => true,
							'about' => true,
							'location' => true,
          ]; 
		@endphp
		
    @include('partials.sub-footer',$footer)	
      </div>  
    </div>

		
    @include('partials.footer')	



@endsection

@section('extra-js-footer')
   
@endsection