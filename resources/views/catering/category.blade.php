@extends('layouts.master')

@section('banner-title',"Order Catering")

@section('content')

  @include('partials.inner-banner')
    <div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container">
          <h3 class="sub_head_mn">Catering BOSTON BURGER COMPANY</h3>
					<div class="store strorePg">
                        @forelse($categories as $category)
                            <h2>{{ $category->name}}</h2>
                            @php 
                            $items = App\CateringItem::where('category_id',$category->id)->get();
                            @endphp
                                @forelse($items as $item)
                            <div class="catering_detailPg">    
                                <div class="row">
                                    <div class="col-md-6">
                                        <h4><a href="{{route('catering.show',$item->slug)}}">{{$item->name}}</a> <span>$ {{$item->price}}</span></h4>
                                        <p>{{$item->description}}</p>
                                    </div>
                                    <div class="col-md-6">
                                    <form action="{{ route('cart.catering-store', $item) }}" method="POST">
                                        {{ csrf_field() }}
                                        <button type="submit" class="view_btn"><i class="fab fa-opencart"></i>Add to Cart</button>
                                        <input type="hidden"  name="quantityCount" value="1">
                                    </form>
                                    </div>
                                </div>
                            </div>
                                @empty
                                <div class="alert alert-danger">
                                    No Item Found
                                </div>
						        @endforelse
                            @empty
                                <div class="alert alert-danger">
                                    No Category Found
                                </div>
						@endforelse
					</div><!-- store end-->
        </div>
        @include('partials.testimonials')
		@php
          $footer=[
              'press' => true,
              'accolades' => true,
          ]; 
		@endphp
		
    @include('partials.sub-footer',$footer)	
      </div>  
    </div>
   
		
    @include('partials.footer')	



@endsection

@section('extra-js-footer')
    
@endsection