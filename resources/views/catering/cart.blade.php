@extends('layouts.master')

@section('banner-title',"Catering Cart")

@section('body-class','blog-main')

@section('content')

  @include('partials.product-banner')
    <div class="product-lisening-main innerPagesBg">
      <div class="container">
      @if (session()->has('success_message'))
                <div class="alert alert-success">
                    {{ session()->get('success_message') }}
                </div>
            @endif

            @if(count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <div class="productCart">
             <div class="contiShop">
            <a href="{{ route('catering.category') }}">
              Continue Shopping <i class="fas fa-long-arrow-alt-right"></i>
            </a>
          </div>
        <div class="productTable">
         
        @if (Cart::instance('catering')->count() > 0)
        <table class="table table-bordered text-center">
          <thead>
            <tr>
              <th scope="col" class="text-left">PRODUCTS</th>
              <th scope="col">PRICE</th>
              <th scope="col">QUANTITY</th>
              <!-- <th scope="col">SIZE</th> -->
              <th scope="col">TOTAL</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
            
          @foreach (Cart::instance('catering')->content() as $item) 
            <tr>
              <td class="text-left align-middle">
                <a href="#">
                  <div class="cart_sec">
                   
                    <div class="cartText">
                    <a href=""><h4>{{ strtoupper(str_limit($item->name, 40, '...')) }}</h4></a>
                      <!-- <ul>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                        <li><i class="fas fa-star"></i></li>
                      </ul> -->
                    </div>
                  </div>
                </a>  
              </td>
              <td class="align-middle">{{ presentPrice($item->price)}}</td>
              <td class="align-middle">
                <div class="qtyCount">
                 <a class="minusBtn disabledAnchor" href="#" data-id="{{ $item->rowId }}" data-type="minus" data-qty="{{ $item->qty }}">-</a>
                    <span>{{ $item->qty }}</span>
                  <a class="plusBtn disabledAnchor" href="#" data-id="{{ $item->rowId }}" data-type="plus" data-qty="{{ $item->qty }}">+</a>
                </div>  
              </td>
              <!-- <td class="align-middle">
                <div class="size">
                  @if(@$item->options->variant)
                  <button class="sizeCart">{{ $item->options->variant }}</button>
                  @endif
                </div>
              </td> -->
              <td class="align-middle">{{ presentPrice($item->subtotal) }}</td>
              <td class="align-middle">
              <form action="{{ route('cart.catering-destroy', $item->rowId) }}" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="cart-options"><img src="{{ asset('images/del.png') }}"></button>
                            </form>
                            </td>
            </tr>
            @endforeach
            @if (! session()->has('coupon'))
            <tr style="display:none">
              <td class="align-middle text-left"  colspan="1">
                <div class="apply_coupn">
                  <form action="{{ route('coupon.store') }}" method="POST">
                        {{ csrf_field() }}
                        <input type="text" placeholder="Coupon Code" name="coupon_code" id="coupon_code">
                        <button type="submit" class="button ">Apply Coupon</button>
                    </form>  
                </div>
              </td>
              <!-- <td class="align-middle text-right update_CBtn"  colspan="5">
                <a href="#" class="view_btn">update cart</a>
              </td> -->
            </tr>
            @endif
          </tbody>
        </table>
        </div>
        <div class="tableTotal">
          <table class="table table-bordered">
            <tr>
              <td class="secTotal">Subtotal</td>
              <td class="text-right">{{ presentPrice(Cart::instance('catering')->subtotal()) }}</td>
            </tr>
            @if (session()->has('coupon'))
            <tr>
              <td class="secTotal">Code ({{ session()->get('coupon')['name'] }}) 
              <form action="{{ route('coupon.destroy') }}" method="POST" style="display:inline-block;">
                                {{ csrf_field() }}
                                {{ method_field('delete') }}
                                <button type="submit" style="font-size:12px;">Remove</button>
                            </form>
              </td>
              <td class="text-right">-{{ presentPrice($discount) }}</td>
            </tr>
            <tr>
              <td class="secTotal">New Subtotal</td>
              <td class="text-right">{{ presentPrice($newSubtotal) }}</td>
            </tr>
            @endif
            <tr>
              <td class="secTotal">Tax ({{config('cart.tax')}}%)</td>
              <td class="text-right">{{ presentPrice($newTax) }}</td>
            </tr>
            <tr>
              <td class="secTotal">Total</td>
              <td class="text-right">{{ presentPrice($newTotal) }}</td>
            </tr>
            <tr>
              <td  colspan="2">
                <a href="{{ route('checkout.catering-index') }}" class="view_btn">Proceed to Checkout</a>
              </td>
            </tr>
          </table>
        </div>
        @else
        <div class="emtyCart">
<img src="{{ asset('images/bag.png') }}">
<h3>No item in Cart!</h3>
<a href="#" class="view_btn">Add items from wishlist</a>
</div>
<!-- <a href="{{ route('store.index') }}" class="button">Continue Shopping</a> -->
@endif
      </div>
      </div>
    </div>

    @php
          $footer=[

              'press' => false,
             
          ]; 
    @endphp



 @include('partials.sub-footer',$footer)	
 @include('partials.footer')	


@endsection


@section('extra-js-footer')
<script>
   $("a.plusBtn, a.minusBtn").click(function(e){
    var qty = $(this).data('qty') ;
    if($(this).data('type')=="plus"){
        var qty = ++qty ;
        }
        else {
            
            var qty = --qty;
            
        }
        if(qty > 0){
       $.ajax({
               type:'PATCH',
               url: '<?php echo URL::to("/"); ?>/catering-cart/'+ $(this).data('id'),
               data:{'_token'  : '<?php echo csrf_token() ?>','quantity' : qty},
                  success:function(data) {
                    window.location.href = '{{ route('cart.catering-index') }}'
               }
            });
        }
   });
       
    </script>
@endsection('extra-js-footer')