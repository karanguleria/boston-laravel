@extends('layouts.master')

<!-- @section('banner-title',"Product") -->

@section('body-class','blog-main')

@section('content')

<!-- @include('partials.product-banner') -->

<style>
.rating {
    color: #a9a9a9;
    margin: 0;
    padding: 0;
}
ul.rating {
    display: inline-block;
}
.rating li {
    list-style-type: none;
    display: inline-block;
    padding: 1px;
    text-align: center;
    font-weight: bold;
    cursor: pointer;
    font-size:20px;
}
.rating .filled {
    color: orangered;
}
.clear{
  margin-top:20px;
}
i.fas.fa-heart.liked {
    color: #e1261c;
}
.listeningImg{
  text-align:center
}
</style>

    <div class="product-lisening-main innerPagesBg">
      <div class="container">

      @if (session()->has('success_message'))
            <div class="alert alert-success">
                {{ session()->get('success_message') }}
            </div>
        @endif

        @if(count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="productPg">
          <div class="row">
            <div class="col-12 col-md-7 productLeft">
              
              <div id="owl-1" class="owl-carousel owl-theme">
                  <div class="item">
                    <img src="{{ imageUrl(productImage($product->image),'670') }}"  class="image" alt="{{ $product->name }}">
                  </div>
                  
                  
                    
                                
              </div>


            
            </div>
            <div class="col-12 col-md-5 mt-5 mt-md-0 productRight">
              <h3>{{ strtoupper($product->name) }}</h3>
              
              <h2 class="product-price">{{ presentPrice($product->price) }}</h2>
              
            <p>{{ $product->description }}</p>
              <div class="productdtl" ng-controller="qtyCtrl">
              <div class="proQty">
                <h4>Quantity</h4>
                  
    

                  <div class="qtyCount">
                   <button class="minusBtn" ng-click="decrement()">-</button>
                      <span>@{{count}}</span>
                    <button class="plusBtn" ng-click="increment()">+</button>
                  </div>

              </div>
              <div class="buttnSize">
               
                
               
              </div>
              <form action="{{ route('cart.catering-store', $product) }}" method="POST">
                    {{ csrf_field() }}
                    <button type="submit" class="view_btn"><i class="fab fa-opencart"></i>Add to Cart</button>
                    <!-- <input type="hidden" class="varient-selected" name="varient" value="{{ (@$varientLast) ? $varientLast : ''  }}"> -->
                    <input type="hidden"  name="quantityCount" value="@{{count}}">
                </form>
                   <div class="blogDtlComment">
                  <div class="share-section">
        <a target="_blank" href="https://twitter.com/share?url={{ urlencode(url()->current())}}&via=TWITTER_HANDLE&text={{$product->name}}" class="btn-media twitter">
            <i class="fab fa-twitter" aria-hidden="true"></i>
          </a>
          <a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(url()->current()) }}" class="btn-media facebook">
            <i class="fab fa-facebook" aria-hidden="true"></i>
          </a>
          <a target="_blank" href="https://plus.google.com/share?url={{ urlencode(url()->current()) }}" class="btn-media google-plus">
            <i class="fab fa-google-plus" aria-hidden="true"></i>
          </a>
            </div>
                   </div>
            </div>

            </div>
          </div>
        </div>
      </div>
    </div>

    
   @include('partials.testimonials')

@php
      $footer=[
          'press' => true,
          'accolades' => true,
      ]; 
@endphp



 @include('partials.sub-footer',$footer)
 @include('partials.footer')		


@endsection

@section('extra-js-footer')


    <script type="text/javascript">
    
      var app = angular.module('myapp', ['simple-accordion']);


  app.controller('qtyCtrl', ['$scope', function($scope) {
    $scope.count = 1;
    $scope.increment = function() {
      $scope.count++;
    };
    $scope.decrement = function() {
      console.log($scope.count);
      if($scope.count <= 1){
        $scope.count = 1 ;
       }
      else{
        $scope.count--;
        }
    };
  }]);
  //   app.controller('likeCtrl', ['$scope','$http', function($scope,$http) {
  //   $scope.triggerLike = function($event){
  //                   // console.log($event.currentTarget);
  //                   var product_id = $event.currentTarget.getAttribute('data-id');
  //               $http({
  //                 method: 'post',
  //                 url:"{{ route('shop.wishlist') }}",
  //                 data: {product_id: product_id }
  //               }).then(function successCallback(response) {
  //                     $scope.status = response.data;
  //                     var myEl = angular.element( document.querySelector( '.classpost'+product_id ) );
  //                     if($scope.status.success=="liked"){
  //                       myEl.addClass('liked');
  //                     }else{
  //                       myEl.removeClass('liked');
  //                     }
  //               });
  //         }
  // }]);
  // var app = angular.module('myapp', []);

  app.controller('StarCtrl', ['$scope','$http', function ($scope,$http) {
    $scope.rating = 0;
    $scope.ratingsuccess = "";
    $scope.ratings = [
    //   {
    //     current: 5,
    //     max: 10
    // }
    // ,
     {
        current: 3,
        max: 5
    }];

    $scope.getSelectedRating = function (rating) {
        console.log(rating);
        $scope.ratingsuccess = "";
        $http({
                  method: 'post',
                  url:"{{ route('shop.rating') }}",
                  data: {
                    product_id : {{$product->id}},
                    rating : rating
                         }
                }).then(function successCallback(response) {
                      $scope.dataproduct = response.data;
                      console.log($scope.dataproduct);
                      $scope.ratingsuccess = "Rating updated Successfully"
                });
    }
}]);

app.directive('starRating', function () {
    return {
        restrict: 'A',
        template: '<ul class="rating">' +
            '<li ng-repeat="star in stars" ng-class="star" ng-click="toggle($index)">' +
            '\u2605' +
            '</li>' +
            '</ul>',
        scope: {
            ratingValue: '=',
            max: '=',
            onRatingSelected: '&'
        },
        link: function (scope, elem, attrs) {

            var updateStars = function () {
                scope.stars = [];
                for (var i = 0; i < scope.max; i++) {
                    scope.stars.push({
                        filled: i < scope.ratingValue
                    });
                }
            };

            scope.toggle = function (index) {
              
                scope.ratingValue = index + 1;
                scope.onRatingSelected({
                    rating: index + 1
                });
            };

            scope.$watch('ratingValue', function (oldVal, newVal) {
                if (newVal) {
                    updateStars();
                }
            });
        }
    }
});
  app.controller('TabController', ['$scope', function($scope) {
      $scope.tab = 1;

      $scope.setTab = function(newTab){
        $scope.tab = newTab;
      };

      $scope.isSet = function(tabNum){
        return $scope.tab === tabNum;
      };
  }]);

              app.controller('classCtrl', function ($scope) {
                $scope.isActive = false;
                $scope.activeButton = function() {
                  $scope.isActive = !$scope.isActive;
                }  
              });

              app.controller('MainCtrl', function($scope) {

              });

              angular.module('simple-accordion', [])
                  .directive('simpleAccordion', function($compile, $timeout) {
                      return {
                          restrict: 'AEC',
                          controller: function($scope) {
                              $scope.current = null;
                              $scope.height = [];
                              $scope.zero = {
                                  height: 0
                              };
                              $scope.toggle = function(i) {
                                  $scope.current = $scope.current === i ? null : i;
                              };
                          },
                          link: function(scope, el, attrs) {
                            var itemSelector = attrs.itemSelector || '.ftr_accordian , .burger_block',
                                titleSelector = attrs.titleSelector || '.ftr_head_mob, .burger_tab_mob',
                                contentSelector = attrs.contentSelector || '.ftr_link_mob, .burger_block_text';
                            $timeout(function(){
                              var items = el[0].querySelectorAll(itemSelector);
                              for (var i in items) {
                                if (angular.isObject(items[i])) {
                                  var title = items[i].querySelectorAll(titleSelector)[0];
                                  var content = items[i].querySelectorAll(contentSelector)[0];
                                  scope.height.push({
                                    'max-height': (content.offsetHeight + 10) + 'px'
                                  });
                                  angular.element(items[i]).addClass('item').attr({
                                    'ng-class': '{\'open\':current == ' + i + '}'
                                  });
                                  angular.element(title).addClass('title').attr('ng-click', 'toggle(' + i + ')');
                                  angular.element(content).addClass('content').attr({
                                    'ng-style': 'current == ' + i + '?height[' + i + ']:zero'
                                  });;

                                }
                              }
                              $compile(angular.element(el).contents())(scope);
                            });
                          }
                      }
                  });
            

            $('.burger_tab').on('click', function(){
              $('.burger_tab.active').removeClass('active');
              $(this).addClass('active');
            });

            $(document).ready(function() {
              var topArrow = $(".topArrow");
              $(topArrow).click(function() {
                $('html, body').animate({
                  scrollTop: 0
                }, 800);
                return false;
              }); 
            }); 
            

    </script>
@endsection