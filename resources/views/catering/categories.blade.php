@extends('layouts.master')

@section('banner-title',"CATERING")
@section('body-class',"subPages")
@section('content')

  @include('partials.inner-banner')
    <div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container">
          <h3 class="sub_head_mn">CATERING BOSTON BURGER COMPANY</h3>
					<div class="store">
					@forelse($categories as $category)
						<div class="row catering-content">
							<div class="col-lg-6 col-12 image-col">
							<div class="image-box">
								<img src="{{ Voyager::image($category->image)}}" alt="{{ $category->title}}"/>
							</div>
							</div>
							<div class="col-lg-6 col-12 text-col">
								<div class="text-box">
									<h3 class="sub_head_mn">{{ $category->title}}</h3>
									<p>{!! $category->description !!}</p>
									@if($category->slug=="corporate-catering")
									<a href="{{route('catering.category')}}" class="view_btn">Order Now</a>
									@else
									<a href="{{ route('contactUs')}}" class="view_btn">CONTACT US</a>
									@endif
								</div>
							</div>
						</div>
						@empty
							<div class="alert alert-danger">
								No Category Found
							</div>
						@endforelse
					</div><!-- store end-->
        </div>
				@php
          $footer=[
              'press' => true,
              'accolades' => true,
              'testimonial' => true,
              'about' => true,
          ]; 
		@endphp
		
    @include('partials.sub-footer',$footer)
      </div>  
    </div>

		
    @include('partials.footer')	



@endsection

@section('extra-js-footer')
   
@endsection