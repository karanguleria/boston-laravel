@extends('layouts.master')

@section('banner-title',"Catering")

@section('content')

  @include('partials.inner-banner')

    <div class="sub_pages our_story">
      <div class="sub_pages_sec">
        <div class="container">
          <h3 class="sub_head_mn">Dropoffs</h3>
			<div class="store catering-level-2">
				  <div class="row catering-content">
					  <div class="col-md-6 image-col">
						<div class="image-box">
							<img src="images/dropoffs1.png" alt="catering-image"/>
						</div>
					  </div>
						<div class="col-md-6 text-col">
							<div class="text-box">
								<h3 class="sub_head_mn">BIG PAPI BURGER</h3>
								<p>Lorem Ipsum is simply dummy text 
								of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since it to make a type specimen book.</p>
								<a href="#" class="view_btn">order Now</a>
							</div>
						</div>
				  </div><!-- content row-->
					<div class="row catering-content">
					  <div class="col-md-6 image-col">
						<div class="image-box">
							<img src="images/dropoffs2.png" alt="catering-image"/>
						</div>
					  </div>
						<div class="col-md-6 text-col">
							<div class="text-box">
								<h3 class="sub_head_mn">WARREN SAPP BURGER</h3>
								<p>Lorem Ipsum is simply dummy text 
								of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since it to make a type specimen book.</p>
								<a href="#" class="view_btn">order Now</a>
							</div>
						</div>
				  </div><!-- content row-->
					<div class="row catering-content">
					  <div class="col-md-6 image-col">
						<div class="image-box">
							<img src="images/dropoffs3.png" alt="catering-image"/>
						</div>
					  </div>
						<div class="col-md-6 text-col">
							<div class="text-box">
								<h3 class="sub_head_mn">CHRIS KILPATRICK BURGER</h3>
								<p>Lorem Ipsum is simply dummy text 
								of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since it to make a type specimen book.</p>
								<a href="#" class="view_btn">order Now</a>
							</div>
						</div>
				  </div><!-- content row-->
				  <a href="#" class="view_btn">more...</a>
				  <div class="row catering-order-row">
					<div class="col-md-4 order-row-col">
						<div class="order-box">
							<img src="images/catering-level-2-1.jpg" alt="order-image">
							<div class="text-box">
								<h3 class="sub_head_mn">ONSITE</h3>
								<a href="#" class="order-link">order now</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 order-row-col">
						<div class="order-box">
							<img src="images/catering-level-2-2.jpg" alt="order-image">
							<div class="text-box">
								<h3 class="sub_head_mn">CORPORATE</h3>
								<a href="#" class="order-link">order now</a>
							</div>
						</div>
					</div>
					<div class="col-md-4 order-row-col">
						<div class="order-box">
							<img src="images/catering-level-2-3.jpg" alt="order-image">
							<div class="text-box">
								<h3 class="sub_head_mn">SOCIAL</h3>
								<a href="#" class="order-link">order now</a>
							</div>
						</div>
					</div>
				  </div>
				</div><!-- store end-->
        </div>
				@include('partials.sub-footer')
      </div>  
    </div>


		@include('partials.footer')	




@endsection

@section('extra-js-footer')
    
@endsection