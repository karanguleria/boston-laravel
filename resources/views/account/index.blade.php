@extends('layouts.master')

@section('banner-title',"My Account")

@section('body-class','blog-main')

@section('content')

@include('partials.product-banner')

 <div class="product-lisening-main innerPagesBg">
      <div class="container">
        <div class="productCart">
          <div class="contiShop">
            <a href="#">
              Logout <i class="fas fa-power-off"></i>
            </a>
          </div>

          <div class="accountDetail" ng-controller="MainCtrl">
            <div class="checkoutLeft" simple-accordion>
              <div class="AccountDetalMain">
                <div class="accountClick">ACCOUNT DETAILS</div>
                <div class="accountText">
                    <div class="checkout_Info">
                      <div class="row">
                        <div class="col-12 col-md-6 accountLeft">
                          <h3>MY DETAILS</h3>
                            <form action="{{ route('myaccount.update') }}" method="POST">
                            @csrf
					                  @method("PATCH")
                              <div class="row">
                                <div class="col-12 col-lg-6">
                                  <input type="text" name ="name" value="{{ Auth::user()->name }}" placeholder="John" required>
                                </div>
                                <div class="col-12 col-lg-6">
                                  <input type="text" name ="last_name" value="{{ Auth::user()->last_name }}" placeholder="Deo" required>
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-12">
                                  <input type="email"name ="email" value="{{ Auth::user()->email }}"  placeholder="Company.johndoe26@gmail.com" required>
                                </div>
                                <div class="col-12">
                                  <button type="submit"  class="view_btn">Save Changes</button>
                                </div>
                              </div>
                          </form> 
                        </div>
                        <div class="col-12 col-md-6 accountRight">
                          <h3>PASSWORD CHANGE</h3>
                          
                            <form action="{{route('myaccount.updatePassword')}}" method="POST">
                            @method('PATCH')
					                  @csrf
                              <div class="row">
                                <div class="col-12">
                                  <input type="password" name="password" placeholder="Current password (leave blank to leave unchanged)">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-12 col-lg-6">
                                  <input type="password" name="password_new" placeholder="New password">
                                </div>
                                <div class="col-12 col-lg-6">
                                  <input type="password" name="password_retype" placeholder="Confirm new password">
                                </div>
                              </div>
                              <div class="row">
                                <div class="col-12">
                                  <button type="submit" class="view_btn">Save Changes</button>
                                </div>
                              </div>
                          </form>                       
                        </div>
                      </div> 
                    </div>  
                </div>
              </div>   


              <div class="AccountDetalMain">
                <div class="accountClick">DASHBOARD</div>
                <div class="accountText">
                  <div class="checkout_Info">                    
                    <h4>Enter Text</h4>
                  </div>
                </div>
              </div> 

              <div class="AccountDetalMain">
                <div class="accountClick">WISHLIST</div>
                <div class="accountText">
                  <div class="checkout_Info">                    
                    @forelse($wishlists as $wishlist)
                      
                      <div class="col-12 col-md-8 orderLft">
                        <div class="orderImg">
                        <a href="{{ route('shop.show',$wishlist->parent->slug)}}">
                          <img style="height:110px" src="{{ productImage($wishlist->parent->image) }}" alt="product" class="active" id="currentImage"></a>
                        </div>
                        <div class="orderText">
                        <a href="{{ route('shop.show',$wishlist->parent->slug)}}"><h3>{{ strtoupper($wishlist->parent->name) }}</h3></a>
                          <p>Price : {{ presentPrice($wishlist->parent->price) }}</p>
                        </div>
                      </div>
                    @empty
                      <div class="alert alert-danger">
                        No product in wishlist
                      </div>
                    @endforelse
                  </div>
                </div>
              </div> 

              <div class="AccountDetalMain">
                <div class="accountClick">ORDER</div>
                <div class="accountText">
                  <div class="checkout_Info">                    
                  <div class="row">
                    @foreach ($orders as $order)
                    <div class="col-12 orderAcc">
                      <div class="row orderIdDtl">
                        <div class="col-6">Order ID : <b>{{ $order->id }}</b> <br> Order Placed : <b>{{ presentDate($order->created_at) }}</b></div>
                        <div class="col-6 text-right"><a href="{{ route('orders.show', $order->id) }}">Order Details</a></div>                      
                      </div>    
                        <div class="orderSec">
                          <div class="row"> 
                          @foreach ($order->products as $product)
                          <div class="col-12 col-md-8 orderLft">
                            <div class="orderImg"><img style="height:110px" src="{{ productImage($product->image) }}" alt="product" class="active" id="currentImage"></div>
                            <div class="orderText">
                              <h3>{{ strtoupper($product->name) }}</h3>
                              @if(@$product->pivot->variant)
                              <p>Varient : {{ $product->pivot->variant }}</p>
                              @endif
                              <p>Quantity : {{ $product->pivot->quantity }}</p>
                              <p>Price : {{ presentPrice($product->pivot->price) }}</p>
                            </div>
                          </div>
                           @endforeach
                          <div class="col-12 col-md-4 orderRgt">
                            <b>Total Price :</b> {{ presentPrice($order->billing_total) }}
                          </div>
                        </div>
                        </div>  
                    </div>
                                                     

                          <!-- end order-container -->
                                @endforeach
			{{ $orders->links() }}
		</div>
                  </div>
                </div>
              </div> 

              <div class="AccountDetalMain">
                <div class="accountClick">DOWNLOAD</div>
                <div class="accountText">
                  <div class="checkout_Info">                    
                    <h4>Enter Text</h4>
                  </div>
                </div>
              </div> 


              <div class="AccountDetalMain">
                <div class="accountClick">ADDRESS</div>
                <div class="accountText">
                  <div class="checkout_Info">                    
                    <h4>Enter Text</h4>
                  </div>
                </div>
              </div> 


            </div>
          </div>
        
        
         </div>
      </div>
    </div> @php
          $footer=[

              'press' => false,
              'accolades' => false,
              'giveback' => false,
              'social_media' => false,
              'about' => false,
              'catering' => false,
          ]; 
    @endphp



    @include('partials.sub-footer',$footer)	
 @include('partials.footer')	


@endsection


@section('extra-js-footer')
@endsection('extra-js-footer')