@extends('layouts.master')
@section('banner-title',"Order Detail")
@section('body-class','blog-main')
@section('content')
 @include('partials.product-banner')
@php
@endphp
<div class="product-lisening-main innerPagesBg">
<div class="products-section my-orders container">
        <div class="sidebar">
        </div> <!-- end sidebar -->
        <div class="my-profile">
           <!--  <div class="products-header">
                <h1 class="stylish-heading">Order ID: {{ $order->id }}</h1>
            </div> -->
            <div>
                <div class="order-container">
                    <div class="order-header">
                        <div class="order-header-items">
                            <div>
                                <div class="uppercase font-bold"></div>
                                <div><span class="orderDtlHd">Order Placed :</span> {{ presentDate($order->created_at) }}</div>
                            </div>
                            <div>
                                <div class="uppercase font-bold"></div>
                                <div><span class="orderDtlHd">Order ID :</span> {{ $order->id }}</div>
                            </div><div>
                                <div class="uppercase font-bold"></div>
                                <div><span class="orderDtlHd">Total :</span> {{ presentPrice($order->billing_total) }}</div>
                            </div>
                        </div>
                        <!-- <div>
                            <div class="order-header-items">
                                <div><a href="#">Invoice</a></div>
                            </div>
                        </div> -->
                    </div>
                    <div class="order-products">
                        <div class="orderAddres">
                        <table class="table" style="width:50%">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>{{ $order->user->name }}</td>
                                </tr>
                                <tr>
                                    <td>Address</td>
                                    <td>{{ $order->billing_address }}</td>
                                </tr>
                                <tr>
                                    <td>City</td>
                                    <td>{{ $order->billing_city }}</td>
                                </tr>
                                <tr>
                                    <td>Subtotal</td>
                                    <td>{{ presentPrice($order->billing_subtotal) }}</td>
                                </tr>
                                <tr>
                                    <td>Tax</td>
                                    <td>{{ presentPrice($order->billing_tax) }}</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>{{ presentPrice($order->billing_total) }}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                        <div class="orderHedPg">
                        <div class="orderHed">
                                Order Items
                            </div>
                        <div class="orderSecInner">
                          <div class="row"> 
                        @foreach ($products as $product)
                          <div class="col-12 col-md-12 orderLft">
                            <div class="orderImg"><img style="height:110px" src="{{ productImage($product->image) }}" alt="Product Image"></div>
                            <div class="orderText">
                              <h3><a href="{{ route('shop.show', $product->slug) }}">{{ strtoupper($product->name) }}</a></h3>
                               @if(@$product->pivot->variant)
                              <p>Varient : {{ $product->pivot->variant }}</p>
                              @endif
                              <p>Quantity : {{ $product->pivot->quantity }}</p>
                              <p>Price : {{ presentPrice($product->pivot->price) }}</p>
                            </div>
                          </div>
                           @endforeach
                        </div>
                        </div> 
                        </div> 
                    </div>
                </div> <!-- end order-container -->
               
            </div>
            <div class="spacer"></div>
        </div>
    </div>
</div> @php
          $footer=[

              'press' => false,
              'accolades' => false,
              'giveback' => false,
              'social_media' => false,
              'about' => false,
              'catering' => false,
          ]; 
    @endphp



 @include('partials.sub-footer',$footer)	
 @include('partials.footer')		
@endsection
