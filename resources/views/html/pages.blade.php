@extends('layouts.master')

@section('content')
<div class="jumbotron">
  <h1 class="text-center display-4">All Pages</h1>
</div>
		<div class="section">
			<div class="container">
				<div class="row">
				<div class="table-responsive">
					<table class="table">
						<thead class="thead-dark">
							<tr>
								<th scope="col">#</th>
								<th scope="col">Name</th>
								<th scope="col">HTML</th>
								<th scope="col">Development</th>
							</tr>
						</thead>
						<tbody>
						<tr>
								<th scope="row">1</th>
								<td>Home</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/index.html">Done</a></td>
								<td><a target="_blank" href="{{ route('landing-page')}}">Done</a></td>
							</tr>
							<tr>
								<th scope="row">2</th>
								<td>About Us</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/about.html">Done</a></td>
								<td><a target="_blank" href="{{ route('about')}}">Done</a></td>
							</tr>
							<tr>
								<th scope="row">3</th>
								<td>Menu</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/menu.html">Done</a></td>
								<td><a target="_blank" href="{{ route('menu')}}">Done</a></td>
							</tr>
							<tr>
								<th scope="row">4</th>
								<td>Store</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/store.html">Done</a></td>
								<td><a target="_blank" href="{{ route('store.index')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">12</th>
								<td>Gallary</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/gallery.html">Done </a></td>
								<td><a target="_blank" href="{{ route('blog.gallery')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">12</th>
								<td>Videos</td>
								<td><a target="_blank" href=""> </a></td>
								<td><a target="_blank" href="{{ route('blog.videos')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">12</th>
								<td>Photos</td>
								<td><a target="_blank" href=""></a></td>
								<td><a target="_blank" href="{{ route('blog.photos')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">12</th>
								<td>Blog</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/blog.html">Done </a></td>
								<td><a target="_blank" href="{{ route('blog.index')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">12</th>
								<td>Blog Detail</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/blog-detail.html">Done </a></td>
								<td><a target="_blank" href="{{ route('blog.show','10-tips-for-making-the-perfect-burger')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">5</th>
								<td>Catering</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/catering-final.html">Done</a></td>
								<td><a target="_blank" href="{{ route('catering.index')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">6</th>
								<td>Catering Level 2</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/catering-level2.html">Done</a></td>
								<td><a target="_blank" href="{{ route('catering.collection')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">7</th>
								<td>Location</td>
								<td><a target="_blank" href=""> </a></td>
								<td><a target="_blank" href="{{ route('location')}}">Pending</a></td>
							</tr>	
							<tr>
								<th scope="row">8</th>
								<td>Order</td>
								<td><a target="_blank" href=""> </a></td>
								<td><a target="_blank" href="{{ route('shop.index')}}">Pending</a></td>
							</tr>
								<tr>
								<th scope="row">9</th>
								<td>Giftcard</td>
								<td><a target="_blank" href=""> </a></td>
								<td><a target="_blank" href="{{ route('giftcard')}}">Pending</a></td>
							</tr>
							<tr>
								<th scope="row">10</th>
								<td>Giveback</td>
								<td><a target="_blank" href=""> </a></td>
								<td><a target="_blank" href="{{ route('giveback')}}">Pending</a></td>
							</tr>	<tr>
								<th scope="row">11</th>
								<td>Product listing</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/product-listening.html">Done </a></td>
								<td><a target="_blank" href="{{ route('shop.index')}}">working</a></td>
							</tr>
								<tr>
								<th scope="row">12</th>
								<td>product Detail</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/product-detail.html"> Done </a></td>
								<td><a target="_blank" href="shop/test">working</a></td>
							</tr>	
							<tr>
							<th scope="row">13</th>
								<td>Cart</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/cart.html"> Done</a></td>
								<td><a target="_blank" href="{{ route('cart.index')}}">working</a></td>
							</tr>
							<tr>
							<th scope="row">14</th>
								<td>Checkout</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/checkout.html"> Done</a></td>
								<td><a target="_blank" href="{{ route('checkout.index')}}">working</a></td>
							</tr>
							<tr>
							<th scope="row">15</th>
								<td>Thankyou</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/thankyou.html"> Done</a></td>
								<td><a target="_blank" href="">Pending</a></td>
							</tr>	
							<tr>
							<th scope="row">16</th>
								<td>Myaccount</td>
								<td><a target="_blank" href="http://development.dexterousteam.com/boston-burger/my-account.html"> Done</a></td>
								<td><a target="_blank" href="{{ route('myaccount.index')}}">Pending</a></td>
							</tr>	
							
										
							
						</tbody>
					</table>

				</div>
			</div>
		</div>
		<br>
<br>
<br><br>
<br>
<br><br>
<br>

		@php
          $footer=[
              'press' => false,
              'accolades' => false,
              'giveback' => false,
              'social_media' => false,
              'about' => false,
              'catering' => false,
          ]; 
    @endphp

    @include('partials.sub-footer',$footer)	
	@include('partials.footer')	
		@endsection
 
@section('extra-js-footer')
    
@endsection

