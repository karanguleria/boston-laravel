@component('mail::message')
# Become a Retail Partner Enquiry recieved at Boston Burger

{{ $firstname}} has submitted a form to ask for an enquiry.

@component('mail::table')

|        |            |
| ------------- |-------------:|
| First Name     | {{ $firstname }}    | 
| Last Name     | {{ $lastname }}    | 
| Email      | {{ $email }}    |
| Phone     | {{ $phone }}    | 
| Description  | {{ $description}}|
@endcomponent

    

Thanks,<br>
{{ config('app.name') }}
@endcomponent
