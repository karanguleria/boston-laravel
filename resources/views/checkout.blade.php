@extends('layouts.master')

<!-- @section('banner-title',"Checkout") -->

@section('body-class','blog-main')

@section('content')

<!-- @include('partials.product-banner') -->
    <div class="product-lisening-main innerPagesBg" ng-controller="">
      <div class="container">
        <div class="productCart">
        @if (Session::has('message'))
     <div class="alert alert-{{ Session::get('code') }}">
      <p>{{ Session::get('message') }}</p>
     </div>
    @endif

    
   @include('partials.success-error')
   <h2 class="head_mn text-center">Shop Checkout</h1>
          <div class="contiShop">
            <a href="{{ route('cart.index')}}">
              <i class="fas fa-long-arrow-alt-left"></i> &nbsp; Back to Cart
            </a>
          </div>

          <div class="CheckoutMain">
            <div class="row">
              <div class="col-12 col-md-6 checkoutLeft">
                <div class="checkout_account">
                  <h3>YOUR ACCOUNT</h3>
                  <p><b>{{ auth()->user()->name }}</b></p>
                  <p>{{ auth()->user()->email }}</p>
                </div>




                <div class="productTable mb-5 d-md-none">
                  <table class="table table-bordered text-center">
                    <thead>
                      <tr>
                        <th scope="col" class="text-left">PRODUCTS</th>
                        <th scope="col">PRICE</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach (Cart::instance('default')->content() as $item)
                      <tr>
                        <td class="text-left align-middle">
                          <a href="#">
                            <div class="cart_sec">
                              <div class="cartImg">
                              <a href="{{ route('shop.show', $item->model->slug) }}">
                                  <img style="height:100px" src="{{ productImage($item->model->image) }}">
                              </a>
                              </div>
                              <div class="cartText">
                              <h4>{{ strtoupper(str_limit($item->model->name, 30, '...')) }}</h4>
                                <p>Quantity = {{ $item->qty }}</p>
                                @if(@$item->options->variant)
                                <p>Size = {{ $item->options->variant }}</p>
                                @endif
                              </div>
                            </div>
                          </a>  
                        </td>
                        <td class="align-middle">{{ presentPrice($item->subtotal) }}</td>
                      </tr>
                      @endforeach
                      <tr>
                        <td class="check_sub">
                          <span>Subtotal</span>
                        </td>
                        <td>
                          <span>{{ presentPrice(Cart::instance('default')->subtotal()) }}</span>
                        </td>
                      </tr>
                      @if (session()->has('coupon'))
                        <tr>
                            <td class="check_sub">
                             <span> Code ({{ session()->get('coupon')['name'] }}) </span>
                            </td>
                            <td> 
                                <span>
                                    -{{ presentPrice($discount) }}
                                </span>
                            </td>
                        </tr>
                        <tr>
                        <td class="check_sub">
                            <span>New Subtotal</span>
                        </td>
                        <td> 
                            <span>{{ presentPrice($newSubtotal) }}</span>
                        </td>
                        </tr>
                        @endif
                        <tr>
                        <td class="check_sub">
                            <span>Tax ({{config('cart.tax')}}%)</span>
                        </td>
                        <td> 
                            <span>{{ presentPrice($newTax) }}</span>
                        </td>
                        </tr>
                      <tr>
                        <td class="check_sub">
                          Total
                        </td>
                        <td>
                        {{ presentPrice($newTotal) }}
                        </td>
                      </tr>
                      
                      
                    </tbody>
                  </table>
                  <!-- <div class="apply_coupn">
                    <input type="text" placeholder="Coupon Code">
                    <button type="submit">Apply Coupon</button>
                  </div> -->
                </div>




                <div class="checkout_Info">
                  <h3>SHIPPING INFO</h3>
                  <form action="{{ route('checkout.paypal') }}" method`="POST" id="payment-form">
                  {{ csrf_field() }}
                    <div class="row">
                      <div class="col-12 col-lg-6">
                        <input type="text" placeholder="First Name" class="form-control" id="name" name="name" value="{{ old('name') }}" >
                      </div>
                      <div class="col-12 col-lg-6">
                        <input type="text" placeholder="Last Name" class="form-control" id="lname" name="lname" value="{{ old('lname') }}" >
                   
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                      <input type="email" placeholder="Email Address" class="form-control" id="email" name="email" value="{{ old('email') }}">
                      </div>
                    </div>     
                    <div class="row">
                      <div class="col-12">
                        <input type="text" placeholder="Address" class="form-control" id="address" name="address" value="{{ old('address') }}" >
                      </div>
                      </div>     
                    <div class="row">
                      <div class="col-12 col-lg-6">
                        <input type="text" placeholder="Town / City" class="form-control" id="city" name="city" value="{{ old('city') }}" >
                      </div>
                      <div class="col-12 col-lg-6">
                      <input type="text" placeholder="State" class="form-control" id="province" name="province" value="{{ old('province') }}" >
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <div class="checkCountry">
                          <input type="text" placeholder="Country" class="form-control" id="country" name="country" value="{{ old('country') }}" >
                      
                        </div>
                      </div>
                    </div>                     
                    <div class="row">
                      <div class="col-12 col-lg-6">
                        <input type="text" placeholder="Zip/Postal code" class="form-control" id="postalcode" name="postalcode" value="{{ old('postalcode') }}" >
                      </div>
                      <div class="col-12 col-lg-6">
                      <input type="text" placeholder="Phone Number" class="form-control" id="phone" name="phone" value="{{ old('phone') }}" >
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-12">
                        <button class="view_btn" type="submit">Continue to payment</button>
                      </div>
                    </div>
                  </form>
                </div>

                <div class="checkout_account payAccount">
                  <h3>PAYMENT METHOD</h3>
                  <div class="paymentCheck">
                    <img src="{{ asset('images/payment.png') }}">
                  </div>
                </div>
              </div>
              <div class="col-12 col-md-6 d-none d-md-block checkoutRight">

                <div class="productTable">
                  <table class="table table-bordered text-center">
                    <thead>
                      <tr>
                        <th scope="col" class="text-left">PRODUCTS</th>
                        <th scope="col">PRICE</th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach (Cart::instance('default')->content() as $item)
                        <tr>
                        <td class="text-left align-middle">
                          <a href="{{ route('shop.show', $item->model->slug) }}">
                            <div class="cart_sec">
                              <div class="cartImg">
                                <img style="height:100px" src="{{ productImage($item->model->image) }}">
                              </div>
                              <div class="cartText">
                                <h4>{{ strtoupper(str_limit($item->model->name, 20, '...')) }}</h4>
                                 @if(@$item->options->variant)
                                <p>Size : {{ $item->options->variant }}</p>
                                @endif
                                <p>Quantity : {{ $item->qty }}</p>
                                <p>Price : {{ presentPrice($item->price) }}</p>
                              </div>
                            </div>
                          </a>  
                        </td>
                        <td class="align-middle">{{ presentPrice($item->subtotal) }}</td>
                      </tr>
                      @endforeach 
                      <tr>
                        <td class="check_sub">
                          <span>Subtotal</span>
                        </td>
                        <td>
                          <span>{{ presentPrice(Cart::instance('default')->subtotal()) }}</span>
                        </td>
                      </tr>
                      @if (session()->has('coupon'))
                        <tr>
                            <td class="check_sub">
                             <span> Code ({{ session()->get('coupon')['name'] }}) </span>
                            </td>
                            <td> 
                                <span>
                                    -{{ presentPrice($discount) }}
                                </span>
                            </td>
                        </tr>
                        <tr>
                        <td class="check_sub">
                            <span>New Subtotal</span>
                        </td>
                        <td> 
                            <span>{{ presentPrice($newSubtotal) }}</span>
                        </td>
                        </tr>
                        @endif
                        <tr>
                        <td class="check_sub">
                            <span>Tax ({{config('cart.tax')}}%)</span>
                        </td>
                        <td> 
                            <span>{{ presentPrice($newTax) }}</span>
                        </td>
                        </tr>
                      <tr>
                        <td class="check_sub">
                          Total
                        </td>
                        <td>
                        {{ presentPrice($newTotal) }}
                        </td>
                      </tr>
                      
                      
                    </tbody>
                  </table>
                  <!-- <div class="apply_coupn">
                    <input type="text" placeholder="Coupon Code">
                    <button type="submit">Apply Coupon</button>
                  </div> -->
                </div>
                

              </div>
            </div>
          </div>
        
        
      </div>
      </div>
    </div>
@php
          $footer=[
            
              'press' => false,
              'accolades' => false,
              'giveback' => false,
              'social_media' => false,
              'about' => false,
              'catering' => false,
          ]; 
    @endphp



 @include('partials.sub-footer',$footer)	
 @include('partials.footer')	



@endsection