<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="{{ asset('fonts/stylesheet.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/owl.carousel.min.css')}}">
    
        @yield('extra-css')
            <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css')}}">
        @yield('extra-js-header')
        <title>@yield('title')</title>
    </head>
    <body ng-app="myapp" class="{{ Request::path() == '/' ? 'homepage ':'' }}  @yield('body-class') ">
    
    @include('partials.nav')
    
    @yield('content')
    
   
  @if (\Route::current()->getName() != 'checkout.index' && \Route::current()->getName() != 'checkout.restaurant-index') 
    @include('partials.subscribe')
  @endif
    <script type="text/javascript" src="{{ asset('js/jquery.min.js')}}"></script> 
    <script type="text/javascript" src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.7.5/angular.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.7.5/angular-sanitize.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script type="text/javascript">  
    function loaderr(){
      $('body > div').each( function(i){
                  
                  var bottom_of_object = $(this).position().top + 100;
                  var bottom_of_window = $(window).scrollTop() + $(window).height();
                  
      //            / If the object is completely visible in the window, fade it it /
                  if( bottom_of_window < bottom_of_object ){
                      
                      $(this).addClass("hideme");
                          
                  }
                  
              }); 
    }    
       $(function(){  // $(document).ready shorthand
  $('.monster').fadeIn('slow');
});

$(document).ready(function() {
    loaderr();
//    / Every time the window is scrolled ... /
    $(window).scroll( function(){
    
//        / Check the location of each desired element /
        $('.hideme').each( function(i){
            
            var bottom_of_object = $(this).position().top + 100;
            var bottom_of_window = $(window).scrollTop() + $(window).height();
            
//            / If the object is completely visible in the window, fade it it /
            if( bottom_of_window > bottom_of_object ){
                
                $(this).animate({'opacity':'1'},1000);
                    
            }
            
        }); 
    
    });
    
});
$(window).on('load',function() { loaderr()});


          $(document).ready(function() {
            // Setting manu toggel
            $(".navButton").click(function(){
              $(".navigation").slideToggle();
            });

            // $( '<span class="triggerClass"></span>' ).before( ".navigation ul li a" );
            $(".navigation").children("ul").children("li").prepend('<span class="triggerClass"></span>');
            // Adding class to the li
            $(".triggerClass").click(function(){
                $('li').removeClass('liClass');
              $(this).parent('li').toggleClass("liClass");
            });
            // $("#subscribeForm").submit(function(e){
            //     e.preventDefault();
            //     $(".subscribeSec").hide();
            //   $(".thanksSec").show();
            // });
              // Testimonial slider
              var owl = $('#owlTestmonial');
              owl.owlCarousel({
                margin: 10,
                nav: true,
                loop: true,
                responsive: {
                  0: {
                    items: 1
                  },
                  600: {
                    items: 2
                  },
                  1000: {
                    items: 3
                  }
                }
              });
               // Shop Overview sliider
             var owl = $('.shop-slide');
            owl.owlCarousel(
            {
                margin: 2,
                nav: true,
                loop: true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                responsive: {
                    0: {
                    items: 1
                    },
                    576: {
                    items: 2
                    },
                    768: {
                    items: 3
                    },
                    1000: {
                    items: 5
                    }
                }
            });
            // ACCOLADES sliider
            var owl = $('.accla-slide');
            owl.owlCarousel(
            {
                margin: 2,
                nav: true,
                loop: true,
                responsive: {
                    0: {
                    items: 1
                    },
                    576: {
                    items: 2
                    },
                    768: {
                    items: 3
                    },
                    1000: {
                    items: 5
                    }
                }
            });
            $('body').on('click','a.disabledAnchor',function(e) {
                e.preventDefault()
            });
            var topArrow = $(".topArrow");
            $(topArrow).click(function() {
              $('html, body').animate({
                scrollTop: 0
              }, 800);
              return false;
            }); 
          }); 
          </script>
    <script>
        
        var app = angular.module("myapp", ['simple-accordion']);
      
        app.controller('classCtrl', function ($scope) {
          $scope.isActive = false;
          $scope.activeButton = function() {
            $scope.isActive = !$scope.isActive;
          }  
        });

        app.controller('MainCtrl', function($scope) {

         });

        angular.module('simple-accordion', [])
          .directive('simpleAccordion', function($compile, $timeout) {
              return {
                  restrict: 'AEC',
                  controller: function($scope) {
                      $scope.current = null;
                      $scope.height = [];
                      $scope.zero = {
                          height: 0
                      };
                      $scope.toggle = function(i) {
                          $scope.current = $scope.current === i ? null : i;
                      };
                  },
                  link: function(scope, el, attrs) {
                    var itemSelector = attrs.itemSelector || '.ftr_accordian , .burger_block, .AccountDetalMain, .faq_accordian',
                                titleSelector = attrs.titleSelector || '.ftr_head_mob, .burger_tab_mob, .accountClick, .faq_head',
                                contentSelector = attrs.contentSelector || '.ftr_link_mob, .burger_block_text, .accountText, .faq_text';
                            
                    $timeout(function(){
                      var items = el[0].querySelectorAll(itemSelector);
                      for (var i in items) {
                        if (angular.isObject(items[i])) {
                          var title = items[i].querySelectorAll(titleSelector)[0];
                          var content = items[i].querySelectorAll(contentSelector)[0];
                          scope.height.push({
                            'max-height': (content.offsetHeight + 10) + 'px'
                          });
                          angular.element(items[i]).addClass('item').attr({
                            'ng-class': '{\'open\':current == ' + i + '}'
                          });
                          angular.element(title).addClass('title').attr('ng-click', 'toggle(' + i + ')');
                          angular.element(content).addClass('content').attr({
                            'ng-style': 'current == ' + i + '?height[' + i + ']:zero'
                          });
                        }
                      }
                      $compile(angular.element(el).contents())(scope);
                    });
                  }
              }
          });
         
    </script>
    @yield('extra-js-footer')
    
    </body>
</html>