<div class="col Gallery_sidebar">
            <div class="selectViewMn  d-none d-md-block">
              <h3>SELECT VIEW</h3>
              <div class="selectView">
                <div class="viewClick">
                  <ul>
                    <li>
                      <span ng-click="gridClick()">
                        <i class="fas fa-th"></i>
                      </span>
                    </li>
                    <li>
                      <span ng-click="listClick()">
                        <i class="fas fa-list-ul"></i>
                      </span>
                    </li>
                  </ul>
                </div>
                <div class="sort">
                  <i class="fas fa-chevron-down"></i>
                  <select ng-model="sortBy">
                    <option value="+name">Name Asc</option>
                    <option value="-name">Name Desc</option>
                    <option value="+price">Price Low to High</option>
                    <option value="-price">Price High to Low</option>
                    <option value="-rating">Rating High to Low</option>
                    <option value="+rating">Rating Low to High</option>
                  </select>
                </div>
              </div>
            </div>
            <div class="priceRange  d-none d-md-block">
              <h3>FILTER BY PRICE</h3>
               <div class="wrapper">
                  <article>
                    <rzslider  rz-slider-model="minRangeSlider.minValue" rz-slider-high="minRangeSlider.maxValue" rz-slider-options="minRangeSlider.options"></rzslider>
                    <div class="rangeMain">
                      <div class="price-range">PRICE: 
                        <span class="priceRg">$@{{minRangeSlider.minValue}}</span>
                        <span  class="priceLn"></span>
                        <span class="priceRg">$@{{minRangeSlider.maxValue}}</span>
                      </div>
                      <!-- <div class="filterButton">
                        <button class="view_btn">Filter</button>
                      </div> -->
                    </div>  
                  </article>
                </div>
            </div>
            <div class="topProduct">              
              <h3>TOP RATED PRODUCTS</h3>
              <div class="top-Rate">
                @php  $products = DB::select('SELECT * from (SELECT `product_id`,avg(`rating`) as rating FROM `ratings` WHERE `status`="Approved" group by `product_id`) as abc order by abc.rating DESC LIMIT 5 ');  @endphp
                 @forelse($products as $product)
                 @php $product = App\Product::find($product->product_id);@endphp
                 @if($product)
                 <div class="rateSec">
                  <div class="TopRImg">
                  <a href="{{ route('shop.show',$product->slug)}}"><img src="{{ productImage($product->image) }}"></a>
                  </div>
                  <div class="TopRText">
                  <a href="{{ route('shop.show',$product->slug)}}"><h4>{{ strtoupper($product->name)}}</h4></a>
                    <h5>{{ presentPrice($product->price)}}</h5>
                  </div>
                </div>
                     @endif
                @empty
                <div class="alert alert-danger">No Top Rated Products</div>
                 @endforelse
              </div>
            </div>

            <div class="categotySidebar">      
              <h3>CATEGORIES</h3>
              @php  $categories = App\Category::all(); @endphp
              <ul>
                @foreach($categories as $category)
                <li class="">
                  <a href="#" class="disabledAnchor"  ng-click="setCatname('{{$category->slug}}')" value="{{$category->slug}}">{{ ucwords($category->name) }}</a>
                </li>
                @endforeach
              </ul>
              
            </div>
             
          </div>