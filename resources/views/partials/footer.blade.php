<!-- setting default value for the footer array -->
<div class="footer_div">
   
    <footer>
        <div class="container">
            
                @if (\Route::current()->getName() == 'cart.index' 
                || \Route::current()->getName() == 'myaccount.index'
                || \Route::current()->getName() == 'shop.index'
                ) <div class="footer_up with-gurantee">
                    <div class="row align-items-center justify-content-between ">
                        <div class="col-auto footer_logo">
                                <a href="{{ route('landing-page') }}">
                                    <img alt="{{ setting('site.title') }}" src="{{ url('storage/'.setting('site.logo')) }}">
                                </a>
                        </div>
                        <div class="col-auto gurantee-img">
                                <img alt="altText" src="{{ asset('images/guarantee.png')}}">              
                        </div>
                    </div></div>
                @else<div class="footer_up">
                <div class="row align-items-center justify-content-between">
                        <div class="col-auto footer_logo">
                            <a href="{{ route('landing-page') }}">
                                <img alt="{{ setting('site.title') }}" src="{{ url('storage/'.setting('site.logo')) }}">
                            </a>
                        </div>
                    </div>
                </div>
                @endif
                   
            
            <div class="footer_links d-none d-md-block">
                <div class="row">
                    <div class="col-3 footer_explore">
                        <div class="ftr_url">
                            <div class="footer_ex_in">
                                <h4>NAVIGATION</h4>
                                {{ menu('Footer Navigation') }}
                            </div>
                        </div>  
                    </div>
                    <div class="col-3 footer_explore">
                        <div class="ftr_url">
                            <div class="footer_ex_in">
                                <h4>SHOP</h4>
                                {{ menu('Footer Shop') }}
                            </div>
                        </div>  
                    </div>
                    <div class="col-3 footer_explore">
                        <div class="ftr_url">
                            <div class="footer_ex_in">
                                <h4>SUPPORT</h4>
                                {{ menu('Footer Support') }}
                            </div>
                        </div>  
                    </div>
                    <div class="col-3 footer_explore">
                        <div class="ftr_url">
                            <div class="footer_ex_in">
                                <h4>SOCIAL</h4>
                                {{ menu('Footer Social') }}
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
            <div class="footer_links d-md-none">          
                <div  ng-controller="MainCtrl">
                    <div class="footer_Acc" simple-accordion>
                        <div class="ftr_link_mob ftr_accordian">
                            <h4 class="ftr_head_mob">NAVIGATION</h4>
                            {{ menu('Footer Navigation', 'partials.menu-footer') }}
                        </div>
                        <div class="ftr_link_mob ftr_accordian">
                            <h4 class="ftr_head_mob">SHOP</h4>
                            {{ menu('Footer Shop', 'partials.menu-footer')  }}
                        </div>
                        <div class="ftr_link_mob ftr_accordian">
                            <h4 class="ftr_head_mob">SUPPORT</h4>
                            {{ menu('Footer Support', 'partials.menu-footer')  }}
                        </div>   
                        <div class="ftr_link_mob ftr_accordian">
                            <h4 class="ftr_head_mob">SOCIAL</h4>
                            {{ menu('Footer Social', 'partials.menu-footer')  }}
                        </div>              
                    </div>
                </div>
            </div>
            <div class="copy-right">
                <p><span>{{ setting('site.title') }}</span> &copy; {{ setting('site.copyright_text') }}</p>
                <div class="topArrow">
                    <span>Go to top</span><i class="fas fa-arrow-up"></i>
                </div>
            </div>
        </div>
    </footer>
</div>
