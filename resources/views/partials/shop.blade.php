@php 
// Getting 10 Products Items
$products = App\Product::where('featured','1')->take(5)->orderBy('id')->get();
@endphp
<div class="includge_sec shop_sec">
    <div class="container_outer">
        <div class="head_sec">
            <h2 class="head_mn">Shop</h2>
        </div>
        <div class="slider">
            <div class="owl-carousel shop-slide owl-theme">
                @forelse($products as $product)
                <div class="item">
                    <a href="{{route('shop.show',$product->slug)}}" class="disabledAnchor">
                       <div class="shop_hmImg"> <img  alt="{{ $product->name }}" src="{{ imageUrl(Voyager::image($product->image),'350','450') }}"></div>
                        <h3>{{ $product->name }}</h3>
                    </a>
                </div>
                @empty
                <div class="alert alert-danger"> No Products Found</div>
                @endforelse
            </div>
        </div>
        <div class="shop_viewBtn">
            <a href="#" class="view_btn">Shop Now</a>
        </div>
    </div>
</div>