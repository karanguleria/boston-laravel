<div class="about_sec">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col abt_home">
                    <div class="abt_homeInn">
                        <h2 class="head_mn">About</h2>
                        <h3 class="sub_head_mn">BOSTON BURGER COMPANY</h3>
                        {!! setting('home-page.about_content') !!}
                        <a href="{{ route('about') }}" class="view_btn">READ MORE</a>
                    </div>
                    </div> 
                    <div class="col abt_img">
                        <img  alt="about image" src="{{ url('storage/'.setting('home-page.about-section-image')) }}">
                    </div>
                    
                </div>
            </div>
        </div>