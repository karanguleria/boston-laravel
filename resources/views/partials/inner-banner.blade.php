<div class="banner inner_banner gallery_banner">
      <div class="container_outer">
        <div class="banner_mn">
          <div class="banner_text">
            <h1>@yield('banner-title')</h1>
            <!-- <div class="food_bn">
              <span>{{ setting('site.banner_tagline') }}</span>
              <img   alt="altText" src="{{ url('storage/'.setting('site.banner_icon')) }}">
            </div> -->
          </div>
        </div>  
      </div>
    </div>