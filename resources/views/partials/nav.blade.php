<header>
    <div class="container">
        <div class="row align-items-center">
            <div class="col nav-menu d-md-none">
                <div class="navButton">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div class="col col-md-auto logo">
                <a href="{{ route('landing-page') }}">
                    <img alt="{{ setting('site.title') }}" src="{{ url('storage/'.setting('site.logo')) }}">
                </a>
            </div>
            <div class="col align-items-center justify-content-end nav">
                <div class="navigation">
                    {{ menu('site') }}
                </div>
                <div class="order_nw">
                    <a href="{{ route('catering.orderNow') }}">Order Now</a>
                </div>
            </div>
        </div>
    </div>
</header>

