<div class="productTestimonial">
  <div class="container_outer">
  <h2 class="head_mn">Testimonials</h2>
  <div id="owlTestmonial" class="owl-carousel owl-theme">
    @php 
    $testimonials = App\Testimonial::all();
    @endphp
    @foreach($testimonials as $testimonial)
    <div class="item">
      <div class="auther_detail">
        <div class="autherImg">
          <img src="{{ productImage($testimonial->image) }}">
        </div>
      </div>
      <p>{!! $testimonial->description !!}</p>
      <div class="auther_detail">
        <div class="autherNm">{{ $testimonial->name }}</div>
      </div>
    </div>
    @endforeach
  </div>
  </div>
</div>
