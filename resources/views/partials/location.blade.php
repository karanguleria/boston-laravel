<div class="about_sec locationSec" id="location">
            <div class="container">
                <div class="row">
                    <div class="col-12 abt_loc text-center">
                       <div class="textLocation"> <h2 class="head_mn">Locations</h2>  
                        {!! setting('home-page.location_content') !!}</div>
                    </div>
                    <div class="col-sm-6 col-lg-3 LocTxtOut">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="locTExt">
                            <p>37 Davis Square</p>
                            <p>Somerville, MA</p>
                            <p><b>(617) 440-7361</b></p>
                            <p><b>M-W</b> 11am-10pm</p>
                            <p><b>Th-Sa</b> 11am-11pm</p>
                            <p><b>Sun</b> 12p-10pm</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 LocTxtOut">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="locTExt">
                            <p>1105 Mass Ave</p>
                            <p>Cambridge, MA</p>
                            <p><b>(857) 242-3605</b></p>
                            <p><b>M-W</b> 11am-11pm</p>
                            <p><b>Th-Sa</b> 11am-12am</p>
                            <p><b>Sun</b> 12p-10pm</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 LocTxtOut">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="locTExt">
                            <p>1100 Boylston St.</p>
                            <p>Boston, MA</p>
                            <p><b>(857) 233-4560</b></p>
                            <p><b>M-W</b> 11am-11pm</p>
                            <p><b>Th-Sa</b> 11am-12am</p>
                            <p><b>Sun</b> 11p-10pm</p>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-3 LocTxtOut">
                        <i class="fas fa-map-marker-alt"></i>
                        <div class="locTExt">
                            <p>133 Washington St.</p>
                            <p>Salem, MA</p>
                            <p><b>(978) 414-5910</b></p>
                            <p><b>M-W</b> 11am-11pm</p>
                            <p><b>Th-Sa</b> 11am-12am</p>
                            <p><b>Sun</b> 11am-10pm</p>
                        </div>
                    </div>
                    <div class="col-12">
                        <h5>***Kendall Square COMING SOON*** 610 Main St. Cambridge, MA</h5>
                    </div>
                </div>
            </div>
        </div>