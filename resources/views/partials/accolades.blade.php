@php 
    // Getting all the Accolades
    $accolades =  App\Accolade::take(6)->get();
@endphp
    <!-- Accolades Section -->
    <div class="accolades_sec">
        <div class="container">
            <!-- <h2 class="head_mn">ACCOLADES</h2> -->
            <div class="accla_sec d-none d-md-block">
                <div class="row">
                    @forelse($accolades as $accolade)
                        <div class="col-4 col-md-6 col-lg-4">     
                            <div class="accla_inn">         
                                <div class="accla_mash">
                                    <img alt="{{ $accolade->title }}" src="{{ Voyager::image( $accolade->image ) }}">
                                    <h3>{!! $accolade->description !!}</h3>
                                </div>
                            </div>
                        </div>
                    @empty
                        <div class="alert alert-danger">No Accolade found</div>
                    @endforelse
                </div>
            </div>
            <div class="accla_sec d-md-none">
                <div class="owl-carousel accla-slide owl-theme">
                @forelse($accolades as $accolade)    
                    <div class="item">
                        <div class="accla_inn">         
                            <div class="accla_mash">
                                <img alt="{{ $accolade->title }}" src="{{ Voyager::image( $accolade->image ) }}">
                                <h3>{!! $accolade->description !!}</h3>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="alert alert-danger">No Accolade found</div>
                @endforelse
                </div>  
            </div>
        </div>
    </div>