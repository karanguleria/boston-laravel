<div class="give_back">
        <div class="container">
            <div class="row align-items-center">
                <div class="col give_bk_txt">
                    <h2 class="head_mn">Give Back</h2>
                    <h3 class="sub_head_mn">BY SUPPORTING THE DAVID ORTIZ FOUNDATION WITH BIG PAPI</h3>
                    <div class="d-none d-md-inline-block">
                        <a class="view_btn" href="http://cloud.marketing.bostonburgercompany.com/give-back">Learn more</a>
                    </div>
                </div>
                <div class="col-auto give_bk_img">
                    <img  alt="give back" src="{{ url('storage/'.setting('home-page.giveback-section-image')) }}">
                    <a class="d-md-none view_btn" href="http://cloud.marketing.bostonburgercompany.com/give-back" >Learn more</a>
                </div>
            </div>
        </div>
    </div>