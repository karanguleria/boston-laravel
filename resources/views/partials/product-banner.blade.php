<div class="banner inner_banner menu_banner product_banner">
      <div class="container_outer">
        <div class="banner_mn">
          <div class="banner_text">
            <h1>@yield('banner-title')</h1>
          </div>
        </div>  
      </div>
    </div>