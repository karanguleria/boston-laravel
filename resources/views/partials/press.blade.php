@php
        // Getting all the Presses
        $presses = App\Press::take(6)->get();
    @endphp
    <!-- Press Section -->
    <div class="press">
        <div class="container_outer">
            <h2 class="head_mn">Press <span>&</span> Accolades</h2>
            <ul>
                @forelse($presses as $k => $press)
                    <li><img src="{{ Voyager::image( $press->image ) }}" alt="{{ $press->title}}"></li>
                <!-- @if($k==1)
                    </ul><ul>
                @endif  -->   
                @empty
                @endforelse
        </div>
    </div>