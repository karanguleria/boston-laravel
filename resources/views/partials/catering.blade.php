<div class="catering">
    <div class="container">
        <div class="row">
            <div class="col catering_img">
                <img alt="altText" src="{{ url('storage/'.setting('home-page.catering-image')) }}">
            </div>
            <div class="col catering_text">
                <div class="head_sec">
                    <h2 class="head_mn">Catering</h2>
                    <h3 class="sub_head_mn">WE COME TO YOU</h3>
                </div>
                {!! setting('home-page.catering-content') !!}
                <a href="{{ route('catering.categories') }}" class="view_btn">LEARN MORE</a>
            </div>
        </div>
    </div>
    <div class="cater_skew"></div>
</div>