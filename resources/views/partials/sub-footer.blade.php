@if(\Route::current()->getName() == 'landing-page')
   
    @include('partials.press')
    @include('partials.accolades')
    @include('partials.testimonials')
    @include('partials.giveback')
    @include('partials.about')
    @include('partials.location')

@elseif(\Route::current()->getName() == 'thank-you')

    @include('partials.shop')

@elseif(\Route::current()->getName() == 'confirmation.index')

    @include('partials.shop')

@elseif(
    \Route::current()->getName() == 'cart.index' ||
    \Route::current()->getName() == 'checkout.index' ||
    \Route::current()->getName() == 'myaccount.index' ||
    \Route::current()->getName() == 'shop.show' ||
    \Route::current()->getName() == 'shop.index' ||
    \Route::current()->getName() == 'confirmation.index'
    )

    @include('partials.giveback')

@elseif(\Route::current()->getName() == 'location')

    @include('partials.catering')
    @include('partials.shop')
    @include('partials.press')
    @include('partials.accolades')
    @include('partials.testimonials')
    @include('partials.giveback')
    @include('partials.about')

@else

    @include('partials.location')
    @include('partials.catering')
    @include('partials.shop')
    @include('partials.press')
    @include('partials.accolades')
    @include('partials.testimonials')
    @include('partials.giveback')
    @include('partials.about')

@endif
