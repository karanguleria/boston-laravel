        <!-- <div ng-controller="classCtrl" ng-class="{'active': isActive}" ng-click="activeButton()" class="social_fix">
            <span>Follow us</span>
            
            <ul>
                <li>
                    <a target="_blank" href="{{ setting('social-links.youtube') }}"><i class="fab fa-youtube"></i></a>
                </li>
                <li>
                    <a target="_blank" href="{{ setting('social-links.facebook') }}"><i class="fab fa-facebook-f"></i></a>
                </li>
                <li>
                    <a target="_blank" href="{{ setting('social-links.linkedin') }}"><i class="fab fa-linkedin-in"></i></a>
                </li>
                <li>
                    <a target="_blank" href="{{ setting('social-links.twitter') }}"><i class="fab fa-twitter"></i></a>
                </li>
                <li>
                    <a target="_blank" href="{{ setting('social-links.instagram') }}"><i class="fab fa-instagram"></i></a>
                </li>
            </ul>
        </div> -->
        <div class="social_fix">
        <span type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#subscribe">SUBSCRIBE</span></div>
                <div class="modal subscribe-model fade" id="subscribe" role="dialog">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="subscribeSec">
                                    <h2>Become a Boston Burger Company INSIDER and receive regular updates, insider scoops, and special offers!</h2>
                                    <div class="subsc_form">
                                        <div class="row">
                                            <div class="col-12 col-md-12">
                                            <input type="text" placeholder="First Name">
                                            </div>
                                            <div class="col-12 col-md-12">
                                            <input type="text" placeholder="Last Name">
                                            </div>
                                            <div class="col-12 col-md-12">
                                            <input type="email" placeholder="Email Address">
                                            </div>
                                           
                                            <div class="col-12">
                                            <button class="btn-subscribe view_btn">SUBSCRIBE</button>
                                            </div>
                                        </div>    
                                    </div>
                            </div>
                            <div class="thanksSec" style="display:none">
                            <h2><span>Thank You!</span> for subscribing.</h2>
                            <button type="button" class="btn btn-default" data-dismiss="modal">X</button>
                            </div>
                        </div>
                        <!-- <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div> -->
                    </div>
                    
                    </div>
                