<div class="col Gallery_sidebar">
            
              <div class="addImage {{ (setting('media-page.aspect_ratio') == '2') ? 'default' :'full'  }}">
                <img src="{{  Voyager::image(setting('media-page.media')) }}" alt="addImage">
              </div>
            <!-- <div class="emal_follow">
              <h4>Get top stories emailed to you.</h4>
              @if(@$_GET)
                @if(@$_GET['subscribed'])
                <div class="alert alert-success">
                    Successfully subscribed to top stories
                </div>
                @endif
              @else
              <form action="https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8" method="POST">
                <input type=hidden name="oid" value="00D0o0000017gdN">
                <input type=hidden name="retURL" value="{{ url()->current() }}/?subscribed=true">
                <input id="email" maxlength="80" name="email" size="20" type="email" placeholder="Email Address">
                <button type="submit">Submit</button>
             </form>
              @endif
            </div> -->
            <div class="sidebar_category">
              <h3>CATEGORIES</h3>
              <ul>
                <li>
                  <a href="{{ route('blog.videos')}}">Videos</a>
                </li>
                <li>
                  <a href="{{ route('blog.photos')}}">Photos</a>
                </li>
                <li>
                  <a href="{{ route('blog.index')}}">Blogs</a>
                </li>
              </ul>
            </div>
            <div class="post_sidebar">
              <h3>RECENT BLOG POST</h3>
              @php 
              $blogs = TCG\Voyager\Models\Post::where('type','blog')->orderBy('id','desc')->take(3)->get();
              @endphp
              <ul>
                @foreach($blogs as $blog)
                <li>
                <a href="{{ route('blog.show',$blog->slug)}}">{!! strtoupper($blog->title) !!}</a>
                </li>
                @endforeach
              </ul>
            </div>
            <!-- <div class="achive_sidebar">
              <h3>ARCHIVE</h3>
             <div class="select_month"> 
                <select>
                  <option>Select Month</option>
                  <option>Janaury</option>
                  <option>April</option>
                  <option>May</option>
                </select>
              </div>  
            </div> -->
           
          </div>