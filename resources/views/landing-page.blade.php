@extends('layouts.master')

@section('content')
<div class="banner">
    <div class="container_outerrr">
        <div class="owl-carousel promotional-slide">
            @foreach($promotionalBanners as $promotionalBanner)
            <div class="item">
                <a href="{{$promotionalBanner->cta_link}}">
                    <div class="banner_mn">
                        <div class="banner_text">
                            {!!$promotionalBanner->description!!}
                            <span class="d-none d-md-inline-block banenrOrder" href="{{$promotionalBanner->cta_link}}">Order Yours</span>
                            <div class="food_bn">
                            <span>{{ setting('site.banner_tagline') }}</span>
                                <img  alt="altText" src="{{ url('storage/'.setting('site.banner_icon')) }}">
                            </div>
                        </div>
                        <div class="banner_img">
                            <img  alt="altText" src="{{ url('storage/'.$promotionalBanner->image) }}">
                            <div class="banner_text d-md-none">
                                <span class="banenrOrder" href="{{$promotionalBanner->cta_link}}">Order Yours</span> 
                            </div>
                        </div> 
                    </div>
                </a>     
            </div> 
            @endforeach
        </div>
    </div>
    <div class="skew"></div>
</div>
    
<div class="includge_sec">
    <div class="container_outer">
        <div class="head_sec">
            <h2 class="head_mn">Indulge</h2>
            <h3 class="sub_head_mn">IN OUR most popular burgers and <span>#freakfrappes</span></h3>
        </div>
        <div class="slider">
            <div class="owl-carousel burger-slide owl-theme">
                @forelse($sliderItems as $sliderItem)
                <div class="item">
                    <a href="#" class="disabledAnchor">
                        <img  alt="{{$sliderItem->title }}" src="{{ Voyager::image($sliderItem->image) }}">
                        <h3>{!! $sliderItem->title !!}</h3>
                    </a>
                </div>
                @empty
                <div class="alert alert-danger"> No Items Found</div>
                @endforelse
            </div>
        </div>
    </div>
</div>
    
<div class="menu_mn" ng-controller="myctrl">
    <div class="container_outer">
        <div class="head_sec">
            <h2 class="head_mn">Menu</h2>
            <h3 class="sub_head_mn">Getting hungry, order now</h3>
        </div>
        <div class="d-none d-md-block">
            <div class="burger_menu">
                <div class="burger_menu_text">
                    @forelse($restaurantMenus as $k=>$restaurantMenu)
                    <div ng-show="show{{ $restaurantMenu->slug }}" class="burger_txt">
                        @php 
                            $menuItems = App\RestaurantItem::getMenuItems($restaurantMenu->id,20);
                        @endphp
                        @forelse($menuItems as $l=>$menuItem)
                            <div class="{{ ( $l%2 ? 'bustn_burger_rt' : 'bustn_burger_lt')}} @if($menuItem->images) @php echo  (count(json_decode($menuItem->images, true)) > 1 ) ? 'multipleImage':''; @endphp @endif ">
                            <div class="titleSec">  
                                  
                            <a href="{{ route('menu','cat='.$restaurantMenu->slug) }}">
                                        <h3>{{ $menuItem->name }} 
                                  @if ($menuItem->images)
                                        @foreach (json_decode($menuItem->images, true) as $k =>$image)
                                            @if($k == 0)
                                            <img src="{{ Voyager::image($image)}}" alt="{{ $menuItem->name }}">
                                            @endif
                                            @if($k == 1)
                                            <img class="secimg" src="{{ Voyager::image($image)}}" alt="{{ $menuItem->name }}">
                                            @endif
                                            
                                        @endforeach
                                    @endif
                                </h3></a>
                                @if(@$menuItem-> price)
                                        <span class="foodprice"> $ {{ $menuItem-> price}}</span>
                                    @endif
                                </div>
                                <p>{{ $menuItem->description }}</p>
                            </div>
                            @if($l > 16)
                            @php break; @endphp
                            @endif
                        @empty
                        <div class="alert alert-danger">No Menu Item found</div>
                        @endforelse
                        <a class="burger_new" href="{{ route('menu') }}">
                            <span></span>
                        </a>                          
                    </div>
                    @empty
                        <div class="alert alert-danger">No Menu found</div>
                    @endforelse
                </div>
                <div class="burger_menu_tab">
                    @forelse($restaurantMenus as $k=>$restaurantMenu)
                        <div ng-click="click{{ $restaurantMenu->slug }}()" class="burger_tab class{{ $restaurantMenu->slug }} {{ $k == 0 ? 'active' :'' }} ">
                            <img alt="{{ $restaurantMenu->name }}" src="{{ Voyager::image($restaurantMenu->image) }}">
                            <h3>{{ $restaurantMenu->name }}</h3>
                        </div>
                    @empty
                        <div class="alert alert-danger">No Menu found</div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>
<div class="d-md-none">
    <div ng-controller="MainCtrl" class="burger_menu_tab" simple-accordion>
        @forelse($restaurantMenus as $k=>$restaurantMenu)
        <div class="burger_block">
            <div class="burger_tab_mob class{{ $restaurantMenu->slug }}">
                <img alt="{{ $restaurantMenu-> name}}" src="{{ Voyager::image($restaurantMenu->image) }}">
                <a href="{{ route('menu','cat='.$restaurantMenu->slug) }}">
                                        <h3>{{ $restaurantMenu-> name}}</h3>
                                        </a>
            </div>
            <div class="burger_block_text" style="display:none">
                
                </div>
            </div>
        @empty
            <div class="alert alert-danger">No Menu found</div>
        @endforelse   
    </div>
</div>

    @include('partials.catering')
    
    @include('partials.shop')
    
<div class="gallery_home">
    <h2 class="head_mn">Media</h2>
    <div class="hm_gallery_mn">
        <div class="galery_sec">
            <img  alt="altText" src="{{ url('storage/'.setting('home-page.photo-video-background-image')) }}">
            <div class="gallery_text">
                <h2 class="head_mn">Videos</h2>
                <a href="{{ route('blog.videos') }}" class="view_btn">View More</a>
            </div>
        </div>
        <div class="galery_sec">
            <img  alt="altText" src="{{ url('storage/'.setting('home-page.photo-background-image')) }}">
            <div class="gallery_text">
                <h2 class="head_mn">Photos</h2>
                <a href="{{ route('blog.photos') }}" class="view_btn">View More</a>
            </div>
        </div>
        <div class="galery_sec">
            <img  alt="altText" src="{{ url('storage/'.setting('home-page.blog-background-image')) }}">
            <div class="gallery_text">
                <h2 class="head_mn">Blogs</h2>
                <a href="{{ route('blog.index') }}" class="view_btn">View More</a>
            </div>
        </div>
        <div class="galery_sec">
            <img  alt="altText" src="{{ url('storage/'.setting('home-page.press_image')) }}">
            <div class="gallery_text">
                <h2 class="head_mn">Press <span>&</span> Accolades</h2>
                <a href="{{ route('blog.index') }}" class="view_btn">View More</a>
    </div>
</div>
    </div>
</div>

@include('partials.sub-footer')	
@include('partials.footer')	

@endsection

@section('extra-js-footer')
<script type="text/javascript" src="{{ asset('js/owl.carousel.min.js')}}"></script>
    <script>
        $(document).ready(function() {
             
            // Promotional Banner Carousel 
            var owl = $('.promotional-slide');
            owl.owlCarousel(
            {
                margin: 2,
                items:1,
                nav: true,
                loop: true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:false,
            });
             // Shop Overview sliider
             var owl = $('.shop-slide');
            owl.owlCarousel(
            {
                margin: 2,
                nav: true,
                loop: true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                responsive: {
                    0: {
                    items: 1
                    },
                    576: {
                    items: 2
                    },
                    768: {
                    items: 3
                    },
                    1000: {
                    items: 5
                    }
                }
            });
            // Product Showcase sliider
             var owl = $('.burger-slide');
            owl.owlCarousel(
            {
                margin: 2,
                nav: true,
                loop: true,
                autoplay:true,
                autoplayTimeout:5000,
                autoplayHoverPause:true,
                responsive: {
                    0: {
                    items: 1
                    },
                    576: {
                    items: 2
                    },
                    768: {
                    items: 3
                    },
                    1000: {
                    items: 5
                    }
                }
            });
            
        });
        var app = angular.module("myapp", ['simple-accordion']);
        app.controller("myctrl", function($scope){
            $scope.showburgers = true;
            $scope.clickburgers = function () 
            {
                $scope.showburgers = true;
                $scope.showfreakfrappes = false;
                $scope.showsandwiches = false;
                $scope.showappetizers = false;
                $scope.showsalads = false;
            }
            $scope.clickfreakfrappes = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= true;
                $scope.showsandwiches= false;
                $scope.showappetizers= false;
                $scope.showSalad= false;
            }
            $scope.clicksandwiches = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= false;
                $scope.showsandwiches= true;
                $scope.showappetizers= false;
                $scope.showsalads= false;
            }
            $scope.clickappetizers = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= false;
                $scope.showsandwiches= false;
                $scope.showappetizers= true;
                $scope.showsalads= false;
            }
            $scope.clicksalads = function()
            {
                $scope.showburgers= false;
                $scope.showfreakfrappes= false;
                $scope.showsandwiches= false;
                $scope.showappetizers= false;
                $scope.showsalads= true;
            }
        }); 
        // app.controller('classCtrl', function ($scope) {
        //   $scope.isActive = false;
        //   $scope.activeButton = function() {
        //     $scope.isActive = !$scope.isActive;
        //   }  
        // });

        app.controller('MainCtrl', function($scope) {

         });
       
          $('.burger_tab').on('click', function(){
            $('.burger_tab.active').removeClass('active');
            $(this).addClass('active');
          });
    </script>
@endsection