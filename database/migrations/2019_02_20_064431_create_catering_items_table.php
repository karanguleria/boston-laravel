<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCateringItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catering_items', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('display_price')->nullable();
            $table->float('price', 8, 2);
            $table->string('quantity')->default(10);
            $table->string('image')->nullable();
            $table->text('images')->nullable();
            $table->text('details')->nullable();
            $table->text('description');
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')
            ->on('catering_menus')->onDelete('cascade');
            $table->boolean('featured')->default(false);
            $table->integer('type')->unsigned()->default(1);
            $table->boolean('out_of_stock')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catering_items');
    }
}
