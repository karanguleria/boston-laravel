<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->string('subject');
            $table->string('date_of_event')->nullable();
            $table->string('corporate_catering')->default('off')->nullable();
            $table->string('social_event_catering')->default('off')->nullable();
            $table->string('concession_catering')->default('off')->nullable();
            $table->string('wedding')->default('off')->nullable();
            $table->string('golf_tournament')->default('off')->nullable();
            $table->string('food_truck')->default('off')->nullable();
            $table->text('message');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
