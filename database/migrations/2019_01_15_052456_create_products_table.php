<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('display_price')->nullable();
            $table->float('price', 8, 2);
            $table->string('quantity')->default(10);
            $table->string('image')->nullable();
            $table->text('images')->nullable();
            $table->text('details')->nullable();
            $table->text('description');
            $table->boolean('featured')->default(false);
            $table->integer('type')->unsigned()->default(1);
            $table->boolean('out_of_stock')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
