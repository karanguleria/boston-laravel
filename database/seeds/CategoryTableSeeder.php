<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now()->toDateTimeString();
        Category::insert([
            ['name' => 'mac and cheese', 'slug' => 'mac-and-cheese', 'image' => 'images/catering3.jpg', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'SHIRTS', 'slug' => 'shirts', 'image' => 'images/store1.jpg', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'SALSA', 'slug' => 'salsa', 'image' => 'images/catering2.jpg', 'created_at' => $now, 'updated_at' => $now],
            ['name' => 'GIFT CARDS', 'slug' => 'gift-cards', 'image' => 'images/store2.jpg', 'created_at' => $now, 'updated_at' => $now],
          
        ]);
    }
}
