<?php

use Illuminate\Database\Seeder;
use App\Product;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
            Product::create([
                'name' => "BBC's Best of Mac & Cheese Trio - Free Shipping!",
                'slug' => 'bbc-best-of-mac',
                'display_price' => '75',
                'price' => '69.99',
                'description' => " We've created a six-pound trio of our best, most delicious, Mac & Cheese for you and a few hungry friends! Order today while supplies last! ",
                 ])->categories()->attach(1);
        

        //  Just to test multiple categories
        $product = Product::find(1);
        $product->categories()->attach(2);
        
        for ($i=1; $i <= 5; $i++) {
        Product::create([
            'name' => "shirt - ".$i,
            'slug' => 'shirt-'.$i,
            'price' => '25',
            'description' => " We've created a six-pound trio of our best, most delicious, Mac & Cheese for you and a few hungry friends! Order today while supplies last! ",
             ])->categories()->attach(2);
        }
    
        for ($i=1; $i <= 5; $i++) {
            Product::create([
                'name' => "Salsa - ".$i,
                'slug' => 'salsa-'.$i,
                'price' => '7',
                'description' => " We've created a six-pound trio of our best, most delicious, Mac & Cheese for you and a few hungry friends! Order today while supplies last! ",
                 ])->categories()->attach(3);
            }
        for ($i=1; $i <= 5; $i++) {
            Product::create([
                'name' => "Gift - ".$i,
                'slug' => 'gift-'.$i,
                'price' =>  rand(15, 100),
                'description' => " We've created a six-pound trio of our best, most delicious, Mac & Cheese for you and a few hungry friends! Order today while supplies last! ",
                    ])->categories()->attach(3);
            }
        
    }
}
